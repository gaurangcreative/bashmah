//
//  GradientView.swift
//  CommunitySafetyWatch
//
//  Created by ABC on 4/16/18.
//  Copyright © 2018 ABC. All rights reserved.
//

import UIKit

class GradientView: UIView
{
    override func layoutSubviews()
    {
        let startColor = UIColor(red: 58/255.0, green: 102/255.0, blue: 234/255.0, alpha: 1.0)
        let endColor = UIColor(red: 222/255.0, green: 106/255.0, blue: 197/255.0, alpha: 1.0)
        applyGradient(colors: [startColor , endColor])
    }
    
    func applyGradient(colors: [UIColor])
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        self.layer.insertSublayer(gradient, at: 0)
    }
}

class GradientBtn: UIButton
{
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        
        let startColor = UIColor(red: 58/255.0, green: 102/255.0, blue: 234/255.0, alpha: 1.0)
        let endColor = UIColor(red: 222/255.0, green: 106/255.0, blue: 197/255.0, alpha: 1.0)
        
        DispatchQueue.main.async
            {
                self.applyGradient(colours: [startColor, endColor])
            }
    }
    
    func applyGradient(colours: [UIColor]) -> Void
    {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradient, below: self.imageView?.layer)
    }
    
}
