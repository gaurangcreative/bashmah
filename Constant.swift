//
//  Constant.swift
//  wholesale
//
//  Created by CT on 8/31/18.
//  Copyright © 2018 CT. All rights reserved.
//

import Foundation
import Firebase

let kBaseURL = "https://ctinfotech.com/CT01/basmah/api/"

let color = UIColor.red
let alerttitle = "Basmah"
let alertservererr = "Server Error"
let alertok = "OK"

struct Constants
{
    struct refs
    {
        static let databaseRoot = FIRDatabase.database().reference()
        static let databaseChats = databaseRoot.child("chats")
    }
}

struct INSTAGRAM_IDS
{
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "9d380eb791c847778e7049774e49bf14"
    
    static let INSTAGRAM_CLIENTSERCRET = "b2c5b1b191664769a508e845bd334782"
    
    static let INSTAGRAM_REDIRECT_URI = "http://www.creativethoughtsinfo.com/"
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
}

public enum Setting: String
{
    case removeBubbleTails = "Remove message bubble tails"
    case removeSenderDisplayName = "Remove sender Display Name"
    case removeAvatar = "Remove Avatars"
}

