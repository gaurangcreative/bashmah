
//
//  AppDelegate.swift
//  Basmah
//
//  Created by CT on 9/3/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import FBSDKLoginKit
import GoogleSignIn
import Google
import Firebase
import Reachability

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInUIDelegate
{
    var window: UIWindow?
    var greeting = ""
    var device_token:String = String()
    var reachability: Reachability?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        //IQKeyboardManager Enable//
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        self.reachability = Reachability()
        do
        {
            try reachability?.startNotifier()
        }
        catch
        {
            print( "ERROR: Could not start reachability notifier." )
        }
        
        // iOS 10 support
        if #available(iOS 10, *)
        {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *)
        {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *)
        {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else
        {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
        GIDSignIn.sharedInstance().clientID = "1082098399003-5l1i2oo4aa4dvd79taunamdnqvgk5s6s.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().uiDelegate = self
        
        //Fire Base Configure//
        FIRApp.configure()

        return true
    }
    
    class func sharedAppDelegate() -> AppDelegate?
    {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    //MARK:- Device Token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        device_token = deviceTokenString
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any])
    {
        // Print notification payload data
        print("Push notification received: \(data)")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        // Override point for customization after application launch.
        
        let returnValuen = UserDefaults.standard.string(forKey: "LoginStatus")
        
        if(returnValuen == "FB")
        {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
        else if(returnValuen == "Twitter")
        {
            // Fabric.with([Twitter.self])
            //return Twitter.sharedInstance().application(app, open: url, options: options)
        }
        else if(returnValuen == "Google")
        {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Show Alert
    func showAlert(title : String,message : String,buttonTitle : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Check Valid Email
    func isValidEmail(testStr:String) -> Bool
    {
        let regex1 = "\\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z"
        let regex2 = "^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*"
        let test1 = NSPredicate(format: "SELF MATCHES %@", regex1)
        let test2 = NSPredicate(format: "SELF MATCHES %@", regex2)
        return test1.evaluate(with: testStr) && test2.evaluate(with: testStr)
    }
}

