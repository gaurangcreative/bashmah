//
//  instagramLoginVC.swift
//  InstagramIntegration
//
//  Created by Kirti Ahlawat on 26/06/18.
//  Copyright © 2018 shashank. All rights reserved.
//

import UIKit
import WebKit

class instagramLoginVC: UIViewController, WKUIDelegate, WKNavigationDelegate
{
    var webView: WKWebView!
    
    override func loadView()
    {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: self.view.frame, configuration: webConfiguration)
        view = webView
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        unSignedRequest()
    }
    
    //MARK: - unSignedRequest
    func unSignedRequest ()
    {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        webView.load(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool
    {
        let requestURLString = (request.url?.absoluteString)! as String
        print("String: \(requestURLString)")
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI)
        {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)
    {
        print("Instagram authentication token ==", authToken)
        self.navigationController?.popViewController(animated: true)
        let url = String(format: "https://api.instagram.com/v1/users/self/?access_token=%@", authToken)
        let request : NSMutableURLRequest = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let session = URLSession(configuration: URLSessionConfiguration.default)
        session.dataTask(with: request as URLRequest)
        { (data, response, error) in
            
            if let data = data
            {
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                let userInfo = json?.value(forKey: "data") as Any
                print(userInfo)
                
               // Sociallogin()
            }
            
        }.resume()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        print("Status : \(checkRequestForCallbackURL(request: navigationAction.request))")
        decisionHandler(.allow)
    }
    
//    func Sociallogin()
//    {
//        let parameters: Parameters =
//            [
//                "user_id":self.social_id as AnyObject,
//                "username":self.full_name as AnyObject,
//                "user_image":self.image_user as AnyObject,
//                "android_token":"" as AnyObject,
//                "ios_token":appDelegate.device_token as AnyObject,
//                "user_address":"" as AnyObject,
//                "dob":"" as AnyObject,
//                "country":"" as AnyObject,
//                "login_type":self.login_type,
//                ]
//
//        print(parameters)
//
//        let url = kBaseURL + "social_login"
//        hud = JGProgressHUD(style: .dark)
//        hud.textLabel.text = "Loading"
//        hud.show(in: self.view)
//
//        let headers: HTTPHeaders = [
//            /* "Authorization": "your_access_token",  in case you need authorization header */
//            "Content-type": "multipart/form-data",
//            "Authorization": "dfs#!df154$",
//            ]
//
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//
//            for (key, value) in parameters
//            {
//                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
//            }
//
//        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
//
//            self.hud.dismiss()
//
//            switch result
//            {
//            case .success(let upload, _, _):
//
//                upload.responseJSON { response in
//
//                    print(response.response!) // URL response
//                    print(response.data!)     // server data
//                    print(response.result.value!)
//                    do
//                    {
//                        let swiftyJsonVar = JSON(response.result.value!)
//
//                        if swiftyJsonVar["success"].string == "1"
//                        {
//                            let data = swiftyJsonVar["userinfo"]
//
//                            let UserDefault = UserDefaults.standard
//                            UserDefault.set(data["user_id"].stringValue,forKey:"user_id")
//                            UserDefault.set(data["id"].stringValue,forKey:"id")
//                            UserDefault.set(data["username"].stringValue,forKey:"username")
//
//                            if (self.appDelegate.greeting == "FB")
//                            {
//                                UserDefault.set(data["user_image"].stringValue,forKey:"userfb_image")
//                            }
//                            else if(self.appDelegate.greeting == "Google")
//                            {
//                                UserDefault.set(data["user_image"].stringValue,forKey:"usergoo_image")
//                            }
//
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                            let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
//                            navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "HomeViewController")], animated: false)
//
//                            let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
//                            mainViewController.rootViewController = navigationController
//                            mainViewController.setup(type: 2)
//                            let window = UIApplication.shared.delegate!.window!!
//                            window.rootViewController = mainViewController
//                            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
//                        }
//                        else
//                        {
//                            let message = swiftyJsonVar["message"].stringValue
//                            self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
//                        }
//                    }
//                }
//
//            case .failure(let error):
//                self.hud.dismiss()
//                print("Error in upload: \(error.localizedDescription)")
//            }
//        }
//    }
    
}
