//
//  StoreCollectionViewCell.swift
//  Bashmah
//
//  Created by CTIMac on 14/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class StoreCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageView_Room: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img_check: UIImageView!
    @IBOutlet weak var lbl_giftprice: UILabel!
    
}


