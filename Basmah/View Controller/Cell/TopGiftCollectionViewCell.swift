//
//  TopGiftCollectionViewCell.swift
//  Basmah
//
//  Created by CT on 9/5/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class TopGiftCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var img_gift: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    
}
