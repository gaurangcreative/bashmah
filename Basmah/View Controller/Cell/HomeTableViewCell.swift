//
//  HomeTableViewCell.swift
//  Basmah
//
//  Created by CT on 9/4/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var img_country: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_friends: UILabel!
    @IBOutlet weak var img_usic: UIImageView!
    @IBOutlet weak var lbl_usercount: UILabel!
    @IBOutlet weak var lbl_desc: UILabel!
    @IBOutlet weak var img_arrow: UIImageView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var switch1: UISwitch!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
