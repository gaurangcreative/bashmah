//
//  UserCollectionViewCell.swift
//  Bashmah
//
//  Created by CTIMac on 14/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageView_Unique: UIImageView!
    @IBOutlet weak var lblUser: UILabel!
}
