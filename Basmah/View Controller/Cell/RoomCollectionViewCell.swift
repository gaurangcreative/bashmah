//
//  RoomCollectionViewCell.swift
//  Basmah
//
//  Created by CT on 9/6/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class RoomCollectionViewCell: UICollectionViewCell
{
//    @IBOutlet weak var constantLabWidth: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_title: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.lbl_title.layer.cornerRadius = 6.0
        self.lbl_title.layer.masksToBounds = true
        // Initialization code
    }
}

