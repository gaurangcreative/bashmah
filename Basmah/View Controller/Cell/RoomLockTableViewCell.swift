//
//  RoomLockTableViewCell.swift
//  Bashmah
//
//  Created by CTIMac on 13/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class RoomLockTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMonthNumber: UILabel!
    @IBOutlet weak var view_room: UIView!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var view_bg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
