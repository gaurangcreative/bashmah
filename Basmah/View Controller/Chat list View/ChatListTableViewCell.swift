//
//  ChatListTableViewCell.swift
//  Basmah
//
//  Created by CT on 9/19/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell
{
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var img_gender: UIImageView!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_msg: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var img_msg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
