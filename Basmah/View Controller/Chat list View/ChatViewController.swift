//
//  ChatViewController.swift
//  Basmah
//
//  Created by CT on 9/24/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Firebase
import Alamofire
import SwiftyJSON
import JGProgressHUD

class ChatViewController: JSQMessagesViewController
{
    var messages = [JSQMessage]()
    var time :NSDate = NSDate()
    var other_username:String = String()
    var other_userimage:String = String()
    var user_id:String = String()
    var room_id:String = String()
    var to_id:String = String()
    var type:String = String()
    var userRef: FIRDatabaseReference?
    var top_view: UIView = UIView()
    var appDelegate = AppDelegate()
    var hud = JGProgressHUD()

    let imagePicker = UIImagePickerController()
    var chosenImage = UIImage()
    var image_Data = UIImageJPEGRepresentation(UIImage(),0)

    private var newMessageRefHandle: FIRDatabaseHandle?
    
    private let avatarSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height: kJSQMessagesCollectionViewAvatarSizeDefault)


    //Bubble Outgoing
    lazy var outgoingBubble: JSQMessagesBubbleImage =
        {
            return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor(red: 143/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0))
    }()
    
    //Bubble Incoming
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.darkGray)
    }()
    
    
    var sender_image = JSQMessagesAvatarImageFactory.avatarImage(with: #imageLiteral(resourceName: "img"), diameter: 80)
    var receiver_image = JSQMessagesAvatarImageFactory.avatarImage(with:#imageLiteral(resourceName: "img"), diameter: 80)

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       appDelegate = UIApplication.shared.delegate as! AppDelegate
        
       imagePicker.delegate = self

       userRef = FIRDatabase.database().reference()

       self.navigationController?.setNavigationBarHidden(false, animated: false) //SHow
       
       let url = URL(string:other_userimage)
        
        if let data = try? Data(contentsOf: url!)
        {
            let image: UIImage = UIImage(data: data)!
            receiver_image = JSQMessagesAvatarImageFactory.avatarImage(with:image, diameter: 80)
        }
        
        self.UserDetails()
        
        let defaults: UserDefaults? = UserDefaults.standard

        
////        if(appDelegate.greeting == "FB")
////        {
//            if(defaults?.dictionaryRepresentation().keys.contains("userfb_image"))!
//            {
//                let user_image = UserDefaults.standard.value(forKey: "userfb_image") as! String
//
//                let url = URL(string:user_image)
//
//                if let data = try? Data(contentsOf: url!)
//                {
//                    let image: UIImage = UIImage(data: data)!
//                    sender_image = JSQMessagesAvatarImageFactory.avatarImage(with:image, diameter: 80)
//                }
//            }
//        //}
        
////        else if (appDelegate.greeting == "Google")
////        {
//            if(defaults?.dictionaryRepresentation().keys.contains("usergoo_image"))!
//            {
//                let user_image = UserDefaults.standard.value(forKey: "usergoo_image") as! String
//                let url = URL(string:user_image)
//                if let data = try? Data(contentsOf: url!)
//                {
//                    let image: UIImage = UIImage(data: data)!
//                    sender_image = JSQMessagesAvatarImageFactory.avatarImage(with:image, diameter: 80)
//                }
//            }
//        //}
        
////        else
////        {
//            if(defaults?.dictionaryRepresentation().keys.contains("user_image"))!
//            {
//                let user_image = UserDefaults.standard.value(forKey: "user_image") as! String
//                let url = URL(string:user_image)
//                if let data = try? Data(contentsOf: url!)
//                {
//                    let image: UIImage = UIImage(data: data)!
//                    sender_image = JSQMessagesAvatarImageFactory.avatarImage(with:image, diameter: 80)
//                }
//            }
//       // }
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            user_id = UserDefaults.standard.value(forKey: "id")as! String
            let name = UserDefaults.standard.value(forKey: "username")as! String
            
            senderId = user_id
            senderDisplayName = name
            self.title = other_username
        }
        
        // Top View //
//        top_view.translatesAutoresizingMaskIntoConstraints = false
//        top_view.backgroundColor = UIColor.black
//        self.view.addSubview(top_view)
//
//        let leading = NSLayoutConstraint(item: top_view,
//                                         attribute: .leading,
//                                         relatedBy: .equal,
//                                         toItem: self.view,
//                                         attribute: .leading,
//                                         multiplier: 1.0,
//                                         constant: 0.0)
//
//        let trailing = NSLayoutConstraint(item: top_view,
//                                          attribute: .trailing,
//                                          relatedBy: .equal,
//                                          toItem: self.view,
//                                          attribute: .trailing,
//                                          multiplier: 1.0,
//                                          constant: 0.0)
//
//        let top = NSLayoutConstraint(item: top_view,
//                                     attribute: .top,
//                                     relatedBy: .equal,
//                                     toItem: self.view,
//                                     attribute: .top,
//                                     multiplier: 1.0,
//                                     constant: 0.0)
//
//        let height = NSLayoutConstraint(item: top_view,
//                                        attribute: .height,
//                                        relatedBy: .equal,
//                                        toItem: nil,
//                                        attribute: .notAnAttribute,
//                                        multiplier: 1.0,
//                                        constant: 60.0)
//
//        NSLayoutConstraint.activate([leading, trailing, top, height])
        
         // Top View //
        
        // enable Back Button
        self.inputToolbar.contentView.leftBarButtonItem.isEnabled = true
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 143/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        // Setup navigation
        setupBackButton()
        
        if (defaults?.bool(forKey: Setting.removeAvatar.rawValue))!
        {
            collectionView?.collectionViewLayout.incomingAvatarViewSize = .zero
            collectionView?.collectionViewLayout.outgoingAvatarViewSize = .zero
        }
        else
        {
            collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
            
            collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        }
        
        // This is a beta feature that mostly works but to make things more stable it is diabled.
        collectionView?.collectionViewLayout.springinessEnabled = false
        automaticallyScrollsToMostRecentMessage = true
        
        userRef = userRef?.child(room_id)
        createMessageObservers()
    }
    
    //MARK: Api Call
    func UserDetails()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":id as AnyObject,
                    "type":"2" as AnyObject,
                    "my_id":"" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_detail"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let dic = swiftyJsonVar["userinfo"]
                                    
                                    let imagestr = dic["user_image"].stringValue
                                    let imageUrl = URL(string: imagestr)!
                                    
                                    if let data = try? Data(contentsOf: imageUrl)
                                    {
                                        let image: UIImage = UIImage(data: data)!
                                        self.sender_image = JSQMessagesAvatarImageFactory.avatarImage(with:image, diameter: 80)
                                    }
                                    
                                    self.finishSendingMessage()
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func prepareCollectionView()
    {
        collectionView?.collectionViewLayout.incomingAvatarViewSize = avatarSize
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = avatarSize
    }
    
    //MARK: Observer
    func createMessageObservers()
    {
        let messageQuery = userRef?.queryLimited(toLast:25)
        newMessageRefHandle = messageQuery?.observe(.childAdded, with: { (snapshot) -> Void in

            var data = snapshot.value
            
            if !(data is NSNull)
                        {
                            let finaldata  = snapshot.value as? [String: String]
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            
                            let date = finaldata!["date"]
                            // convert your string to date
                            formatter.dateFormat = "dd-MMM-yyyy hh:mm:a"
                            self.time = formatter.date(from: date!)! as NSDate
                            
                            print(self.to_id)
                            print(finaldata!["senderId"] as String?)
                            
                            if (finaldata!["sender_id"] == self.to_id)
                            {
                                if (finaldata!["type"] == "1")
                                {
                                    if let message = JSQMessage(senderId: self.user_id, senderDisplayName: "", date: self.time as Date?, text: finaldata!["text"])
                                    {
                                        self.messages.append(message)
                                        self.finishReceivingMessage()
                                    }
                                }
                                else
                                {
//                                    if let message = JSQMessage(senderId: self.user_id, senderDisplayName: "", date: self.time as Date?, text: finaldata!["url"])
//                                    {
//                                        self.messages.append(message)
//                                        self.finishReceivingMessage()
//                                    }
                                    
                                    var photoItem: AsyncPhotoMediaItem? = nil
                                    if let aKey = URL(string: finaldata!["url"] ?? "")
                                    {
                                        photoItem = AsyncPhotoMediaItem(withURL: aKey as NSURL)
                                    }

                                   if let message = JSQMessage(senderId: self.user_id, displayName: "", media: photoItem)
                                   {
                                        self.messages.append(message)
                                        self.finishReceivingMessage()
                                   }
                                }
                            }
                            else
                            {
                                
                                if (finaldata!["type"] == "1")
                                {
                                    if let message = JSQMessage(senderId: self.to_id, senderDisplayName: self.senderDisplayName, date: self.time as Date?, text: finaldata!["text"])
                                    {
                                        self.messages.append(message)
                                        self.finishReceivingMessage()
                                    }
                                }
                                else
                                {
                                    var photoItem: AsyncPhotoMediaItem? = nil
                                    if let aKey = URL(string: finaldata!["url"] ?? "")
                                    {
                                        photoItem = AsyncPhotoMediaItem(withURL: aKey as NSURL)
                                    }
                                    
                                    if let message = JSQMessage(senderId: self.to_id, displayName: self.senderDisplayName, media: photoItem)
                                    {
                                        self.messages.append(message)
                                        self.finishReceivingMessage()
                                    }
                                }
                            }
                        }
        })
    }
    
    func setupBackButton()
    {
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonTapped))
        // Image needs to be added to project.
        let buttonIcon = UIImage(named: "signup_back_icon")
        backButton.image = buttonIcon
        backButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func backButtonTapped()
    {
        self.navigationController?.setNavigationBarHidden(true, animated: true) //Hide
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:Collection View
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        return messages[indexPath.item].senderId == senderId ? sender_image : receiver_image
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString?
    {
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        
        if (indexPath.item % 3 == 0)
        {
            let message = self.messages[indexPath.item]
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat
    {
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every 3rd message
         */
        if indexPath.item % 3 == 0
        {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString?
    {
        let message = messages[indexPath.item]
        
        // Displaying names above messages
        //Mark: Removing Sender Display Name
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         */
        //        if defaults.bool(forKey: Setting.removeSenderDisplayName.rawValue) {
        //            return nil
        //        }
        
        if message.senderId == self.senderId
        {
            return nil
        }
        
        //return NSAttributedString(string: message.senderDisplayName)
        return NSAttributedString(string: "")
    }
    
    //MARK: Send Message
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
       // let ref = Constants.refs.databaseChats.childByAutoId()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd-MMM-yyyy hh:mm:a"
        self.type = "1"
        
        let myStringafd = formatter.string(from: yourDate!) as String
        
        let message = ["sender_id": to_id, "name": senderDisplayName, "text": text,"date":myStringafd,"type":self.type,"url":""] as [String : Any]
        
        let ref = userRef?.childByAutoId()
        ref?.setValue(message)
        finishSendingMessage()
        
        let data = (text).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print(base64)
        self.updateChat(message:base64)
    }
    
    //MARK:- Media Send
    override func didPressAccessoryButton(_ sender: UIButton)
    {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler:
            { _ in
                self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Open Image from camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Basmah", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Image from gallery
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
//    func addMedia(_ media:JSQMediaItem)
//    {
//        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
//        self.messages.append(message!)
//
//        //Optional: play sent sound
//        self.finishSendingMessage(animated: true)
//    }
    
    func updateChat (message:String)
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            if (self.type == "1")
            {
                let parameters: Parameters =
                    [
                        "user_from":id as AnyObject,
                        "user_to":to_id as AnyObject,
                        "type":self.type as AnyObject,
                        "message":message as AnyObject,
                    ]
                
                print(parameters)
                
                let url = kBaseURL + "chat"
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    
                    
                    switch result
                    {
                        case .success(let upload, _, _):
                        
                        upload.responseJSON { response in
                            
                            print(response.response!) // URL response
                            print(response.data!)     // server data
                            
                            if (response.result.value == nil)
                            {
                                print("Response nil")
                                self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                            }
                            else
                            {
                                do
                                {
                                    let swiftyJsonVar = JSON(response.result.value!)
                                    
                                    if swiftyJsonVar["success"].string == "1"
                                    {
                                        
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                            }
                        }
                        
                        case .failure(let error):
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
            }
            else
            {
                let parameters: Parameters =
                    [
                        "user_from":id as AnyObject,
                        "user_to":to_id as AnyObject,
                        "type":self.type as AnyObject,
                    ]
                
                print(parameters)
                
                let url = kBaseURL + "chat"
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                    if let data = self.image_Data
                    {
                        multipartFormData.append(data, withName: "message",fileName: "file.jpg", mimeType: "image/jpg")
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    
                    
                    switch result
                    {
                    case .success(let upload, _, _):
                        
                        upload.responseJSON { response in
                            
                            print(response.response!)
                            print(response.data!)
                            
                            if (response.result.value == nil)
                            {
                                print("Response nil")
                                self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                            }
                            else
                            {
                                do
                                {
                                    let swiftyJsonVar = JSON(response.result.value!)
                                    
                                    if swiftyJsonVar["success"].string == "1"
                                    {
                                        let dict = swiftyJsonVar["last_msg"]
                                        
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        let myString = formatter.string(from: Date())
                                        let yourDate = formatter.date(from: myString)
                                        formatter.dateFormat = "dd-MMM-yyyy hh:mm:a"
                                        
                                        let myStringafd = formatter.string(from: yourDate!) as String
                                        
                                        let message = ["sender_id": self.to_id, "name": self.senderDisplayName, "text": "","date":myStringafd,"type":self.type,"url":dict["message"].stringValue] as [String : Any]
                                        
                                        let ref = self.userRef?.childByAutoId()
                                        ref?.setValue(message)
                                        self.finishSendingMessage()
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                            }
                        }
                        
                        case .failure(let error):
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ChatViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerEditedImage] as? UIImage) != nil
        {
            self.chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            self.image_Data = UIImageJPEGRepresentation(chosenImage,0.5)
            self.type = "2"
            
            self.updateChat(message:"")
        }
        
        picker.dismiss(animated: true)
    }
}

