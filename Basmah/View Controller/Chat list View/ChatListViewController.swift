//
//  ChatListViewController.swift
//  Basmah
//
//  Created by CT on 9/19/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import SwiftyJSON

class ChatListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_chat:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var table_view: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    self.Chatlist()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_chat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ChatListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChatListTableViewCell
        
        cell.lbl_count.layer.cornerRadius = cell.lbl_count.frame.size.width/2
        cell.lbl_count.layer.masksToBounds = true
        
        let dic = JSON(arr_chat.object(at: indexPath.row))
        
        if (dic["message_staus"].stringValue == "2")
        {
            let decodevalue = dic["message"].stringValue.base64Decoded()
            cell.lbl_msg.text = decodevalue
            cell.img_msg.isHidden = true
            cell.lbl_msg.isHidden = false
        }
        else
        {
            cell.img_msg.isHidden = false
            cell.lbl_msg.isHidden = true
            
            cell.img_msg.kf.indicatorType = .activity
            cell.img_msg.layer.cornerRadius = 4.0
            cell.img_msg.layer.masksToBounds = true
            
            (cell.img_msg.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_msg.kf.setImage(with: URL(string:dic["message"].stringValue), placeholder: UIImage(named: ""))
        }
        
        cell.lbl_username.text = dic["user_name"].stringValue
        
        cell.img_user.kf.indicatorType = .activity
        cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
        cell.img_user.layer.masksToBounds = true

        (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    let defaults: UserDefaults? = UserDefaults.standard
                    if(defaults?.dictionaryRepresentation().keys.contains("id"))!
                    {
                        let dic = JSON(arr_chat.object(at: indexPath.row))
                        let to_id = dic["user_id"].string as! String
                        
                        let chef_id = to_id
                        let userid = UserDefaults.standard.value(forKey: "id") as! String
                        
                        if (chef_id > userid)
                        {
                            let chatId = "room_" + chef_id + "_" + userid
                            print(chatId)
                            
                            let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                            chat.to_id = to_id
                            chat.other_username = dic["user_name"].string!
                            chat.other_userimage = dic["image"].string!
                            chat.room_id = chatId
                            self.navigationController?.pushViewController(chat, animated: true)
                        }
                        else
                        {
                            let chatId = "room_" + userid + "_" + chef_id
                            print(chatId)
                            
                            let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                            chat.to_id = to_id
                            chat.other_username = dic["user_name"].string!
                            chat.other_userimage = dic["image"].string!
                            chat.room_id = chatId
                            self.navigationController?.pushViewController(chat, animated: true)
                        }
                    }
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    //MARK: Api Call
    func Chatlist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "chatlist"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["message"].array! as NSArray
                                    self.arr_chat = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_chat.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
}

extension String {
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}

