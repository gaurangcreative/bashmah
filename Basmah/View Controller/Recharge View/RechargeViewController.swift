//
//  RechargeViewController.swift
//  Basmah
//
//  Created by CT on 9/19/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class RechargeViewController: UIViewController,UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var img_currency: UIImageView!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var lbl_userCoin: UILabel!
    
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_recharge_list:NSMutableArray = NSMutableArray()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        img_currency.layer.cornerRadius = img_currency.frame.size.width/2
        img_currency.layer.masksToBounds = true
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    Rechargelist()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:IBAction
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonViewDetails(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    //MARK:Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_recharge_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
        cell.view_shadow.layer.shadowColor = UIColor.lightGray.cgColor
        cell.view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.view_shadow.layer.shadowOpacity = 0.5
        cell.view_shadow.layer.shadowRadius = 2.0
        cell.view_shadow.layer.masksToBounds = false
        
        let dic = JSON(arr_recharge_list.object(at: indexPath.row))

        cell.lbl_coin.text =  dic["coin"].stringValue
        cell.lbl_price.text = "$" + dic["price"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_recharge_list.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
    
    //MARK: Api Call
    func Rechargelist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "recharge_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.lbl_userCoin.text = swiftyJsonVar["user_coin"].stringValue
                                    
                                    let arr = swiftyJsonVar["recharge_list"].array! as NSArray
                                    self.arr_recharge_list = arr.mutableCopy() as! NSMutableArray
                                    self.tableView.reloadData()
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.tableView)
                                self.tableView.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
