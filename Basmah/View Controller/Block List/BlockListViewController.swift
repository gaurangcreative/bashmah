//
//  BlockListViewController.swift
//  Bashmah
//
//  Created by CTIMac on 17/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import SwiftyJSON

class BlockListViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{
//    var providerTitleArry : [String] = ["Rahul Sharma","Michael D'souza", "Rahul Sharma","Michael D'souza","Michael"]
    
    var hud = JGProgressHUD()
    var arr_blocklist:NSMutableArray = NSMutableArray()
    var appDelegate = AppDelegate()
    var to_id:String = String()

    @IBOutlet weak var table_view: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                     BlockList()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_blocklist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.table_view.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
        let dic = JSON(self.arr_blocklist.object(at: indexPath.row))
        
        cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
        cell.img_user.layer.masksToBounds = true
        
        cell.img_user.kf.indicatorType = .activity
        (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
        
        cell.lbl_countryname.text = dic["username"].stringValue.firstUppercased
        cell.lbl_countrycode.text = "ID:" + dic["user_id"].stringValue
        
        cell.btn_blockUnblock.setTitle("Unblock", for: UIControlState.normal)
        cell.btn_blockUnblock.tag = indexPath.row
        
        cell.btn_blockUnblock.layer.cornerRadius = 19
        cell.btn_blockUnblock.layer.masksToBounds = true
        
        cell.btn_blockUnblock.addTarget(self, action: #selector(doclickonUnblock), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_blocklist.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
    
    @objc func doclickonUnblock(sender: UIButton!)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    let dic = JSON(self.arr_blocklist.object(at: sender.tag))
                    to_id = dic["id"].stringValue
                    BlockUser()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    func BlockUser()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "block_uid":to_id as AnyObject,
                    "status":2 as AnyObject
            ]
            
            print(parameters)
            
            let url = kBaseURL + "block_user"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].intValue == 1
                                {
                                    // Add reachability observer
                                    if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                                    {
                                        if reachability.connection != .none
                                        {
                                            if reachability.connection == .wifi || reachability.connection == .cellular
                                            {
                                                self.BlockList()
                                            }
                                        }
                                        else
                                        {
                                            self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                                        }
                                    }
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: Api Call
    func BlockList()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":"3" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["user_list"].array! as NSArray
                                    self.arr_blocklist = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }

}
