//
//  ShowProfileViewController.swift
//  Basmah
//
//  Created by CT on 9/10/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JGProgressHUD
import Kingfisher

class ShowProfileViewController: UIViewController
{
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var lbl_userid: UILabel!
    @IBOutlet weak var lbl_followercount: UILabel!
    @IBOutlet weak var lbl_followingcount: UILabel!
    @IBOutlet weak var view_visitor: UIView!
    @IBOutlet weak var view_activity: UIView!
    @IBOutlet weak var view_message: UIView!
    
    @IBOutlet weak var view_join_room: UIView!
    @IBOutlet weak var view_gift: UIView!
    @IBOutlet weak var lbl_pendingrequest: UILabel!
    
    var arr_user:NSMutableArray = NSMutableArray()
    var appDelegate = AppDelegate()
    var hud = JGProgressHUD()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        view_visitor.layer.cornerRadius = 25.0
        view_visitor.layer.masksToBounds = true
        
        view_activity.layer.cornerRadius = 25.0
        view_activity.layer.masksToBounds = true
        
        view_message.layer.cornerRadius = 25.0
        view_message.layer.masksToBounds = true

        view_gift.layer.cornerRadius = 25.0
        view_gift.layer.masksToBounds = true
        
        view_join_room.layer.cornerRadius = 25.0
        view_join_room.layer.masksToBounds = true
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        img_user.layer.cornerRadius = img_user.frame.size.width/2
        img_user.layer.masksToBounds = true
        img_user.layer.borderWidth = 1.0
        img_user.layer.borderColor = UIColor.white.cgColor
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    UserDetails()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Api Call
    func UserDetails()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":id as AnyObject,
                    "type":"2" as AnyObject,
                    "my_id":"" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_detail"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    //let arr = swiftyJsonVar["userinfo"].array! as NSArray
                                    //self.arr_user = arr.mutableCopy() as! NSMutableArray
                                    //print(self.arr_user)
                                    // let dic = JSON(self.arr_user.object(at: 0))
                                    
                                    let dic = swiftyJsonVar["userinfo"]
                                    self.lbl_userid.text = String(format:"%@%@","ID:",dic["user_id"].stringValue as CVarArg)
                                    self.lbl_username.text = dic["username"].stringValue
                                    
                                    self.lbl_followercount.text = dic["followers"].stringValue
                                    self.lbl_followingcount.text = dic["following"].stringValue
                                    
                                    self.img_user.kf.indicatorType = .activity
                                    (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                    self.img_user.kf.setImage(with: URL(string:dic["user_image"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    self.lbl_pendingrequest.text = String(format:"%@ %d","Pending request",dic["pending_friend_count"].intValue)
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: IBAction
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonPendingRequest(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "PendingRequestViewController") as! PendingRequestViewController
        secondViewController.pendingcount = self.lbl_pendingrequest.text as! String
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonMyRoom(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyRoomViewController") as! MyRoomViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonJoinedRooms(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "TagRoomListViewController") as! TagRoomListViewController
        secondViewController.checkController = "Profile"
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonGift(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "GiftsViewController") as! GiftsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonEditProfile(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        secondViewController.checkController = "Profile"
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonVistors(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "VisitorViewController") as! VisitorViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonFollowers(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "FollowerViewController") as! FollowerViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonFollowing(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "FollowingViewController") as! FollowingViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
