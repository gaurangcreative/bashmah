//
//  RoomGiftReceivedTableViewCell.swift
//  Basmah
//
//  Created by CT on 12/26/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class RoomGiftReceivedTableViewCell: UITableViewCell
{
//    @IBOutlet weak var view_outerbg: UIView!
//    @IBOutlet weak var view_numberbg: UIView!
//    @IBOutlet weak var lbl_numbercount: UILabel!
//    @IBOutlet weak var img_user: UIImageView!
//    @IBOutlet weak var lbl_price: UILabel!
//    @IBOutlet weak var lbl_username: UILabel!

    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var view_outerbg: Shadow!
    @IBOutlet weak var view_numberbg: UIView!
    @IBOutlet weak var lbl_numbercount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
