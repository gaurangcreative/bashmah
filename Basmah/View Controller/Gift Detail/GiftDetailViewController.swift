//
//  GiftDetailViewController.swift
//  Basmah
//
//  Created by CT on 10/6/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD
import Kingfisher

class GiftDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var segment_control: UISegmentedControl!
    @IBOutlet weak var sgment_received: UISegmentedControl!
    
    @IBOutlet weak var btnRoomgift: UIButton!
    @IBOutlet weak var btnRoomgiftReceived: UIButton!
    @IBOutlet weak var img_giftSent: UIImageView!
    @IBOutlet weak var img_giftReceived: UIImageView!
    @IBOutlet weak var table_giftsent: UITableView!
    @IBOutlet weak var table_giftreceived: UITableView!
    @IBOutlet weak var view_giftSent: UIView!
    @IBOutlet weak var view_giftReceived: UIView!
    
    var type:String = String()
    var selectedtypeGiftSent:String = String()
    var selectedtypeGiftReceived:String = String()

    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_giftsent:NSMutableArray = NSMutableArray()
    var arr_giftReceived:NSMutableArray = NSMutableArray()
    var arr_numbercount:NSMutableArray = NSMutableArray()
    var arr_numberReccount:NSMutableArray = NSMutableArray()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        segment_control.backgroundColor = UIColor.white
        segment_control.tintColor = UIColor.uicolorFromHex(0x4650F9, alpha: 1)
        
        sgment_received.backgroundColor = UIColor.white
        sgment_received.tintColor = UIColor.uicolorFromHex(0x4650F9, alpha: 1)
        
        if (self.type == "1")
        {
            img_giftSent.isHidden = false
            img_giftReceived.isHidden = true
            segment_control.isHidden = false
            sgment_received.isHidden = true
            
            view_giftSent.isHidden = false
            view_giftReceived.isHidden = true
            
            btnRoomgift.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 16)
            btnRoomgiftReceived.titleLabel?.font =  UIFont(name: "Lato-Regular", size: 16)
            
            selectedtypeGiftSent = "1"
            Gift_sent()
        }
        else
        {
            img_giftReceived.isHidden = false
            img_giftSent.isHidden = true
            segment_control.isHidden = true
            sgment_received.isHidden = false

            view_giftSent.isHidden = true
            view_giftReceived.isHidden = false
            
            btnRoomgiftReceived.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 16)
            btnRoomgift.titleLabel?.font =  UIFont(name: "Lato-Regular", size: 16)
            
            selectedtypeGiftReceived = "1"
            GiftReceived()
        }
    }
    
    //MARK:IBAction
    @IBAction func doclickonSegment(_ sender: UISegmentedControl)
    {
        if (self.type == "1")
        {
            switch sender.selectedSegmentIndex
            {
                case 0:selectedtypeGiftSent = "1"
                print("Daily")
                Gift_sent()
                
                case 1:selectedtypeGiftSent = "2"
                print("Weekly")
                Gift_sent()
                
                case 2:selectedtypeGiftSent = "3"
                print("Monthly")
                Gift_sent()
                
                default:print("test")
            }
        }
//        else
//        {
//            switch sender.selectedSegmentIndex
//            {
//                case 0:selectedtypeGiftReceived = "1"
//                print("Daily")
//                GiftReceived()
//
//                case 1:selectedtypeGiftReceived = "2"
//                print("Weekly")
//                GiftReceived()
//
//                case 2:selectedtypeGiftReceived = "3"
//                print("Monthly")
//                GiftReceived()
//
//                default:print("test")
//            }
//        }
    }
    
    @IBAction func doclickonSegmentReceived(_ sender: UISegmentedControl)
    {
        switch sender.selectedSegmentIndex
        {
            case 0:selectedtypeGiftReceived = "1"
            print("Daily")
            GiftReceived()
            
            case 1:selectedtypeGiftReceived = "2"
            print("Weekly")
            GiftReceived()
            
            case 2:selectedtypeGiftReceived = "3"
            print("Monthly")
            GiftReceived()
            
            default:print("test")
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonRoomGiftSent(_ sender: Any)
    {
        img_giftSent.isHidden = false
        img_giftReceived.isHidden = true

        type = "1"
        
        view_giftSent.isHidden = false
        view_giftReceived.isHidden = true

        btnRoomgift.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 16)
        btnRoomgiftReceived.titleLabel?.font =  UIFont(name: "Lato-Regular", size: 16)
        
        segment_control.isHidden = false
        sgment_received.isHidden = true
    }
    
    @IBAction func doclickonRoomGiftReceived(_ sender: Any)
    {
        img_giftReceived.isHidden = false
        img_giftSent.isHidden = true
        
        type = "2"
        
        view_giftSent.isHidden = true
        view_giftReceived.isHidden = false

        btnRoomgiftReceived.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 16)
        btnRoomgift.titleLabel?.font =  UIFont(name: "Lato-Regular", size: 16)
        
        segment_control.isHidden = true
        sgment_received.isHidden = false
    }
    
    //MARK:Table View Datasource
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (tableView == table_giftsent)
        {
            return self.arr_giftsent.count
        }
        else
        {
            return arr_giftReceived.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (tableView == table_giftsent)
        {
            let cell:RoomGiftTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RoomGift", for: indexPath) as! RoomGiftTableViewCell
            
            cell.view_outerbg.layer.cornerRadius = cell.view_outerbg.frame.size.width/2
            cell.view_outerbg.layer.masksToBounds = true
            
            cell.view_numberbg.layer.cornerRadius = cell.view_numberbg.frame.size.width/2
            cell.view_numberbg.layer.masksToBounds = true
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            let dic = JSON(arr_giftsent.object(at: indexPath.row))

            cell.lbl_price.text = dic["total_price"].stringValue
            cell.lbl_username.text = dic["username"].stringValue
            
            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            if (self.arr_giftsent.count > 0)
            {
                let index = arr_giftsent.count
                self.arr_numbercount.add(index)
                let number  = self.arr_numbercount[indexPath.row]
                cell.lbl_numbercount.text = String("\(number)")
            }
            
            return cell
        }
        else
        {
            let cell:RoomGiftReceivedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GiftReceived", for: indexPath) as! RoomGiftReceivedTableViewCell
            
            cell.view_outerbg.layer.cornerRadius = cell.view_outerbg.frame.size.width/2
            cell.view_outerbg.layer.masksToBounds = true
            
            cell.view_numberbg.layer.cornerRadius = cell.view_numberbg.frame.size.width/2
            cell.view_numberbg.layer.masksToBounds = true
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            let dic = JSON(arr_giftReceived.object(at: indexPath.row))
            
            cell.lbl_price.text = dic["total_price"].stringValue
            cell.lbl_username.text = dic["username"].stringValue
            
            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            if (self.arr_giftReceived.count > 0)
            {
                let index = arr_giftReceived.count
                self.arr_numberReccount.add(index)
                let number  = self.arr_numberReccount[indexPath.row]
                cell.lbl_numbercount.text = String("\(number)")
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 140
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Gift Sent
    func Gift_sent()
    {
            let parameters: Parameters =
                [
                    "type":selectedtypeGiftSent as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "gift_sent"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    
                                    let arr = swiftyJsonVar["user_list"].array! as NSArray
                                    self.arr_giftsent = arr.mutableCopy() as! NSMutableArray
                                    print(self.arr_giftsent)
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSectionsTabl(in: self.table_giftsent)
                                self.table_giftsent.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
    }
    
    //MARK:Gift Received
    func GiftReceived()
    {
        let parameters: Parameters =
            [
                "type":selectedtypeGiftReceived as AnyObject,
            ]
        
        print(parameters)
        
        let url = kBaseURL + "gift_received"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            self.hud.dismiss()
            
            switch result
            {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print(response.response!)
                    print(response.data!)
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                self.hud.dismiss()
                                
                                let arr = swiftyJsonVar["user_list"].array! as NSArray
                                self.arr_giftReceived = arr.mutableCopy() as! NSMutableArray
                                print(self.arr_giftReceived)
                            }
                            else
                            {
                                self.hud.dismiss()
                            }
                            
                            self.numberOfSectionsTabl(in: self.table_giftreceived)
                            self.table_giftreceived.reloadData()
                        }
                    }
                }
                
                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    func numberOfSectionsTabl(in tableView: UITableView) -> Int
    {
        if (tableView == table_giftsent)
        {
            var numOfSections: Int = 0
            
            if arr_giftsent.count > 0
            {
                numOfSections = 1
                table_giftsent.backgroundView = nil
                table_giftsent.separatorStyle = .singleLine
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: table_giftsent.bounds.size.width, height: table_giftsent.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                table_giftsent.backgroundView  = noDataLabel
                table_giftsent.separatorStyle = .none
            }
            
            return numOfSections
        }
        else
        {
            var numOfSections: Int = 0
            
            if arr_giftReceived.count > 0
            {
                numOfSections = 1
                table_giftreceived.backgroundView = nil
                table_giftreceived.separatorStyle = .singleLine
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: table_giftreceived.bounds.size.width, height: table_giftreceived.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                table_giftreceived.backgroundView  = noDataLabel
                table_giftreceived.separatorStyle = .none
            }
            
            return numOfSections
        }
    }
}

extension UIColor
{
    class func uicolorFromHex(_ rgbValue:UInt32, alpha : CGFloat)->UIColor
    {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8) / 255.0
        let blue = CGFloat(rgbValue & 0xFF) / 255.0
        return UIColor(red:red, green:green, blue:blue, alpha: alpha)
    }
}
