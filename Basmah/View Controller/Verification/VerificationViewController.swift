//
//  VerificationViewController.swift
//  Basmah
//
//  Created by CT on 9/4/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class VerificationViewController: UIViewController
{
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var user_num:String = String()

    @IBOutlet weak var txt_verificationcode: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_confirmpassword: UITextField!
    @IBOutlet weak var lbl_number: UILabel!
    @IBOutlet weak var btnSignUp: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        appDelegate = UIApplication.shared.delegate as! AppDelegate

        txt_verificationcode.attributedPlaceholder = NSAttributedString(string: "Enter Verification Code",
                                                                   attributes: [NSAttributedStringKey.foregroundColor:
                                                                    UIColor.lightGray])
        
        txt_password.attributedPlaceholder = NSAttributedString(string: "Enter Password",
                                                                        attributes: [NSAttributedStringKey.foregroundColor:
                                                                            UIColor.lightGray])
        
        txt_confirmpassword.attributedPlaceholder = NSAttributedString(string: "Enter Confirm Password",
                                                                attributes: [NSAttributedStringKey.foregroundColor:
                                                                    UIColor.lightGray])
        
        btnSignUp.layer.cornerRadius = 20.0
        btnSignUp.layer.masksToBounds = true
        lbl_number.text = user_num
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    @IBAction func doclickonSignUp(_ sender: Any)
    {
        if (txt_verificationcode.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter verification code",buttonTitle: "OK");
        }
        else if (txt_password.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter password",buttonTitle: "OK");
        }
        else if (txt_confirmpassword.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter confirm password",buttonTitle: "OK");
        }
        else
        {
            if (txt_password.text == txt_confirmpassword.text)
            {
                // Add reachability observer
                if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                {
                    if reachability.connection != .none
                    {
                        if reachability.connection == .wifi || reachability.connection == .cellular
                        {
                            self.Verify()
                        }
                    }
                    else
                    {
                        self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                    }
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Password and confirm password does not matched",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Api Call
    func Verify()
    {
        let parameters: Parameters =
            [
                "otp":txt_verificationcode.text as AnyObject,
                "phone": user_num  as AnyObject,
                "password":txt_password.text as AnyObject,
            ]
        
        print(parameters)
        
        let url = kBaseURL + "verification"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            self.hud.dismiss()
            
            switch result
            {
                case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print(response.response!)
                    print(response.data!)
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                let data = swiftyJsonVar["userinfo"]
                                
                                let UserDefault = UserDefaults.standard
                                UserDefault.set(data["id"].stringValue,forKey:"id")
                                UserDefault.set(data["user_id"].stringValue,forKey:"user_id")
                                UserDefault.set(data["username"].stringValue,forKey:"username")
                                
                                let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                                profile.user_country = data["user_country"].stringValue
                                self.navigationController?.pushViewController(profile, animated: true)
                            }
                            else
                            {
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                }
                
                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
}
