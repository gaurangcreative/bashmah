//
//  UniqueIDViewController.swift
//  Bashmah
//
//  Created by CTIMac on 11/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class UniqueIDViewController: UIViewController , UITableViewDataSource, UITableViewDelegate
{
    var providerTitleArry : [String] = ["100","200", "2000","150"]
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_uniqueid_list:NSMutableArray = NSMutableArray()
    var unique_id:String = String()
    var price:String = String()
    var uiColorArray = [UIColor]()
    
    @IBOutlet weak var lbl_currentid: UILabel!
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var view_bg: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        let defaults: UserDefaults? = UserDefaults.standard
//
//        if(defaults?.dictionaryRepresentation().keys.contains("user_id"))!
//        {
//            let user_id = UserDefaults.standard.value(forKey: "user_id")
//            lbl_currentid.text = user_id as! String
//        }
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    UniqueidList()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonRechargeList(_ sender: Any)
    {
        let recharge = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
        self.navigationController?.pushViewController(recharge, animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arr_uniqueid_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CountryListTableViewCell = self.table_View.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
        cell.view_bg.layer.shadowColor = UIColor.lightGray.cgColor
        cell.view_bg.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cell.view_bg.layer.shadowOpacity = 0.5
        cell.view_bg.layer.shadowRadius = 2.0
        cell.view_bg.layer.masksToBounds = false
        
        let dic = JSON(self.arr_uniqueid_list.object(at: indexPath.row))
        
        cell.view_bgcolor.backgroundColor = self.uiColorArray[indexPath.row]
        cell.lbl_uniqueid.text = dic["unique_id"].stringValue
        cell.lbl_price.text = dic["price"].stringValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let title = "Top Unique ID"
        return title
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let refreshAlert = UIAlertController(title: "Basmah", message: "Confirm to Purchase Unique Id?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        let dic = JSON(self.arr_uniqueid_list.object(at: indexPath.row))
                        self.unique_id = dic["unique_id"].stringValue
                        print(self.unique_id)
                        
                        self.price = dic["price"].stringValue
                        print(self.price)
                        
                       self.add_uniqueid()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    //MARK: Api Call
    func add_uniqueid()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "unique_id":self.unique_id as AnyObject,
                    "price":self.price as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "add_unique_id"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.navigationController?.popViewController(animated: true)
                                    
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: Webservice Call
    func UniqueidList()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "unique_id_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["unique_id_list"].array! as NSArray
                                    self.arr_uniqueid_list = arr.mutableCopy() as! NSMutableArray
                                    
                                    self.lbl_currentid.text = swiftyJsonVar["user_unique_id"].stringValue as String
                                    
                                    var i = 0
                                    
                                    while i < self.arr_uniqueid_list.count
                                    {
                                        self.uiColorArray.append(UIColor(red: 171/255.0, green: 84/255.0, blue: 249/255.0, alpha: 1.0) )
                                        
                                        self.uiColorArray.append(UIColor(red: 82/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0) )
                                        
                                        self.uiColorArray.append(UIColor(red: 245/255.0, green: 58/255.0, blue: 111/255.0, alpha: 1.0) )
                                        
                                        i = i + 1
                                    }
                                    
                                    self.table_View.reloadData()
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.table_View)
                                self.table_View.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_uniqueid_list.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
}
