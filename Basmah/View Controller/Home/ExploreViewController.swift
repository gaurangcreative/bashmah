//
//  ExploreViewController.swift
//  Basmah
//
//  Created by CT on 9/5/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class ExploreViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var collection_gift: UICollectionView!
    @IBOutlet weak var collection_country: UICollectionView!
    @IBOutlet weak var collection_tag: UICollectionView!
    @IBOutlet weak var collection_recommend: UICollectionView!
    @IBOutlet weak var collection_newlycreated: UICollectionView!
    @IBOutlet weak var scroll_view: UIScrollView!
    
    var arr_country:NSMutableArray = NSMutableArray()
    var arr_tag:NSMutableArray = NSMutableArray()
    var arr_newlycreate:NSMutableArray = NSMutableArray()
    var myArray = [String?]()
    var uiColorArray = [UIColor]()
    var refreshControl: UIRefreshControl!
    var boolLoader:Bool = Bool()
  
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    
    private let arr_topgift = ["Room Gifts","Gifts Received"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(red: 151/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0)
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                   refreshControl.addTarget(self, action: #selector(Explore_List), for: .valueChanged)
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
        
        if #available(iOS 10.0, *)
        {
            scroll_view.refreshControl = refreshControl
        }
        else
        {
            // Fallback on earlier versions
        }
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    Explore_List()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
       
    }
    
    
    @IBAction func doclickonCreateGroup(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    //MARK: Api Call
   @objc func Explore_List()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "explore_page"
            
            if(boolLoader == false)
            {
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                boolLoader = true
            }
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    self.refreshControl.endRefreshing()
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                    
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["country_list"].array! as NSArray
                                    self.arr_country = arr.mutableCopy() as! NSMutableArray
                                    
                                    let arr_temp = swiftyJsonVar["tag_list"].array! as NSArray
                                    self.arr_tag = arr_temp.mutableCopy() as! NSMutableArray
                                    
                                    var i = 0
                                    
                                    while i < self.arr_tag.count
                                    {
                                        let dic = JSON(self.arr_tag.object(at:i))
                                        self.myArray.append("#" + dic["tag_name"].stringValue)
                                        
                                        self.uiColorArray.append(UIColor(red: 171/255.0, green: 84/255.0, blue: 249/255.0, alpha: 1.0) )
                                        
                                        self.uiColorArray.append(UIColor(red: 82/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0) )
                                        
                                        self.uiColorArray.append(UIColor(red: 245/255.0, green: 58/255.0, blue: 111/255.0, alpha: 1.0) )
                                        
                                        i = i + 1
                                    }
                                    
                                    let arrnew = swiftyJsonVar["newly_created"].array! as NSArray
                                    self.arr_newlycreate = arrnew.mutableCopy() as! NSMutableArray
                                    
                                    self.collection_country.reloadData()
                                    self.collection_tag.reloadData()
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSectionsColl(in: self.collection_newlycreated)
                                self.collection_newlycreated.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Collection View Data Source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if (collectionView == collection_gift)
        {
            return arr_topgift.count
        }
        else if (collectionView == collection_country)
        {
            return arr_country.count
        }
        else if (collectionView == collection_tag)
        {
           return self.arr_tag.count
        }
        else if (collectionView == collection_recommend)
        {
            return 5
        }
        else
        {
            return self.arr_newlycreate.count
        }
    }
    
    func numberOfSections(in collectionview: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if (collectionView == collection_gift)
        {
            let cell:TopGiftCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! TopGiftCollectionViewCell
            
            if (indexPath.item == 0)
            {
                cell.img_gift.backgroundColor = UIColor(red: 82/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0)
            }
            else if (indexPath.item == 1)
            {
                cell.img_gift.backgroundColor = UIColor(red: 171/255.0, green: 84/255.0, blue: 249/255.0, alpha: 1.0)
            }
            else
            {
                cell.img_gift.backgroundColor = UIColor(red: 245/255.0, green: 58/255.0, blue: 111/255.0, alpha: 1.0)
            }
            
            cell.lbl_title.text = arr_topgift[indexPath.item]
            
            cell.img_gift.layer.cornerRadius = cell.img_gift.frame.size.width/2
            cell.img_gift.layer.masksToBounds = true
            cell.img_gift.layer.borderWidth = 1.0
            cell.img_gift.layer.borderColor = UIColor.white.cgColor
            return cell
        }
        else if(collectionView == collection_country)
        {
            let cell:CountryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CountryCell", for: indexPath as IndexPath) as! CountryCollectionViewCell
            
            let dic = JSON(arr_country.object(at: indexPath.item))

            cell.img_country.layer.cornerRadius = cell.img_country.frame.size.width/2
            cell.img_country.layer.masksToBounds = true
            cell.img_country.layer.borderWidth = 1.0
            cell.img_country.layer.borderColor = UIColor.white.cgColor
            
            cell.lbl_countryname.text = dic["name"].stringValue.firstUppercased
            
            cell.img_country.kf.indicatorType = .activity
            (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))
            
            return cell
        }
        else if (collectionView == collection_tag)
        {
            let cell:RoomCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoomCell", for: indexPath as IndexPath) as! RoomCollectionViewCell
            
            cell.lbl_title.text = self.myArray[indexPath.item]
            
//            cell.layer.cornerRadius = 8.0
//            cell.layer.masksToBounds = true
            
            cell.lbl_title.backgroundColor = self.uiColorArray[indexPath.item]
            return cell
        }
        else if (collectionView == collection_recommend)
        {
            let cell:RecommendedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RECell", for: indexPath as IndexPath) as! RecommendedCollectionViewCell
            
            return cell
        }
        else
        {
            let cell:NewlyCreatedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCell", for: indexPath as IndexPath) as! NewlyCreatedCollectionViewCell
            
            cell.lbl_tag.layer.cornerRadius = 10.0
            cell.lbl_tag.layer.masksToBounds = true

            let dic = JSON(arr_newlycreate.object(at: indexPath.item))
            
            cell.img_flag.kf.indicatorType = .activity
            (cell.img_flag.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_flag.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))

            cell.lbl_title.text = dic["room_name"].stringValue
            cell.lbl_tag.text = dic["tags"].stringValue
            cell.lbl_desc.text = dic["announcement"].stringValue
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if (collectionView == collection_gift)
        {
            //return CGSize(width:self.view.frame.size.width/3,height:self.view.frame.size.width/3)
            
            let padding: CGFloat =  100
            let collectionViewSize = collectionView.frame.size.width - padding
            return CGSize(width: collectionViewSize/2, height: 112)
        }
        else if(collectionView == collection_country)
        {
            return CGSize(width:self.view.frame.size.width/3-16,height:self.view.frame.size.width/3-16)
        }
        else if (collectionView == collection_tag)
        {
            let dic = JSON(arr_tag.object(at: indexPath.item))
            let size = (dic["tag_name"].stringValue).size(withAttributes: nil)
            print(size.width)
            
            return CGSize(width:size.width + 10,height:self.view.frame.size.width/4-25)
        }
        else  if (collectionView == collection_recommend)
        {
            return CGSize(width:self.view.frame.size.width-20,height:120)
        }
        else
        {
            return CGSize(width:self.view.frame.size.width-20,height:120)
        }
    }
    
    func numberOfSectionsColl(in tableView: UICollectionView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_newlycreate.count > 0
        {
            numOfSections = 1
            collection_newlycreated.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collection_newlycreated.bounds.size.width, height: collection_newlycreated.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            collection_newlycreated.backgroundView  = noDataLabel
        }
        
        return numOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if (collectionView == collection_gift)
        {
            if (indexPath.item == 0)
            {
                let coun = self.storyboard?.instantiateViewController(withIdentifier: "GiftDetailViewController") as! GiftDetailViewController
                coun.type = "1"
                self.navigationController?.pushViewController(coun, animated: true)
            }
            else
            {
                let coun = self.storyboard?.instantiateViewController(withIdentifier: "GiftDetailViewController") as! GiftDetailViewController
                coun.type = "2"
                self.navigationController?.pushViewController(coun, animated: true)
            }
            
        }
        else if(collectionView == collection_country)
        {
            let dic = JSON(arr_country.object(at: indexPath.item))
            let coun = self.storyboard?.instantiateViewController(withIdentifier: "CountryRoomListViewController") as! CountryRoomListViewController
            coun.country_id = dic["id"].stringValue
            coun.country_name = dic["name"].stringValue
            self.navigationController?.pushViewController(coun, animated: true)
        }
        else if(collectionView == collection_tag)
        {
            let dic = JSON(arr_tag.object(at: indexPath.item))
            let tag = self.storyboard?.instantiateViewController(withIdentifier: "TagRoomListViewController") as! TagRoomListViewController
            tag.tag_id = dic["tag_id"].stringValue
            tag.tag_name = dic["tag_name"].stringValue
            self.navigationController?.pushViewController(tag, animated: true)
        }
    }
}
