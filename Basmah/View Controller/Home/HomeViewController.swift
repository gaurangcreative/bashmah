//
//  HomeViewController.swift
//  Basmah
//
//  Created by CT on 9/4/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController
{
    var pageMenu : CAPSPageMenu?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "PAGE MENU"
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "HomeListViewController") as! HomeListViewController
        vc1.title = "New"
    
        controllerArray.append(vc1)
        
        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "HomeListViewController") as! HomeListViewController
        vc2.title = "All"
        controllerArray.append(vc2)
        
        let vc3 = self.storyboard?.instantiateViewController(withIdentifier: "ExploreViewController") as! ExploreViewController
        vc3.title = "Explore"
        controllerArray.append(vc3)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(UIColor(red: 151/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor.clear),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 15.0)!),
            .menuHeight(40.0),
            .menuItemWidth(self.view.frame.size.width/2-20),
            .centerMenuItems(true),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            //.selectedMenuItemLabelColor(UIColor.white),
            .selectionIndicatorHeight(1.0)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 60.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: self)

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func doclickonSearch(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
