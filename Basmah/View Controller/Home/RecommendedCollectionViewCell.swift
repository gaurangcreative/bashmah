//
//  RecommendedCollectionViewCell.swift
//  Basmah
//
//  Created by CT on 9/24/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class RecommendedCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var img_country: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_tag: UILabel!
    @IBOutlet weak var lbl_usercount: UILabel!
    @IBOutlet weak var lbl_desc: UILabel!
}
