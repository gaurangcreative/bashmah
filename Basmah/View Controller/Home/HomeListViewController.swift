//
//  HomeListViewController.swift
//  Basmah
//
//  Created by CT on 9/4/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class HomeListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var type:String = String()
    var hud = JGProgressHUD()
    var arr_new:NSMutableArray = NSMutableArray()
    var arr_all:NSMutableArray = NSMutableArray()
    var appDelegate = AppDelegate()
    var refreshControl: UIRefreshControl!
    var boolLoader:Bool = Bool()

    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var table_view: UITableView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(red: 151/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0)
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    refreshControl.addTarget(self, action: #selector(Roomlist), for: .valueChanged)
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
        
        if #available(iOS 10.0, *)
        {
            table_view.refreshControl = refreshControl
        }
        else
        {
            // Fallback on earlier versions
        }

        if(self.title == "New")
        {
            type = "2"
            
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                         self.Roomlist()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
        else
        {
            type = "1"
            
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        self.Roomlist()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
     //MARK:IBActions
    @IBAction func doclickonCreateGroup(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:Table View Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.title == "New")
        {
            return arr_new.count
        }
        else
        {
             return arr_all.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(self.title == "New")
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeTableViewCell
            
            cell.selectionStyle = .none
            
            let dic = JSON(self.arr_new.object(at: indexPath.row))
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            cell.lbl_friends.layer.cornerRadius = 10.0
            cell.lbl_friends.layer.masksToBounds = true
            
//            if (dic["image"].stringValue == "")
//            {
//                cell.img_user.image = #imageLiteral(resourceName: "visitors_icon")
//            }
//            else
//            {
                cell.img_user.kf.indicatorType = .activity
                (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
                
                cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
                cell.img_user.layer.masksToBounds = true
           // }
            
            cell.img_country.kf.indicatorType = .activity
            (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))

            cell.lbl_title.text = dic["room_name"].stringValue.firstUppercased
            cell.lbl_friends.text = dic["tags"].stringValue
            cell.lbl_desc.text = dic["announcement"].stringValue
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeTableViewCell
            
            cell.selectionStyle = .none
            
            let dic = JSON(self.arr_all.object(at: indexPath.row))
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            cell.lbl_friends.layer.cornerRadius = 10.0
            cell.lbl_friends.layer.masksToBounds = true
            
            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            cell.img_country.kf.indicatorType = .activity
            (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))
            
            cell.lbl_title.text = dic["room_name"].stringValue.firstUppercased
            cell.lbl_friends.text = dic["tags"].stringValue
            
            cell.lbl_desc.text = dic["announcement"].stringValue
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if(self.title == "New")
        {
            var numOfSections: Int = 0
            
            if arr_new.count > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections = 1
                tableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
        }
        else
        {
            var numOfSections: Int = 0
            
            if arr_all.count > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections = 1
                tableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            
            return numOfSections
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(self.title == "New")
        {
            let dic = JSON(self.arr_new.object(at: indexPath.row))
            
            let group = self.storyboard?.instantiateViewController(withIdentifier: "GroupChatViewController") as! GroupChatViewController
            group.room_id = dic["id"].stringValue
            self.navigationController?.pushViewController(group, animated: true)
        }
        else
        {
            let dic = JSON(self.arr_all.object(at: indexPath.row))
            let group = self.storyboard?.instantiateViewController(withIdentifier: "GroupChatViewController") as! GroupChatViewController
            group.room_id = dic["id"].stringValue
            self.navigationController?.pushViewController(group, animated: true)
        }
    }
    
    //MARK:Api Call
    @objc func Roomlist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":self.type as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "room_list"
            
            if(boolLoader == false)
            {
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                boolLoader = true
            }
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                        
                    self.refreshControl.endRefreshing()
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    
                                    if(self.type == "2")
                                    {
                                        let arr = swiftyJsonVar["room_list"].array! as NSArray
                                        self.arr_new = arr.mutableCopy() as! NSMutableArray
                                    }
                                    else
                                    {
                                        let arr = swiftyJsonVar["room_list"].array! as NSArray
                                        self.arr_all = arr.mutableCopy() as! NSMutableArray
                                    }
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

//}
