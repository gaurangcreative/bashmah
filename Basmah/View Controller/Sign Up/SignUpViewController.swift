//
//  SignUpViewController.swift
//  Basmah
//
//  Created by CT on 9/4/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import SwiftyJSON

class SignUpViewController: UIViewController
{
    var checkcontroller:String = String()
    var phone_code:String = String()
    var country_name:String = String()
    var country_image:String = String()
    var country_id:String = String()
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()

    @IBOutlet weak var txt_phonenumber: UITextField!
    @IBOutlet weak var img_country: UIImageView!
    @IBOutlet weak var btnSelectCountry: UIButton!
    @IBOutlet weak var lbl_code: UILabel!
    @IBOutlet weak var btnNext: UIButton!

    override func viewDidLoad()
    {
        super.viewDidLoad()

        appDelegate = UIApplication.shared.delegate as! AppDelegate

        txt_phonenumber.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                            attributes: [NSAttributedStringKey.foregroundColor:
                                                                UIColor.lightGray])

        btnNext.layer.cornerRadius = 20.0
        btnNext.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        if (checkcontroller == "Country")
        {
            btnSelectCountry.setTitle("         " + country_name, for: UIControlState.normal)
            btnSelectCountry.setTitleColor(UIColor.black, for: UIControlState.normal)
            
            //img_country
            
            self.img_country.kf.indicatorType = .activity
            (self.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            self.img_country.kf.setImage(with: URL(string:self.country_image), placeholder: UIImage(named: ""))

            lbl_code.text =  phone_code
            print(country_id)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBAction
    @IBAction func doclickonSelectCountry(_ sender: Any)
    {
        //cpvInternal.showCountriesList(from: self)
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CountryListViewController") as! CountryListViewController
        secondViewController.checkController = "Signup"
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonNext(_ sender: Any)
    {
        if(btnSelectCountry.titleLabel?.text == "Select Country")
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please select country",buttonTitle: "OK");
        }
        else if (txt_phonenumber.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter phone number",buttonTitle: "OK");
        }
        else
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        self.SignUp()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(profile, animated: false)
    }
    
    //MARK: Api Call
    func SignUp()
    {
        let parameters: Parameters =
            [
                "country":phone_code as AnyObject,
                "phone":txt_phonenumber.text  as AnyObject,
                "android_token":"" as AnyObject,
                "ios_token":self.appDelegate.device_token as AnyObject,
                "login_type":"1",
            ]
        
        print(parameters)
        
        let url = kBaseURL + "signup"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            self.hud.dismiss()
            
            switch result
            {
                case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print(response.response!)
                    print(response.data!)
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                let data = swiftyJsonVar["userinfo"]
                                
                                //let UserDefault = UserDefaults.standard
                                //UserDefault.set(data["id"].stringValue,forKey:"id")
                                //UserDefault.set(data["user_id"].stringValue,forKey:"user_id")
                                
                                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
                                secondViewController.user_num = data["phone"].stringValue
                                self.navigationController?.pushViewController(secondViewController, animated: true)
                            }
                            else
                            {
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                }
                
                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
}
