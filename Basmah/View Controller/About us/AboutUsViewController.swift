//
//  AboutUsViewController.swift
//  Basmah
//
//  Created by CT on 9/20/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import WebKit
import JGProgressHUD

class AboutUsViewController: UIViewController
{
    @IBOutlet weak var displayView: UIView!
    
    var webView: WKWebView!
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate

        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    DispatchQueue.main.async
                        {
                            self.initialSetup()
                        }
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    func initialSetup()
    {
        self.webView = WKWebView(frame: displayView.bounds, configuration: WKWebViewConfiguration())
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.isUserInteractionEnabled = true
        self.webView.navigationDelegate = self as? WKNavigationDelegate
        self.displayView.addSubview(self.webView)
        let request = URLRequest(url: URL(string: "https://ctinfotech.com/CT06/basmah/about.php")!)
        self.webView.load(request)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AboutUsViewController: WKUIDelegate, WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       // Loader.hideLoader()
        self.hud.dismiss()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        //Loader.hideLoader()
         self.hud.dismiss()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!)
    {
       // Loader.showLoader("Loading...")
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
    }
}
