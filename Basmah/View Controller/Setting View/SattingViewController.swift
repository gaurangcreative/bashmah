//
//  SattingViewController.swift
//  Bashmah
//
//  Created by CTIMac on 20/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SattingViewController: UIViewController, UITableViewDataSource , UITableViewDelegate {

   // @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var tableView_Satting: UITableView!
    @IBOutlet weak var img_bg: UIImageView!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var table_view_RoomStatus: UITableView!
    
    var providerTitleArry : [String] = ["Private chat notification","New approval to be my friend", "View my in-room status","Block list","Clear all chatting history"]
    
    var arrStatus : [String] = ["Everyone","Friends only","Friends & Followers only"]
    
    var statusint : Int = Int()
    var status : String = String()
    var status2 : String = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(handleTap))
        img_bg.isUserInteractionEnabled = true
        img_bg.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleTap()
    {
        img_bg.isHidden = true
        view_bg.isHidden = true
        // handling code
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView == tableView_Satting)
        {
            return providerTitleArry.count
        }
        else
        {
            return arrStatus.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == tableView_Satting)
        {
             let cell = self.tableView_Satting.dequeueReusableCell(withIdentifier: "Cell") as! HomeTableViewCell
        
            cell.lblTitleName?.text = providerTitleArry[indexPath.row]
        
            if indexPath.row == 0
            {
                cell.switch1.isHidden = false
            }
            else if indexPath.row == 1
            {
                cell.switch1.isHidden = false
            }
            else if indexPath.row == 2
            {
                cell.lblStatus.isHidden = false
                cell.lblStatus.text = status
            }
            else if indexPath.row == 3
            {
                cell.img_arrow.isHidden = false
            }
            else if indexPath.row == 4
            {
                cell.img_arrow.isHidden = false
            }
            
            return cell
        }
        else
        {
            let cell = self.table_view_RoomStatus.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
            
            cell.lbl_countryname?.text = arrStatus[indexPath.row]
            
            if indexPath.row == statusint
            {
                cell.img_user?.image = #imageLiteral(resourceName: "radio_active")
            }
            else
            {
                 cell.img_user?.image = #imageLiteral(resourceName: "radio_deactive")
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == tableView_Satting)
        {
            if indexPath.row == 2
           {
              img_bg.isHidden = false
              view_bg.isHidden = false
              print("\(indexPath.row)")
           }
           if indexPath.row == 3
           {
               let blocklist = self.storyboard?.instantiateViewController(withIdentifier: "BlockListViewController") as? BlockListViewController
              self.navigationController?.pushViewController(blocklist!, animated: true)
          }
        }
        else
        {
            if indexPath.row == 0
            {
                statusint = 0
                status = "Everyone"
            }
             if indexPath.row == 1
            {
                statusint = 1
                status = "Friends only"
            }
            if indexPath.row == 2
            {
                statusint = 2
                status = "Friends & Followers only"
            }
           
            table_view_RoomStatus.reloadData()
            tableView_Satting.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 45
    }

    @IBAction func doclickonSave(_ sender: Any)
    {
        img_bg.isHidden = true
        view_bg.isHidden = true
    }
}
