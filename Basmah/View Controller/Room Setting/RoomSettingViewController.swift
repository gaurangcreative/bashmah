//
//  RoomSettingViewController.swift
//  Basmah
//
//  Created by CTIMac on 01/10/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class RoomSettingViewController: UIViewController
{
    @IBOutlet weak var view_ButtonBG: UIView!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var view_imgbg: GradientView!
    @IBOutlet weak var txt_room: UITextField!
    @IBOutlet weak var txt_announcement: UITextField!
    @IBOutlet weak var txt_permission: UITextField!
    @IBOutlet weak var txt_membershipfee: UITextField!
    @IBOutlet weak var btnTag: UIButton!
    
    var tagid:String = String()
    var tagname:String = String()
    var room_id:String = String()
    var appDelegate = AppDelegate()
    var hud = JGProgressHUD()
    let imagePicker = UIImagePickerController()
    var chosenImage = UIImage()
    var image_Data = UIImageJPEGRepresentation(UIImage(),0)

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        imagePicker.delegate = self

        self.view_imgbg.layer.cornerRadius = self.view_imgbg.frame.size.width/2
        self.view_imgbg.layer.masksToBounds = true
        
        self.img_Profile.layer.cornerRadius = self.img_Profile.frame.size.width/2
        self.img_Profile.layer.masksToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.img_Profile.addGestureRecognizer(tap)
        self.img_Profile.isUserInteractionEnabled = true
        
        Getroomdetail()
 
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler:
            { _ in
                self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Open Image from camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Basmah", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Image from gallery
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification)
    {
        // Take Action on Notification
        
        if (notification.userInfo?["tag_id"] as? String) != nil
        {
            tagid = (notification.userInfo?["tag_id"] as? String)!
            print(tagid)
        }
        if (notification.userInfo?["tag_name"] as? String) != nil
        {
            tagname = (notification.userInfo?["tag_name"] as? String)!
            print(tagname)
            
            btnTag.setTitle(tagname, for: .normal)
        }
    }

    deinit
    {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:Api Call
    func Getroomdetail()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")as! String
            
            let parameters: Parameters =
                [
                    "room_id":room_id as AnyObject,
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "room_detail"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    
                                    let dic = swiftyJsonVar["room_detail"]
                                    print(dic)
                                    
                                    self.txt_room.text = dic["room_name"].stringValue
                                    self.txt_announcement.text = dic["announcement"].stringValue
                                    self.txt_membershipfee.text = dic["membership_fee"].stringValue
                                    self.btnTag.setTitle(dic["tag_name"].stringValue, for: .normal)
                                    
                                    self.tagid = dic["tags"].stringValue
                                    
                                    self.img_Profile.kf.indicatorType = .activity
                                    (self.img_Profile.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                    self.img_Profile.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    let url = URL(string: dic["image"].stringValue)
                                    
                                    DispatchQueue.global().async
                                        {
                                            if url  != nil
                                            {
                                                let data = try? Data(contentsOf: url!)
                                                
                                                DispatchQueue.main.async
                                                    {
                                                        if data  != nil
                                                        {
                                                            self.chosenImage = UIImage(data: data!)!
                                                            self.image_Data = UIImageJPEGRepresentation(self.chosenImage, 0.5)
                                                        }
                                                }
                                            }
                                    }
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillLayoutSubviews()
    {
        let lblSave = UILabel(frame: view_ButtonBG.bounds)
        lblSave.text = "Save"
        lblSave.font = UIFont.boldSystemFont(ofSize: 15)
        lblSave.textAlignment = .center
        view_ButtonBG.addSubview(lblSave)
        view_ButtonBG.mask = lblSave
    }
    
    @IBAction func doclickonTag(_ sender: Any)
    {
        let taglistVC = self.storyboard?.instantiateViewController(withIdentifier: "TagListViewController") as? TagListViewController
        self.navigationController?.pushViewController(taglistVC!, animated: true)
    }
    
    @IBAction func doclickonSave(_ sender: Any)
    {
        if (tagid == "")
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please select tag",buttonTitle: "OK");
        }
        else
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        self.update_room()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    func update_room()
    {
            let parameters: Parameters =
                [
                    "id":room_id as AnyObject,
                    "room_name":txt_room.text as AnyObject,
                    "announcement":txt_announcement.text as AnyObject,
                    "tags":tagid as AnyObject,
                    "membership_fee":txt_membershipfee.text as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "update_room"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if let data = self.image_Data
                {
                    multipartFormData.append(data, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.navigationController?.popViewController(animated: true)
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }

extension RoomSettingViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerEditedImage] as? UIImage) != nil
        {
            self.chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            self.image_Data = UIImageJPEGRepresentation(chosenImage, 0.5)
            self.img_Profile.image = self.chosenImage
        }
        picker.dismiss(animated: true)
    }
}
