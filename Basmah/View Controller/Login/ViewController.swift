//
//  ViewController.swift
//  Basmah
//
//  Created by CT on 9/3/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD
import FBSDKLoginKit
import GoogleSignIn
import Google
import Reachability

class ViewController: UIViewController,GIDSignInUIDelegate,GIDSignInDelegate
{
    var final:NSMutableAttributedString = NSMutableAttributedString()
    var final2:NSMutableAttributedString = NSMutableAttributedString()
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    
    var dict : [String : AnyObject]!
    var login_type = ""
    var user_email = ""
    var social_id = ""
    //var full_name = ""
    var last_name = ""
    var gender = ""
    var full_name = ""
    var image_user:String = String()

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txt_phonenumber: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate

        btnLogin.layer.cornerRadius = 20.0
        btnLogin.layer.masksToBounds = true
        
        let attrString = NSAttributedString (
            string: "WELCOME TO",
            attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        
        let attrString2 = NSAttributedString (
            string: " BASMAH",
            attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 129/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0)])
        
        final.append(attrString)
        final.append(attrString2)
        lbl_title.attributedText = final
        
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
            navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "HomeViewController")], animated: false)
            
            let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
            mainViewController.rootViewController = navigationController
            mainViewController.setup(type: 2)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:IBAction
    @IBAction func doclickonLogin(_ sender: Any)
    {
        if (txt_phonenumber.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter phone number",buttonTitle: "OK");
        }
        else if (txt_password.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter password",buttonTitle: "OK");
        }
        else
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                         self.login()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    @IBAction func doclickonFb(_ sender: Any)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    appDelegate.greeting = "FB"
                    
                    UserDefaults.standard.set(appDelegate.greeting, forKey: "LoginStatus")
                    UserDefaults.standard.synchronize()
                    
                    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                    fbLoginManager.loginBehavior = FBSDKLoginBehavior.web

                    fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
                        if (error == nil)
                        {
                            let fbloginresult : FBSDKLoginManagerLoginResult = result!
                            if fbloginresult.grantedPermissions != nil
                            {
                                if(fbloginresult.grantedPermissions.contains("email"))
                                {
                                    self.getFBUserData()
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    // MARK: - FaceBook Data
    
    func getFBUserData()
    {
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name,gender,birthday,picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil)
                {
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                    
                    let email = self.dict["email"] as! String?
                    self.user_email = email!
                    
                    let socialid = self.dict["id"] as! String?
                    self.social_id = socialid!
                    
                    let fullname = self.dict["name"]as!String?
                    self.full_name = fullname!
                    
                    let lastname = self.dict["last_name"]as!String?
                    self.last_name = lastname!
                    
                    if let imageURL = ((self.dict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String
                    {
                        //Download image from imageURL
                        print(imageURL)
                        self.image_user = imageURL
                    }
                    
                    self.login_type = "2"
                    self.Sociallogin()
                }
            })
        }
    }
    
    
    @IBAction func doclickonTwitter(_ sender: Any)
    {
        
    }
    
    @IBAction func doclickonInstagram(_ sender: Any)
    {
         self.login_type = "4"
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "instagramLoginVC") as! instagramLoginVC
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonGoogle(_ sender: Any)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    appDelegate.greeting = "Google"
                    UserDefaults.standard.set(appDelegate.greeting, forKey: "LoginStatus")
                    UserDefaults.standard.synchronize()
                    
                    //error object
                    var error : NSError?
                    
                    //setting the error
                    GGLContext.sharedInstance().configureWithError(&error)
                    
                    //if any error stop execution and print error
                    if error != nil{
                        print(error ?? "google error")
                        return
                    }
                    
                    //adding the delegates
                    GIDSignIn.sharedInstance().uiDelegate = self
                    GIDSignIn.sharedInstance().delegate = self
                    GIDSignIn.sharedInstance().signIn()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    //MARK: Google sign in Delegate
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error == nil)
        {
            _ = user.authentication.idToken! // Safe to send to the server
            //print("Authentication idToken is \(idToken)")
            
            let userId = user.userID!
            print("User id is \(userId)")
            
            let fullName = user.profile.name!
            print("User full name is \(fullName)")
            
            let email = user.profile.email!
            print("User email address is \(email)")
            
            self.user_email = email
            self.full_name = fullName
            self.social_id = userId
            self.login_type = "3"
            
            if user.profile.hasImage
            {
                let pic = user.profile.imageURL(withDimension: 100)
                self.image_user = (pic?.absoluteString)!
                print(self.image_user)
            }
            
            self.Sociallogin()
        }
        else
        {
            // some error handling code
            print("ERROR ::\(error.localizedDescription)")
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!)
    {
        guard error == nil else
        {
            print("Error while trying to redirect : \(error)")
            return
        }
        
        print("Successful Redirection")
    }
    
    //MARK: Api Call
    func login()
    {
        let parameters: Parameters =
            [
                "phone":txt_phonenumber.text as AnyObject,
                "password":txt_password.text  as AnyObject,
                "ios_token":self.appDelegate.device_token as AnyObject,
                "android_token":"" as AnyObject,
            ]
        
        print(parameters)
        
        let url = kBaseURL + "login"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            self.hud.dismiss()
            
            switch result
            {
                case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print(response.response!)
                    print(response.data!)
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                let data = swiftyJsonVar["userinfo"]
                                
                                let UserDefault = UserDefaults.standard
                                UserDefault.set(data["user_id"].stringValue,forKey:"user_id")
                                UserDefault.set(data["id"].stringValue,forKey:"id")
                                UserDefault.set(data["username"].stringValue,forKey:"username")
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
                                navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "HomeViewController")], animated: false)
                                
                                let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
                                mainViewController.rootViewController = navigationController
                                mainViewController.setup(type: 2)
                                let window = UIApplication.shared.delegate!.window!!
                                window.rootViewController = mainViewController
                                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
                            }
                            else
                            {
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                }
                
                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    func Sociallogin()
    {
        let parameters: Parameters =
            [
                "social_id":self.social_id as AnyObject,
                "username":self.full_name as AnyObject,
                "user_image":self.image_user as AnyObject,
                "android_token":"" as AnyObject,
                "ios_token":appDelegate.device_token as AnyObject,
                "user_address":"" as AnyObject,
                "dob":"" as AnyObject,
                "country":"" as AnyObject,
                "login_type":self.login_type,
            ]
        
        print(parameters)
        
        let url = kBaseURL + "social_login"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            self.hud.dismiss()
            
            switch result
            {
                case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print(response.response!)
                    print(response.data!)
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                let data = swiftyJsonVar["userinfo"]
                                
                                let UserDefault = UserDefaults.standard
                                UserDefault.set(data["user_id"].stringValue,forKey:"user_id")
                                UserDefault.set(data["id"].stringValue,forKey:"id")
                                UserDefault.set(data["username"].stringValue,forKey:"username")
                                
                                if (self.appDelegate.greeting == "FB")
                                {
                                    UserDefault.set(data["user_image"].stringValue,forKey:"userfb_image")
                                }
                                else if(self.appDelegate.greeting == "Google")
                                {
                                    UserDefault.set(data["user_image"].stringValue,forKey:"usergoo_image")
                                }
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
                                navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "HomeViewController")], animated: false)
                                
                                let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
                                mainViewController.rootViewController = navigationController
                                mainViewController.setup(type: 2)
                                let window = UIApplication.shared.delegate!.window!!
                                window.rootViewController = mainViewController
                                UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
                            }
                            else
                            {
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                }
                
                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func doclickonSignUp(_ sender: Any)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
}

