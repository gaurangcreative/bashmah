//
//  PendingRequestViewController.swift
//  Basmah
//
//  Created by CT on 9/19/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class PendingRequestViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_pendingrequest:NSMutableArray = NSMutableArray()
    var accept_reject_status:Int = Int()
    var pendingcount:String = String()
    var friend_id:String = String()
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var table_view: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.lbl_title.text = pendingcount
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    PendingRequest()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: Table View Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_pendingrequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
        
        let dic = JSON(self.arr_pendingrequest.object(at: indexPath.row))
        
        cell.btnAccept.tag = indexPath.row
        cell.btnReject.tag = indexPath.row
        
        cell.lbl_countryname.text = dic["username"].stringValue
        
        cell.img_user.kf.indicatorType = .activity
        (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
        
        cell.lbl_date.text = dic["created_at"].stringValue
        
        cell.btnAccept.addTarget(self, action: #selector(doclickonAccept), for: .touchUpInside)
        cell.btnReject.addTarget(self, action: #selector(doclickonReject), for: .touchUpInside)

        cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
        cell.img_user.layer.masksToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_pendingrequest.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
    
    @objc func doclickonAccept(sender: UIButton!)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    let dic = JSON(self.arr_pendingrequest.object(at: sender.tag))
                    accept_reject_status = 1
                    friend_id = dic["friend_id"].stringValue

                    Accept_reject()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @objc func doclickonReject(sender: UIButton!)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    let dic = JSON(self.arr_pendingrequest.object(at: sender.tag))
                    accept_reject_status = 2
                    friend_id = dic["friend_id"].stringValue

                    Accept_reject()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    //MARK: Api Call
    func PendingRequest()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "friend_request_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["friend_list"].array! as NSArray
                                    self.arr_pendingrequest = arr.mutableCopy() as! NSMutableArray
                                    self.table_view.reloadData()
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func Accept_reject()
    {
        let parameters: Parameters =
            [
                "id":self.friend_id  as AnyObject,
                "status":accept_reject_status as AnyObject,
            ]

        print(parameters)

        let url = kBaseURL + "accept_reject_friend"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)

        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]

        Alamofire.upload(multipartFormData: { (multipartFormData) in

            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in

            self.hud.dismiss()

            switch result
            {
            case .success(let upload, _, _):

                upload.responseJSON { response in

                    print(response.response!)
                    print(response.data!)
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].intValue == 1
                            {
                                // Add reachability observer
                                if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                                {
                                    if reachability.connection != .none
                                    {
                                        if reachability.connection == .wifi || reachability.connection == .cellular
                                        {
                                            self.PendingRequest()
                                        }
                                    }
                                    else
                                    {
                                        self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                                    }
                                }
                            }
                            else
                            {
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                }

                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
