//
//  GiftsViewController.swift
//  Bashmah
//
//  Created by CTIMac on 17/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class GiftsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{
     var providerTitleArry : [String] = ["Rahul Sharma","Michael D'souza", "Rahul Sharma","Michael D'souza","Michael"]

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return providerTitleArry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
        cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
        cell.img_user.layer.masksToBounds = true
        cell.lbl_countryname?.text = providerTitleArry[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
}
