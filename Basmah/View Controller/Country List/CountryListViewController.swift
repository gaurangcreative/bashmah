//
//  CountryListViewController.swift
//  Basmah
//
//  Created by CT on 9/10/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD
import Kingfisher

class CountryListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var arr_country:NSMutableArray = NSMutableArray()
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var checkController:String = String()

    @IBOutlet weak var table_view: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDelegate = UIApplication.shared.delegate as! AppDelegate
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    GetCountryList()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    //MARK: Api Call
    func GetCountryList()
    {
        let parameters: Parameters =
            [
                :
            ]
        
        print(parameters)
        
        let url = kBaseURL + "phone_code_list"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)

        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            self.hud.dismiss()
            
            switch result
            {
                case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print(response.response!)
                    print(response.data!)
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                let arr = swiftyJsonVar["code_list"].array! as NSArray
                                self.arr_country = arr.mutableCopy() as! NSMutableArray
                                self.table_view.reloadData()
                            }
                            else
                            {
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                }
                
                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Table View Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_country.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return table_view.rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
        
        let dic = JSON(arr_country.object(at: indexPath.row))
        cell.lbl_countryname.text = dic["name"].stringValue.firstUppercased
        cell.lbl_countrycode.text = dic["phonecode"].stringValue
        
        cell.img_country.kf.indicatorType = .activity
        (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //self.navigationController?.popViewController(animated: true)
        
        if (checkController == "Signup")
        {
            let dic = JSON(arr_country.object(at: indexPath.row))
            let signup = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            
            signup.checkcontroller = "Country"
            signup.country_name = dic["name"].stringValue
            signup.phone_code = dic["phonecode"].stringValue
            signup.country_id = dic["id"].stringValue
            signup.country_image = dic["country_image"].stringValue
            self.navigationController?.pushViewController(signup, animated: false)
        }
        else
        {
            let dic = JSON(arr_country.object(at: indexPath.row))
//            let UserDefault = UserDefaults.standard
//            UserDefault.set(dic["name"].stringValue,forKey:"country_name")
//            UserDefault.set(dic["id"].stringValue,forKey:"country_id")
//            UserDefault.set(dic["country_image"].stringValue,forKey:"country_image")
            
            NotificationCenter.default.post(name: Notification.Name("Countrydetail"), object: nil, userInfo: ["country_name":dic["name"].stringValue,"id":dic["id"].stringValue,"cimage":dic["country_image"].stringValue,"checkController":"Edit"])

            self.navigationController?.popViewController(animated: true)
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
