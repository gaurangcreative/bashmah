//
//  CountryListTableViewCell.swift
//  Basmah
//
//  Created by CT on 9/10/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class CountryListTableViewCell: UITableViewCell
{
    @IBOutlet weak var img_country: UIImageView!
    @IBOutlet weak var lbl_countryname: UILabel!
    @IBOutlet weak var lbl_countrycode: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var img_gender: UIImageView!
    @IBOutlet weak var img_gift: UIImageView!
    @IBOutlet weak var img_arrow: UIImageView!
    @IBOutlet weak var txt_view: UITextView!
    @IBOutlet weak var constrantent_txt_hieght: NSLayoutConstraint!
    @IBOutlet weak var lbl_coin: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var view_shadow: UIView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var lbl_desc: UILabel!
    @IBOutlet weak var btn_blockUnblock: GradientBtn!
    @IBOutlet weak var view_bgcolor: UIView!
    @IBOutlet weak var lbl_uniqueid: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var img_onlineoroffline: UIImageView!
    
    
    var size : Float = Float()
    
    var isExpanded:Bool = false
    {
        didSet
        {
            if !isExpanded
            {
                self.constrantent_txt_hieght.constant = 0.0
            }
            else
            {
                self.constrantent_txt_hieght.constant = CGFloat(size)
            }
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
//        self.img_user.layer.cornerRadius = self.img_user.frame.size.width/2
//        self.img_user.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
