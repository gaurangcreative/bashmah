//
//  TagRoomListViewController.swift
//  Basmah
//
//  Created by CT on 10/5/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class TagRoomListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_tag_roomlist:NSMutableArray = NSMutableArray()
    var arr_joinedroom:NSMutableArray = NSMutableArray()
    var tag_id:String = String()
    var tag_name:String = String()
    var checkController:String = String()

    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var lbl_tagname: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if (checkController == "Profile")
        {
            lbl_tagname.text = "Joined rooms"
            JoinedRoomlist()
        }
        else
        {
            print(tag_id)
            lbl_tagname.text = tag_name
            Roomlist()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:Api Call
    func Roomlist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":"3" as AnyObject,
                    "tag":tag_id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "room_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                   case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let arr = swiftyJsonVar["room_list"].array! as NSArray
                                    self.arr_tag_roomlist = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func JoinedRoomlist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":"2" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "room_list_others"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let arr = swiftyJsonVar["room_list"].array! as NSArray
                                    self.arr_joinedroom = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (checkController == "Profile")
        {
            return arr_joinedroom.count
        }
        else
        {
            return arr_tag_roomlist.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (checkController == "Profile")
        {
            let cell = self.table_view.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
            
            let dic = JSON(arr_joinedroom.object(at: indexPath.row))
            cell.lbl_countryname.text = dic["room_name"].stringValue
            cell.lbl_countrycode.text = dic["tags"].stringValue
            cell.lbl_countrycode.layer.cornerRadius = 10.0
            cell.lbl_countrycode.layer.masksToBounds = true
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true

            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))

            cell.img_country.kf.indicatorType = .activity
            (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))

            cell.lbl_desc.text = dic["announcement"].stringValue
            return cell
        }
        else
        {
            let cell = self.table_view.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
            
            let dic = JSON(arr_tag_roomlist.object(at: indexPath.row))
            
            cell.lbl_countryname.text = dic["room_name"].stringValue
            cell.lbl_countrycode.text = dic["tags"].stringValue
            
            cell.lbl_countrycode.layer.cornerRadius = 10.0
            cell.lbl_countrycode.layer.masksToBounds = true
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            cell.img_country.kf.indicatorType = .activity
            (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))
            
            cell.lbl_desc.text = dic["announcement"].stringValue
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if (checkController == "Profile")
        {
            var numOfSections: Int = 0
            
            if arr_joinedroom.count > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections = 1
                tableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
        }
        else
        {
            var numOfSections: Int = 0
            
            if arr_tag_roomlist.count > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections = 1
                tableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
