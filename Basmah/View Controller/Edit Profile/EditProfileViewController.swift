//
//  EditProfileViewController.swift
//  Basmah
//
//  Created by CT on 9/11/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JGProgressHUD
import Kingfisher

class EditProfileViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource
{
    var server_date:String = String()
    var selectedRow = 0;
    var gender:String = String()
    var country_name:String = String()
    var country_name2:String = String()
    var country_image:String = String()
    var phone_code:String = String()
    var country_id:String = String()
    var checkController:String = String()
    var arr_user:NSMutableArray = NSMutableArray()
    var appDelegate = AppDelegate()
    var hud = JGProgressHUD()
    let imagePicker = UIImagePickerController()
    var chosenImage = UIImage()
    var image_Data = UIImageJPEGRepresentation(UIImage(),0)
    
    @IBOutlet weak var lbl_tagcount: UILabel!
    @IBOutlet weak var lbl_hobbycount: UILabel!
    @IBOutlet weak var lbl_whatsappcount: UILabel!
    @IBOutlet weak var img_country: UIImageView!
    
    private let arr_gender = ["Male",
                              "Female",
                              ]
    
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var txt_dob: UITextField!
    @IBOutlet weak var txt_gender: UITextField!
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var view_date: UIView!
    @IBOutlet weak var date_picker: UIDatePicker!
    @IBOutlet weak var view_gender: UIView!
    @IBOutlet weak var picker_view: UIPickerView!
    @IBOutlet weak var txt_tag: UITextField!
    @IBOutlet weak var txt_hobby: UITextField!
    @IBOutlet weak var txt_whatsapp: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        imagePicker.delegate = self
        
//        view_date.isHidden = true
//        view_gender.isHidden = true
        
        self.img_user.layer.cornerRadius = self.img_user.frame.size.width/2
        self.img_user.layer.masksToBounds = true
        
        txt_username.attributedPlaceholder = NSAttributedString(string: "User Name",
                                                                attributes: [NSAttributedStringKey.foregroundColor:
                                                                    UIColor.lightGray])
        
        txt_dob.attributedPlaceholder = NSAttributedString(string: "Date of Birth",
                                                           attributes: [NSAttributedStringKey.foregroundColor:
                                                            UIColor.lightGray])
        
        txt_gender.attributedPlaceholder = NSAttributedString(string: "Select Gender",
                                                              attributes: [NSAttributedStringKey.foregroundColor:
                                                                UIColor.lightGray])
        
        let currentDate: NSDate = NSDate()
        self.date_picker.maximumDate = currentDate as Date
        self.date_picker.addTarget(self, action: #selector(EditProfileViewController.handleDatePicker), for: UIControlEvents.valueChanged)
        // Do any additional setup after loading the view.
        
        txt_tag.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_whatsapp.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_hobby.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReceivedNotification(notification:)), name: Notification.Name("Countrydetail"), object: nil)
    }
    
    @objc func ReceivedNotification(notification: Notification)
    {
        // Take Action on Notification
        
        if (notification.userInfo?["country_name"] as? String) != nil
        {
            country_name = (notification.userInfo?["country_name"] as? String)!
            print(country_name)
            
            self.btnCountry.setTitle("         " + self.country_name, for: UIControlState.normal)
            self.btnCountry.setTitleColor(UIColor.black, for: UIControlState.normal)
        }
        if (notification.userInfo?["id"] as? String) != nil
        {
            country_id = (notification.userInfo?["id"] as? String)!
            print(country_id)
        }
        if (notification.userInfo?["cimage"] as? String) != nil
        {
            country_image = (notification.userInfo?["cimage"] as? String)!
            print(country_image)
            
            self.img_country.kf.indicatorType = .activity
            (self.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            self.img_country.kf.setImage(with: URL(string:country_image), placeholder: UIImage(named: ""))
        }
        if (notification.userInfo?["checkController"] as? String) != nil
        {
           self.checkController = (notification.userInfo?["checkController"] as? String)!
        }
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("Countrydetail"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        view_date.isHidden = true
        view_gender.isHidden = true

        if(checkController == "Profile")
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                       UserDetails()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    //MARK: Api Call
    func UserDetails()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":id as AnyObject,
                    "my_id":"" as AnyObject,
                    "type":"2",
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_detail"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    //                                let arr = swiftyJsonVar["userinfo"].array! as NSArray
                                    //                                self.arr_user = arr.mutableCopy() as! NSMutableArray
                                    //                                print(self.arr_user)
                                    
                                    let dic = swiftyJsonVar["userinfo"]
                                    
                                    self.txt_username.text = dic["username"].stringValue.firstUppercased
                                    
                                    self.txt_tag.text = dic["identity_tag"].stringValue
                                    self.txt_hobby.text = dic["hobby"].stringValue
                                    self.txt_whatsapp.text = dic["whats_up"].stringValue
                                    
                                    self.img_user.kf.indicatorType = .activity
                                    (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                    self.img_user.kf.setImage(with: URL(string:dic["user_image"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    self.img_country.kf.indicatorType = .activity
                                    (self.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                    self.img_country.kf.setImage(with: URL(string:dic["country"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    self.btnCountry.setTitle("         " + dic["country_name"].stringValue, for: UIControlState.normal)
                                    self.btnCountry.setTitleColor(UIColor.black, for: UIControlState.normal)
                                    
                                    self.country_id = dic["country_id"].stringValue
                                    
                                    if (dic["dob"].stringValue == "0000-00-00")
                                    {
                                        self.txt_dob.attributedPlaceholder = NSAttributedString(string: "Date of Birth",
                                                                                                attributes: [NSAttributedStringKey.foregroundColor:
                                                                                                    UIColor.lightGray])
                                        self.server_date = "0000-00-00"
                                    }
                                    else
                                    {
                                        self.txt_dob.text = dic["dob"].stringValue
                                        self.server_date = dic["dob"].stringValue
                                    }
                                    
                                    if (dic["gender"].stringValue == "")
                                    {
                                        self.txt_gender.attributedPlaceholder = NSAttributedString(string: "Select Gender",
                                                                                                   attributes: [NSAttributedStringKey.foregroundColor:
                                                                                                    UIColor.lightGray])
                                        
                                        self.gender = ""
                                    }
                                    else
                                    {
                                        self.txt_gender.text = dic["gender"].stringValue
                                        self.gender = dic["gender"].stringValue
                                    }
                                    
                                    let url = URL(string: dic["user_image"].stringValue)
                                    
                                    DispatchQueue.global().async
                                        {
                                            if url  != nil
                                            {
                                                let data = try? Data(contentsOf: url!)
                                                
                                                DispatchQueue.main.async
                                                    {
                                                        if data  != nil
                                                        {
                                                            self.chosenImage = UIImage(data: data!)!
                                                            self.image_Data = UIImageJPEGRepresentation(self.chosenImage, 0.5)
                                                        }
                                                }
                                            }
                                    }
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:IBAction
    @IBAction func doclickonBack(_ sender: Any)
    {
//        if(checkController == "Edit")
//        {
//            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShowProfileViewController") as! ShowProfileViewController
//            self.navigationController?.pushViewController(secondViewController, animated: true)
//
//        }
//        else
//        {
            self.navigationController?.popViewController(animated: true)
        //}
    }
    
    @IBAction func doclickonGender(_ sender: Any)
    {
        view_date.isHidden = true
        view_gender.isHidden = false
    }
    
    @IBAction func doclickonSave(_ sender: Any)
    {
        if (txt_username.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter username",buttonTitle: "OK");
        }
        else
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        Updateprofile()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    @IBAction func doclickonPickImage(_ sender: Any)
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler:
            { _ in
                self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Open Image from camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Basmah", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Image from gallery
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func doclickonSelectCountry(_ sender: Any)
    {
        self.view.endEditing(true)
        self.view_date.isHidden = true
        self.view_gender.isHidden = true
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "CountryListViewController") as! CountryListViewController
        secondViewController.checkController = "Edit"
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func doclickonCancel(_ sender: Any)
    {
        self.view.endEditing(true)
        self.view_date.isHidden = true
    }
    
    @IBAction func doclickonDone(_ sender: Any)
    {
        self.view.endEditing(true)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        txt_dob.text = dateFormatter.string(from: date_picker.date)
        txt_dob.textColor = UIColor.black
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        server_date = dateFormatter.string(from: date_picker.date)
        print(server_date)
        view_date.isHidden = true
    }
    
    @objc func handleDatePicker()
    {
    }
    
    @IBAction func doclickonCancelPicker(_ sender: Any)
    {
        self.view.endEditing(true)
        view_gender.isHidden = true
    }
    
    @IBAction func doclickonDonePicker(_ sender: Any)
    {
        self.view.endEditing(true)
        self.view_gender.isHidden = true
        txt_gender.text = arr_gender[selectedRow]
        txt_gender.textColor = UIColor.black
        gender = arr_gender[selectedRow]
    }
    
    // MARK: Api Call
    func Updateprofile()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
                let parameters: Parameters =
                    [
                        "id":id as AnyObject,
                        "dob":server_date as AnyObject,
                        "gender": gender as AnyObject,
                        "username":txt_username.text as AnyObject,
                        "user_country":country_id as AnyObject,
                        "identity_tag": txt_tag.text as AnyObject ,
                        "hobby": txt_hobby.text as AnyObject,
                        "whats_up": txt_whatsapp.text as AnyObject,
                    ]
                
                print(parameters)
                
                let url = kBaseURL + "updateProfile"
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                    if let data = self.image_Data
                    {
                        multipartFormData.append(data, withName: "user_image",fileName: "file.jpg", mimeType: "image/jpg")
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    self.hud.dismiss()
                    switch result
                    {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            print(response.response!) // URL response
                            print(response.data!)     // server data
                            print(response.result.value!)
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let dict = swiftyJsonVar["userinfo"]
                                    
                                    let UserDefault = UserDefaults.standard
                                    UserDefault.set(dict["username"].stringValue,forKey:"username")
                                    UserDefault.set(dict["user_image"].stringValue,forKey:"user_image")
                                    
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                        case .failure(let error):
                        self.hud.dismiss()
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
            }
    }
    
    //MARK:Text Field Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField == txt_username)
        {
            view_date.isHidden = true
            view_gender.isHidden = true
        }
        else if (textField == txt_gender)
        {
            self.view.endEditing(true)
            textField.tintColor = UIColor.clear
        }
        else if (textField == txt_tag)
        {
            view_date.isHidden = true
            view_gender.isHidden = true
        }
        else if (textField == txt_hobby)
        {
            view_date.isHidden = true
            view_gender.isHidden = true
        }
        else if (textField == txt_whatsapp)
        {
            view_date.isHidden = true
            view_gender.isHidden = true
        }
    }
    
    @IBAction func doclickonDob(_ sender: Any)
    {
        self.view.endEditing(true)
        view_date.isHidden = false
    }
    
    override func didChangeValue(forKey key: String)
    {
        //print(textView_Desc.text.characters.count)
    }
    
    func updateCharacterCount()
    {
        let descriptionCount = self.txt_tag.text?.characters.count
        self.lbl_tagcount.text = "(\((45) - descriptionCount!))"
        
        let hobbyCount = self.txt_hobby.text?.characters.count
        self.lbl_hobbycount.text = "(\((45) - hobbyCount!))"
        
        let whatsappCount = self.txt_whatsapp.text?.characters.count
        self.lbl_whatsappcount.text = "(\((60) - whatsappCount!))"
    }
    
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        self.updateCharacterCount()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == txt_tag)
        {
            return (txt_tag.text?.characters.count)! +  (string.characters.count - range.length) <= 45
        }
        else if(textField == txt_hobby)
        {
            return (txt_hobby.text?.characters.count)! +  (string.characters.count - range.length) <= 45
        }
        else if(textField == txt_whatsapp)
        {
            return (txt_whatsapp.text?.characters.count)! +  (string.characters.count - range.length) <= 60
        }
        
        return false
    }
    
    //MARK:- Picker View Deleagte
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arr_gender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arr_gender[row] as String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedRow = row;
    }
}

extension EditProfileViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.checkController = "image"
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerEditedImage] as? UIImage) != nil
        {
            self.chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            self.image_Data = UIImageJPEGRepresentation(chosenImage, 0.5)
            self.checkController = "image"
            self.img_user.image = self.chosenImage
        }
        picker.dismiss(animated: true)
    }
    
}
