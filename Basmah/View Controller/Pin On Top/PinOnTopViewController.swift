//
//  PinOnTopViewController.swift
//  Bashmah
//
//  Created by CTIMac on 13/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class PinOnTopViewController: UIViewController , UITableViewDataSource , UITableViewDelegate
{
    var providerTitleArry : [String] = ["30","60", "120","150","250","350","452","890","789"]
    var providerTitleArry2 : [String] = ["1.Enter the room with Password","2.Pin your room on the top", "3.Enter the room with Password","4.Pin your room on the top"]
    
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    
    var timer = Timer()
    
    var arr_plan_list:NSMutableArray = NSMutableArray()
    var arr_time:NSMutableArray = NSMutableArray()
    var plan_id:String = String()
    var uiColorArray = [UIColor]()

    @IBOutlet weak var cantblTop: NSLayoutConstraint!
    @IBOutlet weak var cantblDown: NSLayoutConstraint!

    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var tableView_Down: UITableView!
    @IBOutlet weak var tableView_Top: UITableView!
    @IBOutlet weak var lbl_time: UILabel!
    
    var hour:Int = Int()
    var minutes:Int = Int()
    var seconds:Int = Int()
    var milsecons:Double = Double()
    
    var selectedhour:Int = Int()
    var selectminutes:Int = Int()
    var selectsceonds:Int = Int()
    
    var selectedstring:String = String()
    
    var countdownTimer: Timer!
    var showtimer: Timer!
    var str_time:String = String()
    var tempint:Int = Int()
    var counter = 0

    var dateCounter = Date()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        show_pin_top()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        print("Hello")
        self.timer.invalidate()
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.timer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonRecharge(_ sender: Any)
    {
        
    }
    
    //MARK:Api Call
    @objc func show_pin_top()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "show_pin_top"
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response)
                        
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                self.hud.dismiss()
                                let arr = swiftyJsonVar["plan_list"].array! as NSArray
                                self.arr_plan_list = arr.mutableCopy() as! NSMutableArray
                                
                                var i = 0
                                
                                while i < self.arr_plan_list.count
                                {
                                    self.uiColorArray.append(UIColor(red: 171/255.0, green: 84/255.0, blue: 249/255.0, alpha: 1.0) )
                                    
                                    self.uiColorArray.append(UIColor(red: 82/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0) )
                                    
                                    self.uiColorArray.append(UIColor(red: 245/255.0, green: 58/255.0, blue: 111/255.0, alpha: 1.0) )
                                    
                                    i = i + 1
                                }
                                
                                let arrtime = swiftyJsonVar["time_array"].array! as NSArray
                                self.arr_time = arrtime.mutableCopy() as! NSMutableArray
                                
                                if (self.arr_time.count>0)
                                {
                                    let dic = JSON(self.arr_time.object(at: 0))
                                    
                                    self.hour = dic["hours"].intValue
                                    self.minutes = dic["minutes"].intValue
                                    self.seconds = dic["sec"].intValue
                                    self.milsecons = dic["miliseconds"].double!
                                    
                                    self.dateCounter = Date(timeIntervalSinceNow: (self.milsecons / 1000.0))
                                    print("datekjadhsfkjhakjlsdfhkajlsdfhkjlasdf - \(self.dateCounter)")
                                    
                                    let valueDate = Date()
                                    print(valueDate)
                                    
                                    self.scheduledTimerWithTimeInterval()
                                }
                                else
                                {
                                    self.lbl_time.text = "0" as String + ":" as String + "0"  as String + ":"  as String + "0" as String
                                }
                                
                                self.setTableLayout()
                            }
                            else
                            {
                                self.hud.dismiss()
                            }
                            
                            self.numberOfSections(in: self.tableView_Top)
                            DispatchQueue.main.async
                            {
                                self.tableView_Top.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: -- timenew
    func scheduledTimerWithTimeInterval()
    {
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting()
    {
        NSLog("counting..")
        
       let now = Date()
        
       let value = relativeDateStringForDate(dateCounter)
        
       print(value)
    }
    
    func getStringFromDate(_ date: Date, sourceFormat: String, destinationFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = sourceFormat
        
        dateFormatter.dateFormat = destinationFormat
        let desiredString = dateFormatter.string(from: date)
        
        return desiredString
    }
    
    func relativeDateStringForDate(_ date:  Date?) -> String {
        
        if date == nil {
            return ""
        }
        
        let strTime = getStringFromDate(date!, sourceFormat: "yyyy-MM-dd HH:mm:ss ZZZ", destinationFormat: "dd MMM")
        
        let gregorian = Calendar.current
        let units = NSCalendar.Unit(rawValue: UInt.max)
        
        let dateToday = Date()
        
        let components = (gregorian as NSCalendar).components(units, from: dateToday, to: date!, options: [])
        
        if components.year! > 0 {
            
            if components.year == 1 {
                //                return "\(components.year!) year ago"
                return "\(strTime)"
            } else {
                //                return "\(components.year!) years ago"
                return "\(strTime)"
            }
        }
        else if components.month! > 0 {
            
            if components.month == 1 {
                //                return "\(components.month!) month ago"
                return "\(strTime)"
                
            } else {
                //                return "\(components.month!) months ago"
                return "\(strTime)"
            }
        }
        else if components.weekOfYear! > 0 {
            
            if components.weekOfYear == 1 {
                //                return "\(components.weekOfYear!) week ago"
                return "\(strTime)"
            } else {
                //return "\(components.weekOfYear!) weeks ago"
                return "\(strTime)"
            }
        }
        else if components.day! > 0 {
            
            if components.day == 1 {
                //return "\(components.day!) day ago"
                return "\(strTime)"
            } else {
                //return "\(components.day!) days ago"
                return "\(strTime)"
            }
        }
        else if components.second! > 0
        {
            if components.second == 1
            {
                let val =  "\(components.minute!) min ago"
                print(val)
                
                self.lbl_time.text = "\(components.hour!):\(components.minute!):\(components.second!)"
                
                 return "\(components.second!) second  ago"
            }
            else
            {
                let val =  "\(components.minute!) min ago"
                print(val)
                
                self.lbl_time.text = "\(components.hour!):\(components.minute!):\(components.second!)"
                
                 return "\(components.second!) seconds  ago"
            }
            return "just now"
        }
        
        if lbl_time.text == "0:0:1"
        {
            timer.invalidate()
            lbl_time.text = "0:0:0"
        }
        
        return ""
    }
    
    //MARK: -- endTime

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table view delegate
    func setTableLayout()
    {
        cantblTop.constant = CGFloat(arr_plan_list.count * 70)
        DispatchQueue.main.async
            {
                self.tableView_Top.reloadData()
            }
        
        cantblDown.constant = CGFloat(providerTitleArry2.count * 44)
        DispatchQueue.main.async
            {
                self.tableView_Down.reloadData()
            }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView == tableView_Top)
        {
            return arr_plan_list.count
        }
        else
        {
            return providerTitleArry2.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.tableView_Top
        {
            let cell = self.tableView_Top.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
            
            cell.selectionStyle = .none
            
            let dic = JSON(arr_plan_list.object(at: indexPath.row))

            cell.lbl_price?.text = dic["price"].stringValue
            cell.lbl_countryname?.text = dic["minutes"].stringValue
            cell.view_bg.backgroundColor = self.uiColorArray[indexPath.row]

            return cell
        }
        else
        {
            let cell = self.tableView_Down.dequeueReusableCell(withIdentifier: "Cell") as! HomeTableViewCell
            cell.lbl_title?.text = providerTitleArry2[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.tableView_Top
        {
            let refreshAlert = UIAlertController(title: "Basmah", message: "Confirm to purchase Pin on Top?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
                
                // Add reachability observer
                if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                {
                    if reachability.connection != .none
                    {
                        if reachability.connection == .wifi || reachability.connection == .cellular
                        {
                            let dic = JSON(self.arr_plan_list.object(at: indexPath.row))
                            
                            self.selectedstring = NSString(format:"%d%@%d%@%d", 0, ":", dic["minutes"].intValue,":",0) as String
                            
                            self.selectedhour = 0
                            self.selectminutes = dic["minutes"].intValue
                            self.selectsceonds = 0
                            
                            print(self.selectedstring)
                            
                            self.plan_id = dic["id"].stringValue
                            print(self.plan_id)
                            self.add_pin_top()
                        }
                    }
                    else
                    {
                        self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                    }
                }
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.tableView_Top
        {
            return 70
        }
        else
        {
            return 44
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_plan_list.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
//    deinit
//    {
//        self.countdownTimer?.invalidate()
//        self.countdownTimer = nil
//    }

    //MARK:Api Call
    func add_pin_top()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "plan":self.plan_id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "add_pin_top"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let message1 = swiftyJsonVar["message"].stringValue as String
                                    
                                    if (message1 == "Already added in top")
                                    {
                                        let message1 = swiftyJsonVar["message"].stringValue as String
                                        self.appDelegate.showAlert(title: "Basmah",message: message1,buttonTitle: "OK");
                                    }
                                    else
                                    {
                                        self.show_pin_top()
                                    }
                                }
                                else
                                {
                                    self.hud.dismiss()
                                    
                                    let message1 = swiftyJsonVar["message"].stringValue as String
                                    self.appDelegate.showAlert(title: "Basmah",message: message1,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    func timeString(time:TimeInterval) -> String
    {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
}


