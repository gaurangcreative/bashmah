//
//  FollowingViewController.swift
//  Basmah
//
//  Created by CT on 9/14/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import SwiftyJSON

class FollowingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_following:NSMutableArray = NSMutableArray()
    var arr_roomfollow:NSMutableArray = NSMutableArray()
    var Checkuserorfollowing:String = String()
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var img_room: UIImageView!
    @IBOutlet weak var img_users: UIImageView!
    @IBOutlet weak var btnRoom: UIButton!
    @IBOutlet weak var btnUser: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Checkuserorfollowing = "Rooms"
        self.img_users.isHidden = true
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    self.FollowRoomlist()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Followinglist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":"1" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["user_list"].array! as NSArray
                                    self.arr_following = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func FollowRoomlist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":"1" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "room_list_others"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let arr = swiftyJsonVar["room_list"].array! as NSArray
                                    self.arr_roomfollow = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK:IBActions
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonRooms(_ sender: Any)
    {
        Checkuserorfollowing = "Rooms"
        self.img_room.isHidden = false
        self.img_users.isHidden = true
        self.img_room.backgroundColor = UIColor.darkGray
        self.btnRoom.setTitleColor(UIColor.darkGray, for: .normal)
        self.btnUser.setTitleColor(UIColor.lightGray, for: .normal)
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    // self.Followinglist()
                    self.FollowRoomlist()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonUsers(_ sender: Any)
    {
        Checkuserorfollowing = "Users"
        self.img_room.isHidden = true
        self.img_users.isHidden = false
        self.img_users.backgroundColor = UIColor.darkGray
        self.btnUser.setTitleColor(UIColor.darkGray, for: .normal)
        self.btnRoom.setTitleColor(UIColor.lightGray, for: .normal)

        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    self.Followinglist()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    //MARK: Table View Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (Checkuserorfollowing == "Rooms")
        {
            return arr_roomfollow.count
        }
        else
        {
            return arr_following.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (Checkuserorfollowing == "Rooms")
        {
            let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
            
            let dic = JSON(self.arr_roomfollow.object(at: indexPath.row))
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            cell.lbl_countryname.text = dic["room_name"].stringValue.firstUppercased
            cell.lbl_date.text = dic["tags"].stringValue
            
            if (dic["gender"].stringValue == "")
            {
                cell.img_gender.isHidden = true
            }
            else if (dic["gender"].stringValue == "male")
            {
                cell.img_gender.image = #imageLiteral(resourceName: "male_icon")
            }
            else
            {
                cell.img_gender.image = #imageLiteral(resourceName: "female_icon")
            }
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            return cell
        }
        else
        {
            let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
            
            let dic = JSON(self.arr_following.object(at: indexPath.row))
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            cell.lbl_countryname.text = dic["username"].stringValue.firstUppercased
            cell.lbl_date.text = dic["user_country"].stringValue
            
            if (dic["gender"].stringValue == "")
            {
                cell.img_gender.isHidden = true
                
            }
            else if (dic["gender"].stringValue == "male")
            {
                cell.img_gender.image = #imageLiteral(resourceName: "male_icon")
            }
            else
            {
                cell.img_gender.image = #imageLiteral(resourceName: "female_icon")
            }
            
            cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if (Checkuserorfollowing == "Rooms")
        {
            var numOfSections: Int = 0
            
            if arr_roomfollow.count > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections = 1
                tableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            
            return numOfSections
        }
        else
        {
            var numOfSections: Int = 0
            
            if arr_following.count > 0
            {
                tableView.separatorStyle = .singleLine
                numOfSections = 1
                tableView.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            return numOfSections
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
