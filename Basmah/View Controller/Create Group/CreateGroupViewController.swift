
//
//  CreateGroupViewController.swift
//  Bashmah
//
//  Created by CTIMac on 21/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class CreateGroupViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , UITableViewDelegate , UITableViewDataSource
{
    var packamount:[String] = ["Free","2000","2000","2000","2000"]
    var roomArry : [String] = ["Room Capacity","Room Admin", "Room Members"]
    var roomImg : [String] = ["capacity_icon" , "admin_icon" ,"group_member_icon"]
    var arr_plan:NSMutableArray = NSMutableArray()
    var statusint : Int = Int()
    var plan_id:String = String()
    var price:String = String()
    var appDelegate = AppDelegate()
    var hud = JGProgressHUD()
    var user_name:String = String()
    var user_image:String = String()
    var image_Data = UIImageJPEGRepresentation(UIImage(),0)

    @IBOutlet weak var tableView_Room: UITableView!
    @IBOutlet weak var collectionView_Package: UICollectionView!
    @IBOutlet weak var view_shadow: UIView!
    @IBOutlet weak var btnCreate: GradientBtn!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Do any additional setup after loading the view.
        
        btnCreate.layer.cornerRadius = 20.0
        btnCreate.layer.masksToBounds = true
        
        view_shadow.layer.cornerRadius = 8.0
        view_shadow.layer.masksToBounds = true

        view_shadow.layer.shadowColor = UIColor.lightGray.cgColor
        view_shadow.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_shadow.layer.shadowOpacity = 0.5
        view_shadow.layer.shadowRadius = 2.0
        view_shadow.layer.masksToBounds = false
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                     UserDetails()
                     Plan_list()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Api Call
    func UserDetails()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":id as AnyObject,
                    "type":"2" as AnyObject,
                    "my_id":"" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_detail"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let dic = swiftyJsonVar["userinfo"]
                                    self.user_name = dic["username"].stringValue
                                    
                                    let imageUrl = URL(string: dic["user_image"].stringValue)!
                                    let imageData = try! Data(contentsOf: imageUrl)
                                    let image = UIImage(data: imageData)
                                    self.image_Data = UIImageJPEGRepresentation(image!, 0.5)
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: Api Call
    func Plan_list()
    {
            let parameters: Parameters =
                [
                    :
                ]
            
            print(parameters)
            
            let url = kBaseURL + "plan_list"
//            hud = JGProgressHUD(style: .dark)
//            hud.textLabel.text = "Loading"
//            hud.show(in: self.view)
        
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                //self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["plan_list"].array! as NSArray
                                    self.arr_plan = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.collectionView_Package)
                                self.collectionView_Package.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    //self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
    }
    
    //MARK:Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arr_plan.count
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:CreateGroupCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateGroupCell", for: indexPath) as! CreateGroupCollectionViewCell
            
        let dic = JSON(arr_plan.object(at: indexPath.row))
        cell.lbl_PackName.text = dic["title"].stringValue
        cell.lbl_PackAmount.text = dic["price"].stringValue
        
        if indexPath.row == statusint
        {
            cell.img_checkIcon.isHidden = false
            cell.img_checkIcon?.image = #imageLiteral(resourceName: "check_icon")
            
            print("\(statusint)")
        }
        else
        {
            cell.img_checkIcon.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:self.collectionView_Package.frame.width/3-25, height:120)
    }
    
    func numberOfSections(in collection: UICollectionView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_plan.count > 0
        {
            numOfSections = 1
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: collection.bounds.size.width, height: collection.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
        }
        
        return numOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        statusint = indexPath.item
        let dic = JSON(arr_plan.object(at: indexPath.item))
        plan_id = dic["id"].stringValue
        price = dic["price"].stringValue
        
        collectionView_Package.reloadData()
    }

    //MARK: Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return roomArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView_Room.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
        cell.lbl_countryname?.text = roomArry[indexPath.row]
        cell.img_country.image = UIImage(named: roomImg[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    //MARK:IBAction
    @IBAction func doclickonCreate(_ sender: Any)
    {
        if (plan_id == "")
        {
           self.appDelegate.showAlert(title: "Basmah",message:"Please select plan first",buttonTitle: "OK");
        }
        else
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        self.CreateGroup()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func CreateGroup()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "room_name":self.user_name as AnyObject,
                    "plan_id":self.plan_id as AnyObject,
                    "price":self.price as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "add_room"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if let data = self.image_Data
                {
                    multipartFormData.append(data, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.navigationController?.popViewController(animated: true)
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
}
