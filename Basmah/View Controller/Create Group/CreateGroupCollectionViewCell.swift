//
//  CreateGroupCollectionViewCell.swift
//  Bashmah
//
//  Created by CTIMac on 21/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class CreateGroupCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var img_checkIcon: UIImageView!
    @IBOutlet weak var lbl_PackAmount: UILabel!
    @IBOutlet weak var lbl_PackName: UILabel!
}
