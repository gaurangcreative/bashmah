//
//  StoreViewController.swift
//  Bashmah
//
//  Created by CTIMac on 14/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class StoreViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var collectionView_User: UICollectionView!
    @IBOutlet weak var collectionView_Room: UICollectionView!
    
    var tableData: [String] = ["Room Lock", "Pin on Top"]
    var roomImage: [String] = ["room_lock", "pin_top"]
    var userImage: [String] = ["big_id_icon"]
    var UserData: [String] = ["Unique ID"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(collectionView == collectionView_Room)
        {
            return tableData.count
        }
        else
        {
            return UserData.count
        }
    }
    
    func collectionView( _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if(collectionView == collectionView_Room)
        {
           let cell:StoreCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell", for: indexPath) as! StoreCollectionViewCell
        
           cell.lblTitle.text = tableData[indexPath.row]
           cell.imageView_Room.image = UIImage(named: roomImage[indexPath.row])
           return cell
        }
        else
        {
            let cell:UserCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCell", for: indexPath) as! UserCollectionViewCell
            
            cell.lblUser.text = UserData[indexPath.row]
            cell.imageView_Unique.image = UIImage(named: userImage[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if(collectionView == collectionView_Room)
        {
            if(indexPath.item == 0)
            {
                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "LockRoomViewController") as! LockRoomViewController
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
            else
            {
                let lock = self.storyboard?.instantiateViewController(withIdentifier: "PinOnTopViewController") as! PinOnTopViewController
                self.navigationController?.pushViewController(lock, animated: true)
            }
        }
        else
        {
            let unique = self.storyboard?.instantiateViewController(withIdentifier: "UniqueIDViewController") as! UniqueIDViewController
            self.navigationController?.pushViewController(unique, animated: true)
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
       return CGSize(width:self.view.frame.width/2-35,height:105)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 15
    }
}
