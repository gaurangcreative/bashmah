//
//  GroupChatTableViewCell.swift
//  Basmah
//
//  Created by CT on 10/4/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class GroupChatTableViewCell: UITableViewCell
{
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var lbl_msg: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
