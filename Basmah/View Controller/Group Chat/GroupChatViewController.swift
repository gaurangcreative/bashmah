//
//  GroupChatViewController.swift
//  Basmah
//
//  Created by CT on 10/4/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD
import SwiftyGif
import Kingfisher

class GroupChatViewController:UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource
{
   var textArray : [String] = ["Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudLorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud","exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.Rahul Sharmaexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda." , "Rahul Sharm" , "Rahul Sharmaexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda." , "Rahul Sharmaexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,"]
    
    
    private let arr_otherroom = ["Member",
                              "Sound off",
                                ]
    
    
    private let arr_otherroomimg = ["visitors_icon",
                                 "mike_off",
                                   ]
    
    var room_id:String = String()
    var follow_status:String = String()
    var membership_fee:String = String()
    var checkController:String = String()
    var room_lock_status:String = String()
    var OpenClosetable:Bool = Bool()
    var appDelegate = AppDelegate()
    var hud = JGProgressHUD()
    var selectedIndex = Int ()
    var arr_giflist:NSMutableArray = NSMutableArray()
    var arr_member_list:NSMutableArray = NSMutableArray()
    var arr_groupchat:NSMutableArray = NSMutableArray()
    let imagePicker = UIImagePickerController()
    var image_Data = UIImageJPEGRepresentation(UIImage(),0)
    var type:String = String()
    var gift_id:String = String()
    var other_user_id:String = String()
    var selectedRow = 0;
    var user_image:String = String()

    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var table_more: UITableView!
    @IBOutlet weak var collection_view: UICollectionView!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var img_bg: UIImageView!
    @IBOutlet weak var btnKeep: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var view_gift: UIView!
    @IBOutlet weak var btnAddRoom: UIButton!
    @IBOutlet weak var collection_member: UICollectionView!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_roomname: UILabel!
    @IBOutlet weak var lbl_userid: UILabel!
    @IBOutlet weak var img_country: UIImageView!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var btnYes: GradientBtn!
    @IBOutlet weak var btnNo: GradientBtn!
    @IBOutlet weak var view_sendmsg: UIView!
    @IBOutlet weak var view_bordermsg: AngleGradientBorderView!
    @IBOutlet weak var view_giftborder: AngleGradientBorderView!
    @IBOutlet weak var btnSendGift: GradientBtn!
    @IBOutlet weak var btnSelectUserGift: UIButton!
    @IBOutlet weak var txt_send_grpmsg: UITextField!
    @IBOutlet weak var view_groupmember: UIView!
    @IBOutlet weak var picker_view: UIPickerView!
    @IBOutlet weak var img_other_user_gif: UIImageView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.table_view.estimatedRowHeight = 140
        self.table_view.rowHeight = UITableViewAutomaticDimension
        
        view_password.isHidden = true
        txt_send_grpmsg.delegate = self
        self.view_sendmsg.isHidden = true
        self.view_groupmember.isHidden = true
        
        selectedIndex = -1
        
        btnSendGift.layer.cornerRadius = 20.0
        btnSendGift.layer.masksToBounds = true
        
        let startColor = UIColor(red: 58/255.0, green: 102/255.0, blue: 234/255.0, alpha: 1.0)
        let endColor = UIColor(red: 222/255.0, green: 106/255.0, blue: 197/255.0, alpha: 1.0)
        
        let angleGradientBorderView1Colors: [AnyObject] = [
            startColor.cgColor,
            endColor.cgColor,]

        // Change the colors of angleGradientBorderView1 and its border width
        view_bordermsg.setupGradientLayer(borderColors: angleGradientBorderView1Colors, borderWidth: 1)
        view_giftborder.setupGradientLayer(borderColors: angleGradientBorderView1Colors, borderWidth: 1)
        
        btnYes.layer.cornerRadius = 20.0
        btnYes.layer.masksToBounds = true
        btnNo.layer.cornerRadius = 20.0
        btnNo.layer.masksToBounds = true
        view_sendmsg.layer.cornerRadius = 4.0
        view_sendmsg.layer.masksToBounds = true

        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if (checkController == "MyRoom")
        {
            OpenClosetable = false
            table_more.isHidden = true
            btnFollow.isHidden  = true
            btnAddRoom.isHidden = true
        }
        else
        {
            OpenClosetable = false
            table_more.isHidden = true
            btnFollow.isHidden  = false
            btnAddRoom.isHidden = false
        }
        
        //table_view.register(UINib(nibName: "GroupchatImageTableViewCell", bundle: nil), forCellReuseIdentifier:"GroupChatImage")

        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    Getroomdetail()
                    Group_chatHistory()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Collection View Data Source
    func numberOfSections(in collectionview: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if (collectionView == collection_view)
        {
            return arr_giflist.count
        }
        else
        {
            return arr_member_list.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if (collectionView == collection_view)
        {
            let cell:StoreCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell", for: indexPath as IndexPath) as! StoreCollectionViewCell
            
            let dic = JSON(self.arr_giflist.object(at: indexPath.row))
            
            cell.img_check.isHidden = false
            
            cell.imageView_Room.kf.indicatorType = .activity
            (cell.imageView_Room.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.imageView_Room.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            cell.lbl_giftprice.text = "$" + dic["price"].stringValue
            cell.lbl_giftprice.textColor = UIColor(red: 151/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0)
            
            if selectedIndex == indexPath.item
            {
                //cell.img_check.image = #imageLiteral(resourceName: "check_icon")
                
                cell.layer.cornerRadius = 4.0
                cell.layer.masksToBounds = true
                cell.layer.borderWidth = 1.0
                cell.layer.borderColor = UIColor(red: 151/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0).cgColor
            }
            else
            {
                cell.layer.cornerRadius = 4.0
                cell.layer.masksToBounds = true
                cell.layer.borderWidth = 1.0
                cell.layer.borderColor = UIColor.clear.cgColor
                //cell.img_check.image = nil
            }
            
            return cell
        }
        else
        {
            let cell:NewlyCreatedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCell", for: indexPath as IndexPath) as! NewlyCreatedCollectionViewCell
            
            cell.img_user.layer.cornerRadius = cell.img_user.layer.frame.size.width/2
            cell.img_user.layer.masksToBounds = true
            
            let dic = JSON(self.arr_member_list.object(at: indexPath.item))
            cell.lbl_title.text = dic["username"].stringValue
            
            cell.img_user.layer.borderWidth = 2.0
            cell.img_user.layer.borderColor = UIColor.white.cgColor
            cell.img_user.kf.indicatorType = .activity
            (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
            cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if (collectionView == collection_view)
        {
           return CGSize(width:self.view.frame.size.width/3-16,height:self.view.frame.size.width/3-16)
        }
        else
        {
           return CGSize(width:self.view.frame.size.width/5-5,height:80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if (collectionView == collection_view)
        {
            let dic = JSON(self.arr_giflist.object(at: indexPath.row))
            selectedIndex = indexPath.item
            gift_id = dic["id"].stringValue
            collection_view.reloadData()
        }
    }
    
     func numberOfSectionsColl(in collection: UICollectionView) -> Int
     {
        if (collection == collection_member)
        {
            var numOfSections: Int = 0
            
            if arr_member_list.count > 0
            {
                numOfSections = 1
                collection_member.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collection_member.bounds.size.width, height: collection_member.bounds.size.height))
                noDataLabel.text          = "No members found."
                noDataLabel.textColor     = UIColor.white
                noDataLabel.textAlignment = .center
                collection_member.backgroundView  = noDataLabel
            }
            return numOfSections
        }
        else
        {
            var numOfSections: Int = 0
            
            if arr_giflist.count > 0
            {
                numOfSections = 1
                collection_view.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collection_view.bounds.size.width, height: collection_view.bounds.size.height))
                noDataLabel.text          = "No gifts found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                collection_view.backgroundView  = noDataLabel
            }
            return numOfSections
        }
    }

    //MARK:Table View Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (checkController == "MyRoom")
        {
            if (tableView == table_view)
            {
                return textArray.count
            }
            else
            {
                return 4
            }
        }
        else
        {
            if (tableView == table_view)
            {
                return arr_groupchat.count
            }
            else
            {
                return 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (checkController == "MyRoom")
        {
            if (tableView == table_view)
            {
                let cell:GroupChatTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GCell", for: indexPath) as! GroupChatTableViewCell
                let text = textArray[indexPath.row]
                cell.selectionStyle = .none
                cell.img_user.layer.cornerRadius = cell.img_user.layer.frame.size.width/2
                cell.img_user.layer.masksToBounds = true
                cell.view_bg.layer.cornerRadius = 6.0
                cell.view_bg.layer.masksToBounds = true
                cell.lbl_msg.text = text
                return cell
            }
            else
            {
                let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
                cell.selectionStyle = .none
                
                if (indexPath.row == 0)
                {
                    cell.lbl_countryname.text = "Setting"
                    cell.img_user.image = #imageLiteral(resourceName: "room_settingnew")
                }
                else if (indexPath.row == 1)
                {
                    cell.lbl_countryname.text = "Member"
                    cell.img_user.image = #imageLiteral(resourceName: "visitors_icon")
                }
                else if (indexPath.row == 2)
                {
                    if (self.room_lock_status == "1")
                    {
                        cell.lbl_countryname.text = "Unlock"
                        cell.img_user.image = #imageLiteral(resourceName: "lock_icon")
                    }
                    else
                    {
                        cell.lbl_countryname.text = "Lock"
                        cell.img_user.image = #imageLiteral(resourceName: "unlock")
                    }
                }
                else
                {
                     cell.lbl_countryname.text = "Sound off"
                     cell.img_user.image = #imageLiteral(resourceName: "mike_off")
                }
                
                return cell
            }
        }
        else
        {
            if (tableView == table_view)
            {
                let dic = JSON(self.arr_groupchat.object(at: indexPath.row))
                
                if (dic["message_staus"].stringValue == "1")
                {
                    let cell:GroupChatTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GCell", for: indexPath) as! GroupChatTableViewCell
                    
                    let dic = JSON(self.arr_groupchat.object(at: indexPath.row))
                    let decodevalue = dic["message"].stringValue.base64Decoded()
                    let text  = decodevalue
                    cell.lbl_msg.text = text
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                    let date = dateFormatter.date(from: dic["created_at"].stringValue)
                    dateFormatter.dateFormat = "h:mm a"
                    let final =  dateFormatter.string(from: date!)
                    cell.lbl_time.text = final
                    
                    cell.img_user.layer.borderWidth = 2.0
                    cell.img_user.layer.borderColor = UIColor.white.cgColor
                    cell.img_user.kf.indicatorType = .activity
                    (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_user.kf.setImage(with: URL(string:dic["user_image"].stringValue), placeholder: UIImage(named: "visitors_icon"))
                    
                    cell.lbl_username.text = dic["username"].stringValue
                    
                    cell.selectionStyle = .none
                    cell.img_user.layer.cornerRadius = cell.img_user.layer.frame.size.width/2
                    cell.img_user.layer.masksToBounds = true
                    cell.view_bg.layer.cornerRadius = 6.0
                    cell.view_bg.layer.masksToBounds = true
                    return cell
                }
                else if (dic["message_staus"].stringValue == "2")
                {
                    let cell:GroupchatImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GroupChatImage", for: indexPath) as! GroupchatImageTableViewCell
                    
                    let dic = JSON(self.arr_groupchat.object(at: indexPath.row))
                    
                    cell.img_user.layer.borderWidth = 2.0
                    cell.img_user.layer.borderColor = UIColor.white.cgColor
                    cell.img_user.kf.indicatorType = .activity
                    (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_user.kf.setImage(with: URL(string:dic["user_image"].stringValue), placeholder: UIImage(named: "visitors_icon"))
                    
                    cell.img_chat.kf.indicatorType = .activity
                    (cell.img_chat.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.white
                    cell.img_chat.kf.setImage(with: URL(string:dic["message"].stringValue), placeholder: UIImage(named: ""))
                    cell.img_chat.layer.borderWidth = 2.0
                    cell.img_chat.layer.borderColor = UIColor.white.cgColor
                    cell.img_chat.layer.cornerRadius  = 4.0
                    cell.img_chat.layer.masksToBounds = true
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                    let date = dateFormatter.date(from: dic["created_at"].stringValue)
                    dateFormatter.dateFormat = "h:mm a"
                    let final =  dateFormatter.string(from: date!)
                    cell.lbl_time.text = final
                    
                    cell.view_bg.layer.cornerRadius  = 6.0
                    cell.view_bg.layer.masksToBounds = true
                    
                    //cell.lbl_username.text = dic["username"].stringValue
                    
                    cell.selectionStyle = .none
                    cell.img_user.layer.cornerRadius = cell.img_user.layer.frame.size.width/2
                    cell.img_user.layer.masksToBounds = true

                    return cell
                }
                else
                {
                    let cell:GifTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GifCell", for: indexPath) as! GifTableViewCell
                    
                    let dic = JSON(self.arr_groupchat.object(at: indexPath.row))
                    
                    cell.img_other_image.kf.indicatorType = .activity
                    (cell.img_other_image.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_other_image.kf.setImage(with: URL(string:dic["user_image"].stringValue), placeholder: UIImage(named: "visitors_icon"))
                    
                    cell.lbl_other_form.text = dic["gif_to_user"].stringValue
                    cell.lbl_user_from.text = dic["username"].stringValue
                    
                    cell.img_gif.kf.indicatorType = .activity
                    (cell.img_gif.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_gif.kf.setImage(with: URL(string:dic["message"].stringValue), placeholder: UIImage(named: ""))
                    
                    cell.selectionStyle = .none
                    
                    cell.img_from.kf.indicatorType = .activity
                    (cell.img_from.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_from.kf.setImage(with: URL(string:user_image), placeholder: UIImage(named: ""))
                    
                    cell.img_from.layer.borderWidth = 2.0
                    cell.img_from.layer.borderColor = UIColor.white.cgColor
                    cell.img_from.layer.cornerRadius = cell.img_from.layer.frame.size.width/2
                    cell.img_from.layer.masksToBounds = true

                    cell.img_other_image.layer.borderWidth = 2.0
                    cell.img_other_image.layer.borderColor = UIColor.white.cgColor
                    cell.img_other_image.layer.cornerRadius = cell.img_other_image.frame.size.width/2
                    cell.img_other_image.layer.masksToBounds = true
                    
                    cell.view_bg.layer.cornerRadius = 6.0
                    cell.view_bg.layer.masksToBounds = true
                    return cell
                }
            }
            else
            {
                let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
                cell.selectionStyle = .none
                cell.lbl_countryname.text = arr_otherroom[indexPath.row]
                cell.img_user.image = UIImage(named:arr_otherroomimg[indexPath.row])
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (checkController == "MyRoom")
        {
            if (tableView == table_view)
            {
                
            }
            else
            {
                if (indexPath.row == 0)
                {
                    table_more.isHidden = true
                    OpenClosetable = false
                    
                    let roomsetting = self.storyboard?.instantiateViewController(withIdentifier: "RoomSettingViewController") as! RoomSettingViewController
                    roomsetting.room_id = room_id
                    self.navigationController?.pushViewController(roomsetting, animated: true)
                }
                else if (indexPath.row == 1)
                {
                    table_more.isHidden = true
                    OpenClosetable = false
                    
                    let member = self.storyboard?.instantiateViewController(withIdentifier: "MemberListViewController") as! MemberListViewController
                    member.room_id = room_id
                    self.navigationController?.pushViewController(member, animated: true)
                }
                else if (indexPath.row == 2)
                {
                    if (self.room_lock_status == "1")
                    {
                        self.room_lock_status = "2"
                    }
                    else
                    {
                        self.room_lock_status = "1"
                    }
                    
                    self.room_lock_manage()
                }
            }
        }
        else
        {
            if (tableView == table_view)
            {
                
            }
            else
            {
                if (indexPath.row == 0)
                {
                    table_more.isHidden = true
                    OpenClosetable = false
                    
                    let member = self.storyboard?.instantiateViewController(withIdentifier: "MemberListViewController") as! MemberListViewController
                    member.room_id = room_id
                    self.navigationController?.pushViewController(member, animated: true)
                }
                else if (indexPath.row == 1)
                {
                    table_more.isHidden = true
                    OpenClosetable = false
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_groupchat.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.white
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (checkController == "MyRoom")
        {
            return tableView.rowHeight
        }
        else
        {
            if (arr_groupchat.count>0)
            {
                let dic = JSON(self.arr_groupchat.object(at: indexPath.row))
                
                let msg_status = dic["message_staus"].stringValue
                
                if(msg_status == "1")
                {
                    return tableView.rowHeight
                }
                else if (msg_status == "2")
                {
                    return 200
                }
                else if (msg_status == "3")
                {
                    return 140
                }
                else
                {
                    return tableView.rowHeight
                }
            }
            else
            {
              return 0
            }
        }
    }
    
    //MARK:- Picker View Data Source
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arr_member_list.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let dic = JSON(self.arr_member_list.object(at:row))
        return dic["username"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedRow = row;
    }
    
    //MARK:Text Field Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if ((textField.text?.characters.count)!>0)
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        SendGroupmessage()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
        else
        {
            self.appDelegate.showAlert(title: "Basmah",message:"",buttonTitle: "OK");
        }
        
        return true
    }
    
    //MARK:IBAction
    @IBAction func doclickonSelectUserGift(_ sender: Any)
    {
        self.view_groupmember.isHidden = false
        
        if (arr_member_list.count>0)
        {
             self.picker_view.isUserInteractionEnabled = true
             self.picker_view.reloadAllComponents()
        }
        else
        {
             self.picker_view.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func doclickonSendGift(_ sender: Any)
    {
        if (selectedIndex == -1)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please select gif first",buttonTitle: "OK");
        }
        else
        {
            if (btnSelectUserGift.titleLabel?.text == "Select User")
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Please select user which you want to send gif",buttonTitle: "OK");
            }
            else
            {
                self.view_gift.isHidden = true
                
                type = "3"
                
                // Add reachability observer
                if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                {
                    if reachability.connection != .none
                    {
                        if reachability.connection == .wifi || reachability.connection == .cellular
                        {
                           SendGroupmessage()
                        }
                    }
                    else
                    {
                        self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                    }
                }
            }
        }
    }
    
    @IBAction func doclickonCancelPicker(_ sender: Any)
    {
         self.view_groupmember.isHidden = true
    }
    
    @IBAction func doclickonDonePicker(_ sender: Any)
    {
        self.view_groupmember.isHidden = true
        let dic = JSON(self.arr_member_list.object(at:selectedRow))
        other_user_id = dic["id"].stringValue
        self.btnSelectUserGift.setTitle(dic["username"].stringValue, for: .normal)
        self.btnSelectUserGift.setTitleColor(UIColor(red: 151/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0), for: .normal)
        
        self.img_other_user_gif.kf.indicatorType = .activity
        self.img_other_user_gif.layer.cornerRadius = self.img_other_user_gif.frame.size.width/2
        self.img_other_user_gif.layer.masksToBounds = true
        (self.img_other_user_gif.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        self.img_other_user_gif.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
    }
    
    @IBAction func doclickonCloseGroupchat(_ sender: Any)
    {
        self.view_sendmsg.isHidden = true
    }
    
    @IBAction func doclickonFollow(_ sender: Any)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    if (self.follow_status == "2")
                    {
                         Follow_room()
                    }
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonLuckyGold(_ sender: Any)
    {
        
    }
    
    @IBAction func doclickonAddRoomMember(_ sender: Any)
    {
        let refreshAlert = UIAlertController(title: "Basmah", message: "You have to pay $ " + membership_fee + " for this Room Member", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        self.Addroommember()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func doclickonShare(_ sender: Any)
    {
        
    }
    
    @IBAction func doclickonMessage(_ sender: Any)
    {
        self.view_sendmsg.isHidden = false
        type = "1"
    }
    
    @IBAction func doclickonPickImage(_ sender: Any)
    {
        type = "2"
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler:
            { _ in
                self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func doclickonContribution(_ sender: Any)
    {
        
    }
    
    @IBAction func doclickonMore(_ sender: Any)
    {
        if (OpenClosetable == false)
        {
            table_more.isHidden = false
            OpenClosetable = true
        }
        else
        {
            table_more.isHidden = true
            OpenClosetable = false
        }
    }
    
    @IBAction func doclickonExit(_ sender: Any)
    {
        self.img_bg.isHidden = false
        self.btnKeep.isHidden = false
        self.btnExit.isHidden = false
    }
    
    @IBAction func doclickonLogout(_ sender: Any)
    {
        self.img_bg.isHidden = true
        self.btnKeep.isHidden = true
        self.btnExit.isHidden = true
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonGift(_ sender: Any)
    {
        self.view_gift.isHidden = false
        self.GifList()
    }
    
    @IBAction func doclickonCloseGift(_ sender: Any)
    {
        view_gift.isHidden = true
        selectedIndex = -1
    }
    
    @IBAction func doclickonYes(_ sender: Any)
    {
        set_room_password()
    }
    
    @IBAction func doclickonNo(_ sender: Any)
    {
        self.view_password.isHidden = true
    }
    
    //MARK: Open Image from camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Basmah", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Image from gallery
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:Group Chat
    func SendGroupmessage()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            if (type == "1")
            {
                let data = (txt_send_grpmsg.text)?.data(using: String.Encoding.utf8)
                let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                print(base64)
                
                txt_send_grpmsg.text = ""
                
                let parameters: Parameters =
                    [
                        "user_from":id as AnyObject,
                        "group_id":room_id as AnyObject,
                        "type":type as AnyObject,
                        "message":base64 as AnyObject,
                    ]
                
                print(parameters)
                
                let url = kBaseURL + "group_chat"
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    
                    self.hud.dismiss()
                    
                    switch result
                    {
                    case .success(let upload, _, _):
                        
                        upload.responseJSON { response in
                            
                            print(response.response!)
                            print(response.data!)
                            
                            if (response.result.value == nil)
                            {
                                print("Response nil")
                                self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                            }
                            else
                            {
                                do
                                {
                                    let swiftyJsonVar = JSON(response.result.value!)
                                    
                                    if swiftyJsonVar["success"].intValue == 1
                                    {
                                        self.view.endEditing(true)
                                        self.view_sendmsg.isHidden = true
                                        self.Group_chatHistory()
                                    }
                                    else
                                    {
                                        let message = swiftyJsonVar["message"].stringValue
                                        self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                    }
                                }
                            }
                        }
                        
                        case .failure(let error):
                        self.hud.dismiss()
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
            }
            else if (type == "2")
            {
                    let parameters: Parameters =
                        [
                            "user_from":id as AnyObject,
                            "group_id":room_id as AnyObject,
                            "type":type as AnyObject,
                        ]
                    
                    print(parameters)
                    
                    let url = kBaseURL + "group_chat"
                    hud = JGProgressHUD(style: .dark)
                    hud.textLabel.text = "Loading"
                    hud.show(in: self.view)
                    
                    let headers: HTTPHeaders = [
                        /* "Authorization": "your_access_token",  in case you need authorization header */
                        "Content-type": "multipart/form-data",
                        "Authorization": "dfs#!df154$",
                        ]
                    
                    Alamofire.upload(multipartFormData: { (multipartFormData) in
                        
                        for (key, value) in parameters
                        {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                        }
                        
                        if let data = self.image_Data
                        {
                            multipartFormData.append(data, withName: "message",fileName: "file.jpg", mimeType: "image/jpg")
                        }
                        
                    }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                        
                        self.hud.dismiss()
                        
                        switch result
                        {
                            case .success(let upload, _, _):
                            
                            upload.responseJSON { response in
                                
                                print(response.response!) // URL response
                                print(response.data!)     // server data
                                
                                if (response.result.value == nil)
                                {
                                    print("Response nil")
                                    self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                                }
                                else
                                {
                                    do
                                    {
                                        let swiftyJsonVar = JSON(response.result.value!)
                                        
                                        if swiftyJsonVar["success"].intValue == 1
                                        {
                                            self.Group_chatHistory()
                                        }
                                        else
                                        {
                                            let message = swiftyJsonVar["message"].stringValue
                                            self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                        }
                                    }
                                }
                            }
                            
                            case .failure(let error):
                            self.hud.dismiss()
                            print("Error in upload: \(error.localizedDescription)")
                        }
                    }
            }
            else
            {
                let parameters: Parameters =
                    [
                        "user_from":id as AnyObject,
                        "group_id":room_id as AnyObject,
                        "type":type as AnyObject,
                        "message":gift_id  as AnyObject,
                        "user_id":other_user_id as AnyObject,
                    ]
                
                print(parameters)
                
                let url = kBaseURL + "group_chat"
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    
                    self.hud.dismiss()
                    
                    switch result
                    {
                    case .success(let upload, _, _):
                        
                        upload.responseJSON { response in
                            
                            print(response.response!)
                            print(response.data!)
                            
                            if (response.result.value == nil)
                            {
                                print("Response nil")
                                self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                            }
                            else
                            {
                                do
                                {
                                    let swiftyJsonVar = JSON(response.result.value!)
                                    
                                    if swiftyJsonVar["success"].intValue == 1
                                    {
                                        self.Group_chatHistory()
                                    }
                                    else
                                    {
                                        let message = swiftyJsonVar["message"].stringValue
                                        self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                    }
                                }
                            }
                        }
                        
                        case .failure(let error):
                        self.hud.dismiss()
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
     //MARK:Group Chat History
    func Group_chatHistory()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")as! String
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "group_id":room_id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "group_chatHistory"
//            hud = JGProgressHUD(style: .dark)
//            hud.textLabel.text = "Loading"
//            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                //self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["chat_history"].array! as NSArray
                                    self.arr_groupchat = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    
                                }
                            }
                            
                            if (self.arr_groupchat.count>0)
                            {
                                DispatchQueue.main.async
                                    {
                                        let indexPath = IndexPath(row: self.arr_groupchat.count-1, section: 0)
                                        self.table_view.scrollToRow(at: indexPath, at: .bottom, animated: true)
                                }
                            }
                            
                            self.numberOfSections(in: self.table_view)
                            self.table_view.reloadData()
                        }
                    }
                    
                    case .failure(let error):
                    //self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                    
                }
            }
        }
    }
    
    //MARK:Set Password
    func set_room_password()
    {
        let parameters: Parameters =
            [
                "room_id":room_id as AnyObject,
                "password":txt_password.text as AnyObject,
            ]
        
        print(parameters)
        
        let url = kBaseURL + "set_room_password"
        hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data",
            "Authorization": "dfs#!df154$",
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            self.hud.dismiss()
            
            switch result
            {
                case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    
                    if (response.result.value == nil)
                    {
                        print("Response nil")
                        self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                    }
                    else
                    {
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                self.hud.dismiss()
                                
                                self.view_password.isHidden = true
                                
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                            else
                            {
                                self.hud.dismiss()
                                
                                let message = swiftyJsonVar["message"].stringValue
                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                }
                
                case .failure(let error):
                self.hud.dismiss()
                print("Error in upload: \(error.localizedDescription)")
                
              }
        }
    }
    
    //MARK:Room Lock
    func room_lock_manage()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")as! String
            
            let parameters: Parameters =
                [
                    "room_id":room_id as AnyObject,
                    "user_id":id as AnyObject,
                    "status":self.room_lock_status as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "room_lock_manage"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    
                                    let message1 = swiftyJsonVar["status"].intValue
                                    
                                    if (message1 == 1)
                                    {
                                        //self.appDelegate.showAlert(title: "Basmah",message: message1,buttonTitle: "OK");
                                        
                                        let alert = UIAlertController(title: "Basmah", message: "You haven't purchased Room Lock yet,confirm to buy?", preferredStyle: UIAlertControllerStyle.alert)
                                        
                                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                            switch action.style{
                                            case .default:
                                                print("default")
                                                
                                                let recharge = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                                                self.navigationController?.pushViewController(recharge, animated: true)
                                                
                                            case .cancel:
                                                print("cancel")
                                                
                                            case .destructive:
                                                print("destructive")
                                            }}))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else
                                    {
                                        let message1 = swiftyJsonVar["message"].stringValue
                                        
                                        self.appDelegate.showAlert(title: "Basmah",message:
                                            message1,buttonTitle: "OK");
                                        
                                        self.table_more.reloadData()
                                        
                                        self.view_password.isHidden = false
                                    }
                                }
                                else
                                {
                                    self.hud.dismiss()
                                    
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK:Room Members
    func Addroommember()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")as! String
            
            let parameters: Parameters =
                [
                    "room_id":room_id as AnyObject,
                    "user_id":id as AnyObject,
                    "price":membership_fee as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "add_room_member"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                else
                                {
                                    self.hud.dismiss()
                                    
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }

    //MARK:Room Detail
    func Getroomdetail()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")as! String
            
            let parameters: Parameters =
                [
                    "room_id":room_id as AnyObject,
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "room_detail"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    
                                    let dic = swiftyJsonVar["room_detail"]
                                    print(dic)
                                    
                                    self.follow_status = dic["follow_status"].stringValue
                                    
                                    if (self.follow_status == "2")
                                    {
                                        self.btnFollow.setImage(#imageLiteral(resourceName: "follow_icon"), for: .normal)
                                    }
                                    else
                                    {
                                        
                                    }
                                    
                                    self.lbl_userid.text = "ID:" + dic["id"].stringValue
                                    self.lbl_roomname.text = dic["room_name"].stringValue
                                    
                                    self.img_user.layer.borderWidth = 2.0
                                    self.img_user.layer.borderColor = UIColor.white.cgColor
                                    self.img_user.kf.indicatorType = .activity
                                    self.img_user.layer.cornerRadius = self.img_user.frame.size.width/2
                                    self.img_user.layer.masksToBounds = true
                                    (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.white
                                    self.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    self.user_image = dic["image"].stringValue
                                    
                                    self.img_country.kf.indicatorType = .activity
                                    (self.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                    self.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    let arr = swiftyJsonVar["member_list"].array! as NSArray
                                    self.arr_member_list = arr.mutableCopy() as! NSMutableArray
                                    
                                    if (self.arr_member_list.count == 0)
                                    {
                                        self.numberOfSectionsColl(in: self.collection_member)
                                        self.collection_member.reloadData()
                                    }
                                    
                                    self.room_lock_status = dic["room_lock_staus"].stringValue
                                    self.membership_fee = dic["membership_fee"].stringValue
                                    
                                    self.collection_member.reloadData()
                                    self.table_more.reloadData()
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK:Follow Room
    func Follow_room()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "follow_uid":id as AnyObject,
                    "status":"1" as AnyObject,
                    "room_id":room_id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "follow_room"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].intValue == 1
                                {
                                    if(swiftyJsonVar["message"] == "Follow added")
                                    {
                                        self.btnFollow.setImage(#imageLiteral(resourceName: "follow_icon_active"), for: UIControlState.normal)
                                    }
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK:Gif list
    func GifList()
    {
            let parameters: Parameters =
                [
                    :
                ]
            
            print(parameters)
            
            let url = kBaseURL + "gif_list"
        
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        print(response.result.value!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["gif_list"].array! as NSArray
                                    self.arr_giflist = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    
                                }
                                
                                self.numberOfSectionsColl(in: self.collection_view)
                                self.collection_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
}

extension GroupChatViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerEditedImage] as? UIImage) != nil
        {
//            self.chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            self.image_Data = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage, 0.5)
            
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        self.SendGroupmessage()

                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
        picker.dismiss(animated: true)
    }
    
}
