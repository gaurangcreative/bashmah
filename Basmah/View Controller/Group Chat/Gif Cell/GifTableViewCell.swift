//
//  GifTableViewCell.swift
//  Basmah
//
//  Created by CT on 12/24/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class GifTableViewCell: UITableViewCell
{
    @IBOutlet weak var img_from: UIImageView!
    @IBOutlet weak var lbl_user_from: UILabel!
    @IBOutlet weak var img_other_image: UIImageView!
    @IBOutlet weak var img_gif: UIImageView!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var lbl_other_form: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
