//
//  GroupchatImageTableViewCell.swift
//  Basmah
//
//  Created by CT on 12/19/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit

class GroupchatImageTableViewCell: UITableViewCell
{
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var img_chat: UIImageView!
    @IBOutlet weak var view_bg: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
