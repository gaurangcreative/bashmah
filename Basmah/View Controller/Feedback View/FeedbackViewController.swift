//
//  FeedbackViewController.swift
//  Bashmah
//
//  Created by CTIMac on 19/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class FeedbackViewController: UIViewController , UITextViewDelegate
{
    @IBOutlet weak var textView_Desc: UITextView!
    @IBOutlet weak var lbl_WordCount: UILabel!
    @IBOutlet weak var btnSubmit: GradientBtn!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var img_user: UIImageView!
    
    var appDelegate = AppDelegate()
    var hud = JGProgressHUD()
    let imagePicker = UIImagePickerController()
    var chosenImage = UIImage()
    var image_Data = UIImageJPEGRepresentation(UIImage(),0)

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       appDelegate = UIApplication.shared.delegate as! AppDelegate
       btnSubmit.layer.cornerRadius = 20.0
       btnSubmit.layer.masksToBounds = true
        
       imagePicker.delegate = self
        
       textView_Desc.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        img_user.addGestureRecognizer(tap)
        img_user.isUserInteractionEnabled = true
        
        self.img_user.layer.cornerRadius = self.img_user.frame.size.width/2
        self.img_user.layer.masksToBounds = true
        self.img_user.layer.borderWidth = 1.0
        self.img_user.layer.borderColor = UIColor.lightGray.cgColor
        
        self.updateCharacterCount()
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler:
            { _ in
                self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Open Image from camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Basmah", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Image from gallery
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func doclickonSubmit(_ sender: Any)
    {
        if (textView_Desc.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter description",buttonTitle: "OK");
        }
        else
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        AddFeedback()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    override func didChangeValue(forKey key: String)
    {
        print(textView_Desc.text.characters.count)
    }
    
    func updateCharacterCount()
    {
        let descriptionCount = self.textView_Desc.text.characters.count
        self.lbl_WordCount.text = "(\((120) - descriptionCount))"
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        self.updateCharacterCount()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
         if(textView == textView_Desc)
         {
            return textView.text.characters.count +  (text.characters.count - range.length) <= 120
        }
        return false
    }
    
    func AddFeedback()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
                let id = UserDefaults.standard.value(forKey: "id")
                let parameters: Parameters =
                    [
                        "user_id":id as AnyObject,
                        "description":textView_Desc.text as AnyObject,
                        "feedback_type": "" as AnyObject,
                    ]
                
                print(parameters)
                
                let url = kBaseURL + "add_feedback"
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                    if let data = self.image_Data
                    {
                        multipartFormData.append(data, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    self.hud.dismiss()
                    switch result
                    {
                        case .success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            print(response.response!) // URL response
                            print(response.data!)     // server data
                            
                            if (response.result.value == nil)
                            {
                                print("Response nil")
                                self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                            }
                            else
                            {
                                do
                                {
                                    let swiftyJsonVar = JSON(response.result.value!)
                                    
                                    if swiftyJsonVar["success"].string == "1"
                                    {
                                        let dict = swiftyJsonVar["userinfo"]
                                        self.navigationController?.popViewController(animated: true)
                                        
                                        let message = swiftyJsonVar["message"].stringValue
                                        self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                    }
                                    else
                                    {
                                        let message = swiftyJsonVar["message"].stringValue
                                        self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                    }
                                }
                            }
                        }
                        
                        case .failure(let error):
                        self.hud.dismiss()
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
        }
    }
}

extension FeedbackViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerEditedImage] as? UIImage) != nil
        {
            self.chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            self.image_Data = UIImageJPEGRepresentation(chosenImage, 1)
            self.img_user.image = self.chosenImage
            self.img_user.contentMode = .scaleAspectFill
        }
        picker.dismiss(animated: true)
    }
    
}
