//
//  ActivityViewController.swift
//  Bashmah
//
//  Created by CTIMac on 17/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController, UITableViewDelegate , UITableViewDataSource
{
    var providerTitleArry : [String] = ["Today 2:30 am","Today 1:30 am", "Today 1:30 am","Today 12:30 am"]

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
    self.navigationController?.popViewController(animated: true)
    }
    
 //MARK: Table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return providerTitleArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
//        cell.lblTime?.text = providerTitleArry[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
}
