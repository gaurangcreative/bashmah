//
//  NotificationViewController.swift
//  Bashmah
//
//  Created by CTIMac on 17/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD


class NotificationViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()

    var providerTitleArry : [String] = ["Today 2:30 am","Today 1:30 am", "Today 1:30 am","Today 12:30 am"]
    var arr_notification:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Notification_list()
    }
    
    func Notification_list()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    ]
            
            print(parameters)
            
            let url = kBaseURL + "notification_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let arr = swiftyJsonVar["notification_list"].array! as NSArray
                                    self.arr_notification = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSections(in: self.tableView)
                                self.tableView.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_notification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CountryListTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
        let dic = JSON(arr_notification.object(at: indexPath.row))
        
        if (dic["type"].stringValue == "1")
        {
            let final:NSMutableAttributedString = NSMutableAttributedString()
            
            let username = dic["username"].stringValue.firstUppercased
            let messagetitle = dic["message"].stringValue
            
            let attrString = NSAttributedString (
                string: username as String,
                attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
            
            let attrString2 = NSAttributedString (
                string: " " as String + messagetitle as String,
                attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
            
            final.append(attrString)
            final.append(attrString2)
            cell.lbl_countryname.attributedText = final
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let date1 = dic["date"].stringValue
            let finaldate = dateFormatter.date(from: date1)
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            let latestdate =  dateFormatter.string(from: finaldate!)
            cell.lbl_date.text = latestdate
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_notification.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
}
