//
//  MyRoomViewController.swift
//  Bashmah
//
//  Created by CTIMac on 17/09/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class MyRoomViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_roomlist:NSMutableArray = NSMutableArray()

    @IBOutlet weak var table_view: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                     self.Myroom()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: Table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_roomlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.table_view.dequeueReusableCell(withIdentifier: "Cell") as! CountryListTableViewCell
        
        let dic = JSON(arr_roomlist.object(at: indexPath.row))
        
        cell.lbl_countryname.text = dic["room_name"].stringValue
        cell.lbl_countrycode.text = dic["tags"].stringValue
        
        cell.lbl_countrycode.layer.cornerRadius = 10.0
        cell.lbl_countrycode.layer.masksToBounds = true
        
        cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
        cell.img_user.layer.masksToBounds = true
        
        cell.img_user.kf.indicatorType = .activity
        (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
        
        cell.img_country.kf.indicatorType = .activity
        (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))

        cell.lbl_desc.text = dic["announcement"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_roomlist.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dic = JSON(arr_roomlist.object(at: indexPath.row))
        let group = self.storyboard?.instantiateViewController(withIdentifier: "GroupChatViewController") as! GroupChatViewController
        group.checkController = "MyRoom"
        group.room_id = dic["id"].stringValue
        self.navigationController?.pushViewController(group, animated: true)
    }
    
    //MARK: Api Call
    func Myroom()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":"1" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "general_room_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["room_list"].array! as NSArray
                                    self.arr_roomlist = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
}
