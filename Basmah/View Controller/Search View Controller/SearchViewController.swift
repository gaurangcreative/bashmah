//
//  SearchViewController.swift
//  Basmah
//
//  Created by CT on 9/17/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
{
    @IBOutlet weak var view_search: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var btnRooms: UIButton!
    @IBOutlet weak var img_roomselected: UIImageView!
    @IBOutlet weak var img_userselected: UIImageView!
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var table_room: UITableView!
    @IBOutlet weak var table_user: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_user:NSMutableArray = NSMutableArray()
    var arr_room:NSMutableArray = NSMutableArray()
    var arr_searchuser:NSMutableArray = NSMutableArray()
    var arr_searchRoom:NSMutableArray = NSMutableArray()
    var type:String = String()
    var check:String = String()

    override func viewDidLoad()
    {
        super.viewDidLoad()

        view_search.layer.cornerRadius = 20.0
        view_search.layer.masksToBounds = true
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        txt_search.delegate = self
        
        table_room.isHidden = false
        table_user.isHidden = true
        type = "2"
        check = "Normal"
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                   GetRoomlist()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
        
        btnClose.isUserInteractionEnabled = false
    }
    
    @IBAction func doclickBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickRooms(_ sender: Any)
    {
        type = "2"
        check = "Normal"
        table_room.isHidden = false
        table_user.isHidden = true
        img_userselected.isHidden = true
        img_roomselected.isHidden = false
        btnRooms.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 16)
        btnUser.titleLabel?.font =  UIFont(name: "Lato-Regular", size: 16)
    }
    
    @IBAction func doclickUsers(_ sender: Any)
    {
        type = "1"
        check = "Normal"
        table_room.isHidden = true
        table_user.isHidden = false
        img_userselected.isHidden = false
        img_roomselected.isHidden = true
        btnRooms.titleLabel?.font =  UIFont(name: "Lato-Regular", size: 16)
        btnUser.titleLabel?.font =  UIFont(name: "Lato-Bold", size: 16)
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    Getgeneraluserlist()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonClearSearch(_ sender: Any)
    {
        if((txt_search.text?.count)!>0)
        {
            if(check == "search")
            {
                if(type == "1")
                {
                    txt_search.text = ""
                    check = "Normal"
                    
                    // Add reachability observer
                    if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                    {
                        if reachability.connection != .none
                        {
                            if reachability.connection == .wifi || reachability.connection == .cellular
                            {
                                Getgeneraluserlist()
                            }
                        }
                        else
                        {
                            self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                        }
                    }
                }
                else
                {
                    txt_search.text = ""
                    check = "Normal"
                    
                    // Add reachability observer
                    if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                    {
                        if reachability.connection != .none
                        {
                            if reachability.connection == .wifi || reachability.connection == .cellular
                            {
                                GetRoomlist()
                            }
                        }
                        else
                        {
                            self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                        }
                    }
                }
            }
            else
            {
                if(type == "1")
                {
                    txt_search.text = ""
                    
                    // Add reachability observer
                    if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                    {
                        if reachability.connection != .none
                        {
                            if reachability.connection == .wifi || reachability.connection == .cellular
                            {
                               Getgeneraluserlist()
                            }
                        }
                        else
                        {
                            self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                        }
                    }
                }
                else
                {
                    txt_search.text = ""
                    
                    // Add reachability observer
                    if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                    {
                        if reachability.connection != .none
                        {
                            if reachability.connection == .wifi || reachability.connection == .cellular
                            {
                                 GetRoomlist()
                            }
                        }
                        else
                        {
                            self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                        }
                    }
                   
                }
            }
        }
        else
        {
            
        }
    }
    
    //MARK: Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (check == "Normal")
        {
            if(tableView == table_room)
            {
                return arr_room.count
            }
            else
            {
                return arr_user.count
            }
        }
        else
        {
            if(tableView == table_room)
            {
                return arr_searchRoom.count
            }
            else
            {
                return arr_searchuser.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (check == "Normal")
        {
            if(tableView == table_room)
            {
                let cell:HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeTableViewCell
                
                let dic = JSON(self.arr_room.object(at: indexPath.row))

                cell.lbl_title.text = dic["room_name"].stringValue
                cell.lbl_friends.text = dic["tags"].stringValue
                
                cell.lbl_friends.layer.cornerRadius = 10.0
                cell.lbl_friends.layer.masksToBounds = true

                if (dic["image"].stringValue == "")
                {
                    cell.img_user.image = #imageLiteral(resourceName: "user_default")
                }
                else
                {
                    cell.img_user.kf.indicatorType = .activity
                    (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
                }
                
                cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
                cell.img_user.layer.masksToBounds = true
                
                cell.img_country.kf.indicatorType = .activity
                (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))

                return cell
            }
            else
            {
                let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
                
                cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
                cell.img_user.layer.masksToBounds = true

                let dic = JSON(self.arr_user.object(at: indexPath.row))
                
                if (dic["image"].stringValue == "")
                {
                    cell.img_user.image = #imageLiteral(resourceName: "user_default")
                }
                else
                {
                    cell.img_user.kf.indicatorType = .activity
                    (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
                }
                
                cell.lbl_countryname.text = dic["username"].stringValue.firstUppercased
                cell.lbl_countrycode.text = "ID:" + dic["user_id"].stringValue
                return cell
            }
        }
        else
        {
            if(tableView == table_room)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeTableViewCell
                
                let dic = JSON(self.arr_searchRoom.object(at: indexPath.row))
                
                cell.lbl_title.text = dic["room_name"].stringValue
                cell.lbl_friends.text = dic["tags"].stringValue
                
                cell.lbl_friends.layer.cornerRadius = 10.0
                cell.lbl_friends.layer.masksToBounds = true
                
                if (dic["image"].stringValue == "")
                {
                    cell.img_user.image = #imageLiteral(resourceName: "user_default")
                }
                else
                {
                    cell.img_user.kf.indicatorType = .activity
                    (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
                }
                
                cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
                cell.img_user.layer.masksToBounds = true
                
                cell.img_country.kf.indicatorType = .activity
                (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))
                return cell
            }
            else
            {
                let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
                
                cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
                cell.img_user.layer.masksToBounds = true

                let dic = JSON(self.arr_searchuser.object(at: indexPath.row))
                
                if (dic["image"].stringValue == "")
                {
                    cell.img_user.image = #imageLiteral(resourceName: "user_default")
                }
                else
                {
                    cell.img_user.kf.indicatorType = .activity
                    (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                    cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
                }
                
                cell.lbl_countryname.text = dic["username"].stringValue.firstUppercased
                cell.lbl_countrycode.text = "ID:" + dic["user_id"].stringValue
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(tableView == table_room)
        {
            return 120
        }
        else
        {
           return 80
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView == table_user)
        {
            if (check == "Normal")
            {
                let dic = JSON(self.arr_user.object(at: indexPath.row))
                let other = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfileViewController") as! OtherUserProfileViewController
                other.to_id = dic["id"].stringValue
                self.navigationController?.pushViewController(other, animated: true)
            }
            else
            {
                let dic = JSON(self.arr_searchuser.object(at: indexPath.row))
                let other = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfileViewController") as! OtherUserProfileViewController
                other.to_id = dic["id"].stringValue
                self.navigationController?.pushViewController(other, animated: true)
            }
        }
        else
        {
            if (check == "Normal")
            {
                let dic = JSON(self.arr_room.object(at: indexPath.row))
                let other = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfileViewController") as! OtherUserProfileViewController
                other.to_id = dic["id"].stringValue
                self.navigationController?.pushViewController(other, animated: true)
            }
            else
            {
                let dic = JSON(self.arr_searchRoom.object(at: indexPath.row))
                let other = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfileViewController") as! OtherUserProfileViewController
                other.to_id = dic["id"].stringValue
                self.navigationController?.pushViewController(other, animated: true)
            }
        }
    }
    
    func numberOfSectionstbl(in tableView: UITableView) -> Int
    {
        if(self.type == "1")
        {
            var numOfSections: Int = 0
            
            if arr_searchuser.count > 0
            {
                table_user.separatorStyle = .singleLine
                numOfSections = 1
                table_user.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: table_user.bounds.size.width, height: table_user.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                table_user.backgroundView  = noDataLabel
                table_user.separatorStyle  = .none
            }
            return numOfSections
        }
        else
        {
            var numOfSections: Int = 0
            
            if arr_searchRoom.count > 0
            {
                table_room.separatorStyle = .singleLine
                numOfSections = 1
                table_room.backgroundView = nil
            }
            else
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: table_room.bounds.size.width, height: table_room.bounds.size.height))
                noDataLabel.text          = "No records found."
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                table_room.backgroundView  = noDataLabel
                table_room.separatorStyle  = .none
            }
            
            return numOfSections
        }
    }
    
    //MARK: Api Call
    func Getgeneraluserlist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "general_user_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        print(response.result.value!)
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                let arr = swiftyJsonVar["user_list"].array! as NSArray
                                self.arr_user = arr.mutableCopy() as! NSMutableArray
                                self.table_user.reloadData()
                            }
                            else
                            {
                                self.numberOfSectionstbl(in: self.table_user)
                                self.table_user.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: Api Call
    func GetRoomlist()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "type":"2" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "general_room_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    let arr = swiftyJsonVar["room_list"].array! as NSArray
                                    self.arr_room = arr.mutableCopy() as! NSMutableArray
                                    self.table_room.reloadData()
                                }
                                else
                                {
                                    self.numberOfSectionstbl(in: self.table_room)
                                    self.table_room.reloadData()
                                }
                                
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //MARK: Api Call
    func Search()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "name":txt_search.text as AnyObject,
                    "type":type as AnyObject,
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "searchItem"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    if(self.type == "1")
                                    {
                                        let arr = swiftyJsonVar["user_list"].array! as NSArray
                                        self.arr_searchuser = arr.mutableCopy() as! NSMutableArray
                                        self.table_user.reloadData()
                                    }
                                    else if (self.type == "2")
                                    {
                                        let arrroom = swiftyJsonVar["room_list"].array! as NSArray
                                        self.arr_searchRoom = arrroom.mutableCopy() as! NSMutableArray
                                        self.table_room.reloadData()
                                    }
                                    else
                                    {
                                    }
                                }
                                else
                                {
                                    if(self.type == "1")
                                    {
                                        self.numberOfSectionstbl(in: self.table_user)
                                        self.table_user.reloadData()
                                    }
                                    else
                                    {
                                        self.numberOfSectionstbl(in: self.table_room)
                                        self.table_room.reloadData()
                                    }
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if((txt_search.text?.count)!>1)
        {
//            if(type == "1")
//            {
                btnClose.isUserInteractionEnabled = true
                check = "search"
            
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        Search()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
            
//            }
//            else
//            {
//                check = "search"
//                Search()
//            }
        }
        else
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please type either room name or username",buttonTitle: "OK");
        }
        
        self.view.endEditing(true)
        return true
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
