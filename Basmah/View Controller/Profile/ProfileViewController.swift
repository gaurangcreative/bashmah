//
//  ProfileViewController.swift
//  Basmah
//
//  Created by CT on 9/6/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD
import Kingfisher

class ProfileViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate
{
    let imagePicker = UIImagePickerController()
    var chosenImage = UIImage()
    var image_Data = UIImageJPEGRepresentation(UIImage(),0)
    var selectedRow = 0;
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var gender:String = String()
    var server_date:String = String()
    var user_country:String = String()

    private let arr_gender = ["Male",
                              "Female",
                             ]

    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var btnDob: UIButton!
    @IBOutlet weak var view_date: UIView!
    @IBOutlet weak var view_gender: UIView!
    @IBOutlet weak var date_picker: UIDatePicker!
    @IBOutlet weak var picker_view: UIPickerView!
    @IBOutlet weak var btnGender: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate

        view_date.isHidden = true
        view_gender.isHidden = true
        
        imagePicker.delegate = self
        
        print(user_country)
        
        btnSubmit.layer.cornerRadius = 20.0
        btnSubmit.layer.masksToBounds = true
        
        btnProfile.layer.cornerRadius = btnProfile.frame.size.width/2
        btnProfile.layer.masksToBounds = true
        btnProfile.layer.borderWidth = 1.0
        btnProfile.layer.borderColor = UIColor.lightGray.cgColor
        
        let currentDate: NSDate = NSDate()
        self.date_picker.maximumDate = currentDate as Date
        self.date_picker.addTarget(self, action: #selector(ProfileViewController.handleDatePicker), for: UIControlEvents.valueChanged)

        
        txt_username.attributedPlaceholder = NSAttributedString(string: "User Name",
                                                             attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
    }
    
    //MARK:IBAction
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonSkip(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
        navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "HomeViewController")], animated: false)
        
        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 2)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
    }
    
    @IBAction func doclickonCancel(_ sender: Any)
    {
        self.view_date.isHidden = true
    }
    
    @IBAction func doclickonDone(_ sender: Any)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        btnDob.setTitle(dateFormatter.string(from: date_picker.date), for: UIControlState.normal)
        btnDob.setTitleColor(UIColor.black, for: UIControlState.normal)
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        server_date = dateFormatter.string(from: date_picker.date)
        print(server_date)
        view_date.isHidden = true
    }
    
    @IBAction func doclickonDob(_ sender: Any)
    {
        view_date.isHidden = false
    }
    
    @objc func handleDatePicker()
    {
    }
    
    @IBAction func doclickonCancelPicker(_ sender: Any)
    {
        view_gender.isHidden = true

    }
    
    @IBAction func doclickonDonePicker(_ sender: Any)
    {
        self.view_gender.isHidden = true
        btnGender.setTitle(arr_gender[selectedRow], for: UIControlState.normal)
        btnGender.setTitleColor(UIColor.black, for: UIControlState.normal)
        gender = arr_gender[selectedRow]
    }
    
    @IBAction func doclickonSelectGender(_ sender: Any)
    {
        view_gender.isHidden = false
        self.picker_view.reloadAllComponents()
    }
    
    @IBAction func doclickonSubmit(_ sender: Any)
    {
        if (txt_username.text?.isEmpty ?? true)
        {
            self.appDelegate.showAlert(title: "Basmah",message:"Please enter username",buttonTitle: "OK");
        }
        else
        {
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                       self.Updateprofile()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
        }
    }
    
    @IBAction func doclickonProfilepic(_ sender: Any)
    {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler:
            { _ in
                self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Open Image from camera
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Basmah", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Image from gallery
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- Picker View Deleagte
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arr_gender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arr_gender[row] as String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedRow = row;
    }
    
   // MARK: Api Call
    func Updateprofile()
    {
            let defaults: UserDefaults? = UserDefaults.standard
        
            if(defaults?.dictionaryRepresentation().keys.contains("id"))!
            {
                let id = UserDefaults.standard.value(forKey: "id")
                
                let parameters: Parameters =
                [
                    "id":id as AnyObject,
                    "dob":server_date as AnyObject,
                    "gender": gender as AnyObject,
                    "username":txt_username.text as AnyObject,
                    "user_country":user_country as AnyObject,
                    "identity_tag":"" as AnyObject,
                    "hobby":"" as AnyObject,
                    "whats_up":"" as AnyObject,
                ]
                
                print(parameters)

                let url = kBaseURL + "updateProfile"
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                    if let data = self.image_Data
                    {
                        multipartFormData.append(data, withName: "user_image",fileName: "file.jpg", mimeType: "image/jpg")
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    self.hud.dismiss()
                    switch result
                    {
                        case .success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            print(response.response!)
                            print(response.data!)
                            
                            if (response.result.value == nil)
                            {
                                print("Response nil")
                                self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                            }
                            else
                            {
                                do
                                {
                                    let swiftyJsonVar = JSON(response.result.value!)
                                    
                                    if swiftyJsonVar["success"].string == "1"
                                    {
                                        let dict = swiftyJsonVar["userinfo"]
                                        
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
                                        navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "HomeViewController")], animated: false)
                                        
                                        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
                                        mainViewController.rootViewController = navigationController
                                        mainViewController.setup(type: 2)
                                        let window = UIApplication.shared.delegate!.window!!
                                        window.rootViewController = mainViewController
                                        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
                                    }
                                    else
                                    {
                                        let message = swiftyJsonVar["message"].stringValue
                                        self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                    }
                                }
                            }
                        }
                        
                        case .failure(let error):
                        self.hud.dismiss()
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
            }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension ProfileViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerEditedImage] as? UIImage) != nil
        {
            self.chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            self.image_Data = UIImageJPEGRepresentation(chosenImage, 1)
            btnProfile.setImage(self.chosenImage, for: UIControlState.normal)
        }
        
        picker.dismiss(animated: true)
    }
}
