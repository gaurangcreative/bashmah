//
//  OtherUserProfileViewController.swift
//  Basmah
//
//  Created by CT on 9/18/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD
import Kingfisher

class OtherUserProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var view_top: UIView!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var img_gender: UIImageView!
    @IBOutlet weak var img_country: UIImageView!
    @IBOutlet weak var lbl_userid: UILabel!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var btnSendrequest: GradientBtn!
    @IBOutlet weak var view_image: GradientView!
    @IBOutlet weak var view_tag: UIView!
    @IBOutlet weak var view_hobby: UIView!
    @IBOutlet weak var view_whatsup: UIView!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var view_block: UIView!
    @IBOutlet weak var btnBlockUnblock: UIButton!
    @IBOutlet weak var lbl_identity_tag: UILabel!
    @IBOutlet weak var lbl_hobby: UILabel!
    @IBOutlet weak var lbl_whatup: UILabel!
    
    var to_id:String = String()
    var room_id:String = String()
    var friend_id:Int = Int()
    var follow_status:Int = Int()
    var accept_reject_status:Int = Int()
    var country_id:String = String()
    var country_image:String = String()
    var country_name:String = String()
    var block_status:Int = Int()
    var Openclose:Bool = Bool()
    var friend_status:Int = Int()
    var other_user_image:String = String()
    
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_detail:NSMutableArray = NSMutableArray()
    
    private let arr_more = ["Block",]
    
    @IBOutlet weak var table_view: UITableView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as! AppDelegate

        // Do any additional setup after loading the view.
        view_block.isHidden = true
        Openclose = false
        SetCorner()
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                     GetOtheruserdetail()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == true, animated: false)
    }
    
    func SetCorner()
    {
        view_top.layer.cornerRadius = 6.0
        view_top.layer.masksToBounds = true
        
        btnSendrequest.layer.cornerRadius = 20.0
        btnSendrequest.layer.masksToBounds = true
        
        btnAccept.layer.cornerRadius = 4.0
        btnAccept.layer.masksToBounds = true
        
        btnReject.layer.cornerRadius = 4.0
        btnReject.layer.masksToBounds = true
        
        view_image.layer.cornerRadius = view_image.frame.size.width/2
        view_image.layer.masksToBounds = true
        
        img_user.layer.cornerRadius = img_user.frame.size.width/2
        img_user.layer.masksToBounds = true
        
        view_top.layer.shadowColor = UIColor.lightGray.cgColor
        view_top.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_top.layer.shadowOpacity = 0.5
        view_top.layer.shadowRadius = 2.0
        view_top.layer.masksToBounds = false
        
        view_hobby.layer.shadowColor = UIColor.lightGray.cgColor
        view_hobby.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_hobby.layer.shadowOpacity = 0.5
        view_hobby.layer.shadowRadius = 2.0
        view_hobby.layer.masksToBounds = false
        
        view_tag.layer.shadowColor = UIColor.lightGray.cgColor
        view_tag.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_tag.layer.shadowOpacity = 0.5
        view_tag.layer.shadowRadius = 2.0
        view_tag.layer.masksToBounds = false
        
        view_whatsup.layer.shadowColor = UIColor.lightGray.cgColor
        view_whatsup.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_whatsup.layer.shadowOpacity = 0.5
        view_whatsup.layer.shadowRadius = 2.0
        view_whatsup.layer.masksToBounds = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_more.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    
    @IBAction func doclickonBlock(_ sender: Any)
    {
        if(self.block_status == 2)
        {
            let refreshAlert = UIAlertController(title: "Basmah", message: "Do you really want to block this user?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
                
                self.view_block.isHidden = true
                self.Openclose = false
                
                self.block_status = 1
                
                // Add reachability observer
                if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                {
                    if reachability.connection != .none
                    {
                        if reachability.connection == .wifi || reachability.connection == .cellular
                        {
                           self.BlockUser()
                        }
                    }
                    else
                    {
                        self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                    }
                }
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Basmah", message: "Do you really want to Unblock this user?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
                
                self.block_status = 2
                
                self.view_block.isHidden = true
                self.Openclose = false
                
                // Add reachability observer
                if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                {
                    if reachability.connection != .none
                    {
                        if reachability.connection == .wifi || reachability.connection == .cellular
                        {
                            self.BlockUser()
                        }
                    }
                    else
                    {
                        self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                    }
                }
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    //MARK: IBAction
    @IBAction func doclickonMore(_ sender: Any)
    {
       if (Openclose == false)
        {
            view_block.isHidden = false
            Openclose = true
        }
        else
        {
            view_block.isHidden = true
            Openclose = false
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func doclickonAccept(_ sender: Any)
    {
        accept_reject_status = 1
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    Accept_reject()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonReject(_ sender: Any)
    {
        accept_reject_status = 2
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    Accept_reject()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func docliconSendRequest(_ sender: Any)
    {
        let refreshAlert = UIAlertController(title: "Basmah", message: "Do you really want to send friend request?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            // Add reachability observer
            if let reachability = AppDelegate.sharedAppDelegate()?.reachability
            {
                if reachability.connection != .none
                {
                    if reachability.connection == .wifi || reachability.connection == .cellular
                    {
                        self.SendRequest()
                    }
                }
                else
                {
                    self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                }
            }
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func doclickonFollowUnfollow(_ sender: Any)
    {
        if (follow_status == 1)
        {
            follow_status = 2
        }
        else
        {
            follow_status = 1
        }
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    Follow_user()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    @IBAction func doclickonChat(_ sender: Any)
    {
        
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    if(self.block_status == 2)
                    {
                        if (self.friend_status == 4)
                        {
                            let chef_id = to_id
                            let userid = UserDefaults.standard.value(forKey: "id") as! String
                            
                            if (chef_id > userid)
                            {
                                let chatId = "room_" + chef_id + "_" + userid
                                print(chatId)
                                
                                let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                                chat.to_id = to_id
                                chat.other_username = self.lbl_username.text!
                                chat.other_userimage = self.other_user_image
                                chat.room_id = chatId
                                self.navigationController?.pushViewController(chat, animated: true)
                            }
                            else
                            {
                                let chatId = "room_" + userid + "_" + chef_id
                                print(chatId)
                                
                                let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                                chat.to_id = to_id
                                chat.other_username = self.lbl_username.text!
                                chat.other_userimage = self.other_user_image
                                chat.room_id = chatId
                                self.navigationController?.pushViewController(chat, animated: true)
                            }
                        }
                    }
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    //MARK: Api Call
    func GetOtheruserdetail()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":to_id as AnyObject,
                    "type":"1" as AnyObject,
                    "my_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_detail"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    //                                let arr = swiftyJsonVar["userinfo"].array! as NSArray
                                    //                                self.arr_detail = arr.mutableCopy() as! NSMutableArray
                                    //                                let dic = JSON(self.arr_detail.object(at: 0))
                                    
                                    let dic = swiftyJsonVar["userinfo"]
                                    self.lbl_username.text = dic["username"].stringValue.firstUppercased
                                    self.lbl_userid.text = "ID:" + dic["user_id"].stringValue
                                    
                                    self.follow_status = dic["follow_status"].intValue
                                    self.block_status = dic["block_status"].intValue
                                    
                                    self.other_user_image = dic["user_image"].stringValue
                                    
                                    if(self.block_status == 2)
                                    {
                                        self.btnBlockUnblock.setTitle("Block", for: UIControlState.normal)
                                    }
                                    else
                                    {
                                        self.btnBlockUnblock.setTitle("Unblock", for: UIControlState.normal)
                                    }
                                    
                                    if(self.follow_status == 1)
                                    {
                                        self.btnFollow.setImage(#imageLiteral(resourceName: "follow_icon_active"), for: UIControlState.normal)
                                    }
                                    else
                                    {
                                        self.btnFollow.setImage(#imageLiteral(resourceName: "follow_icon"), for: UIControlState.normal)
                                    }
                                    
                                    if (dic["friend_status"].intValue == 1)
                                    {
                                        self.btnAccept.isHidden = false
                                        self.btnReject.isHidden = false
                                        self.btnSendrequest.isHidden = true
                                    }
                                    else if (dic["friend_status"].intValue == 2)
                                    {
                                        self.btnReject.isHidden = true
                                        self.btnAccept.isHidden = true
                                        self.btnSendrequest.isHidden = false
                                        self.btnSendrequest.isUserInteractionEnabled = false
                                        
                                        self.btnSendrequest.setTitle("Already send request", for: UIControlState.normal)
                                    }
                                    else if (dic["friend_status"].intValue == 3)
                                    {
                                        self.btnReject.isHidden = true
                                        self.btnAccept.isHidden = true
                                        self.btnSendrequest.isHidden = false
                                        self.btnSendrequest.setTitle("Send Request", for: UIControlState.normal)
                                    }
                                    else
                                    {
                                        self.btnReject.isHidden = true
                                        self.btnAccept.isHidden = true
                                        self.btnSendrequest.isHidden = false
                                        self.btnSendrequest.isUserInteractionEnabled = false
                                        self.btnSendrequest.setTitle("Friend", for: UIControlState.normal)
                                    }
                                    
                                    self.friend_status = dic["friend_status"].intValue
                                    
                                    self.img_country.kf.indicatorType = .activity
                                    (self.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                    self.img_country.kf.setImage(with: URL(string:dic["country"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    self.lbl_whatup.text = dic["whats_up"].stringValue
                                    self.lbl_hobby.text = dic["hobby"].stringValue
                                    self.lbl_identity_tag.text = dic["identity_tag"].stringValue
                                    self.friend_id = dic["friend_id"].intValue
                                    
                                    let createdat = dic["created_at"].stringValue as String
                                    let fullNameArr = createdat.components(separatedBy: " ")
                                    
                                    if (fullNameArr.count>0)
                                    {
                                        let week = fullNameArr[0]
                                        
                                        if (week == "")
                                        {
                                            
                                        }
                                        else
                                        {
                                            let check = fullNameArr[1]
                                            
                                            if (check == "week")
                                            {
                                                self.lbl_count.text = week + " W"
                                            }
                                            else if(check == "month")
                                            {
                                                self.lbl_count.text = week + " M"
                                            }
                                            else if (check == "year")
                                            {
                                                self.lbl_count.text = week + " Y"
                                            }
                                        }
                                    }
                                    
                                    self.img_user.kf.indicatorType = .activity
                                    (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                    self.img_user.kf.setImage(with: URL(string:dic["user_image"].stringValue), placeholder: UIImage(named: ""))
                                    
                                    if (dic["gender"].stringValue == "male")
                                    {
                                        self.img_gender.image = #imageLiteral(resourceName: "male_icon")
                                    }
                                    else if(dic["gender"].stringValue == "")
                                    {
                                        self.img_gender.isHidden = true
                                    }
                                    else
                                    {
                                        self.img_gender.image = #imageLiteral(resourceName: "female_icon")
                                    }
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                        
                        
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func SendRequest()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "friend_id":to_id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "friend_request"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].intValue == 1
                                {
                                    self.navigationController?.popViewController(animated: true)
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func Follow_user()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "follow_uid":to_id as AnyObject,
                    "status":follow_status as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "follow_user"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].intValue == 1
                                {
                                    if(swiftyJsonVar["message"] == "Follow added")
                                    {
                                        self.btnFollow.setImage(#imageLiteral(resourceName: "follow_icon_active"), for: UIControlState.normal)
                                    }
                                    else
                                    {
                                        self.btnFollow.setImage(#imageLiteral(resourceName: "follow_icon"), for: UIControlState.normal)
                                    }
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func Accept_reject()
    {
            let parameters: Parameters =
                [
                    "id":self.friend_id as AnyObject,
                    "status":accept_reject_status as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "accept_reject_friend"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].intValue == 1
                                {
                                    self.navigationController?.popViewController(animated: true)
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
    }
    
    func BlockUser()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "block_uid":to_id as AnyObject,
                    "status":self.block_status as AnyObject
                ]
            
            print(parameters)
            
            let url = kBaseURL + "block_user"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].intValue == 1
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                    
                                    // Add reachability observer
                                    if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                                    {
                                        if reachability.connection != .none
                                        {
                                            if reachability.connection == .wifi || reachability.connection == .cellular
                                            {
                                                self.GetOtheruserdetail()
                                            }
                                        }
                                        else
                                        {
                                            self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                                        }
                                    }
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
