//
//  MemberListViewController.swift
//  Basmah
//
//  Created by CT on 10/10/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class MemberListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var hud = JGProgressHUD()
    var appDelegate = AppDelegate()
    var arr_memberlist:NSMutableArray = NSMutableArray()
    var room_id:String = String()

    @IBOutlet weak var view_search: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var table_view: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view_search.layer.cornerRadius = 6.0
        view_search.layer.masksToBounds = true
        
        view_search.layer.shadowColor = UIColor.lightGray.cgColor
        view_search.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view_search.layer.shadowOpacity = 0.5
        view_search.layer.shadowRadius = 2.0
        view_search.layer.masksToBounds = false
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        MemberList()
    }
    
    func MemberList()
    {
            let parameters: Parameters =
                [
                    "room_id":room_id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "member_list"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let arr = swiftyJsonVar["member_list"].array! as NSArray
                                    self.arr_memberlist = arr.mutableCopy() as! NSMutableArray
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSections(in: self.table_view)
                                self.table_view.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table View Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arr_memberlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CountryListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryListTableViewCell
        
        let dic = JSON(arr_memberlist.object(at: indexPath.row))

        cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
        cell.img_user.layer.masksToBounds = true
        
        cell.img_onlineoroffline.layer.cornerRadius = cell.img_onlineoroffline.frame.size.width/2
        cell.img_onlineoroffline.layer.masksToBounds = true
        
        cell.lbl_countryname.text  = dic["username"].stringValue
        cell.lbl_countrycode.text = dic["user_id"].stringValue
        
        cell.img_user.layer.cornerRadius = cell.img_user.frame.size.width/2
        cell.img_user.layer.masksToBounds = true
        
        cell.img_user.kf.indicatorType = .activity
        (cell.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_user.kf.setImage(with: URL(string:dic["image"].stringValue), placeholder: UIImage(named: ""))
        
        cell.img_country.kf.indicatorType = .activity
        (cell.img_country.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
        cell.img_country.kf.setImage(with: URL(string:dic["country_image"].stringValue), placeholder: UIImage(named: ""))


        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let title = "Member 6/400"
        return title
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
            
        if arr_memberlist.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
            return numOfSections
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
