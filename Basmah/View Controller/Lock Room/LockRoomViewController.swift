//
//  LockRoomViewController.swift
//  Basmah
//
//  Created by CT on 9/15/18.
//  Copyright © 2018 CT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JGProgressHUD

class LockRoomViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var providerTitleArry : [String] = ["01","02","03","04","05","06"]
    var providerTitleArry2 : [String] = ["Lock the Room","Room Owner Privilege","How to use"]
    var providerTitleArry3 : [String] = ["Enter the room with password","Only room owner can lock/unlock the room","In room > Up Right Corner > Lock"]
    
    var uiColorArray = [UIColor]()
    
    var hud = JGProgressHUD()
    var arr_plan_list:NSMutableArray = NSMutableArray()
    var arr_day:NSMutableArray = NSMutableArray()
    var appDelegate = AppDelegate()
    var plan_id:String = String()
    var end_date:String = String()
    var enddate:Date = Date()
    var currentdate:Date = Date()
    var daytimer = Timer()

    @IBOutlet weak var table_month: UITableView!
    @IBOutlet weak var table_room: UITableView!
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var lbl_days: UILabel!
    
    //Height Constant
    @IBOutlet weak var consTblMonth: NSLayoutConstraint!
    @IBOutlet weak var consTblYear: NSLayoutConstraint!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        self.show_room_lock()
    }
    
    func setTableLayout()
    {
        consTblMonth.constant = CGFloat(arr_plan_list.count * 70)
        DispatchQueue.main.async
        {
            self.table_month.reloadData()
        }
        
        consTblYear.constant = CGFloat(providerTitleArry2.count * 60)
        DispatchQueue.main.async
        {
            self.table_room.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Table view delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(tableView == table_month)
        {
            return arr_plan_list.count
        }
        else
        {
            return providerTitleArry2.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.table_month
        {
            let cell = self.table_month.dequeueReusableCell(withIdentifier: "RoomLockCell") as! RoomLockTableViewCell
            
            cell.selectionStyle = .none
            cell.view_room.layer.shadowColor = UIColor.lightGray.cgColor
            cell.view_room.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            cell.view_room.layer.shadowOpacity = 0.5
            cell.view_room.layer.shadowRadius = 2.0
            cell.view_room.layer.masksToBounds = false
            
            cell.view_bg.backgroundColor = self.uiColorArray[indexPath.row]
            
            let dic = JSON(arr_plan_list.object(at: indexPath.row))
            cell.lbl_price?.text = dic["price"].stringValue
            cell.lblMonthNumber?.text = dic["month"].stringValue
            return cell
        }
        else
        {
            let cell = self.table_room.dequeueReusableCell(withIdentifier: "RoomLockSecondCell") as! RoomLockSecondCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.lblTop?.text = providerTitleArry2[indexPath.row]
            cell.lbl_bottom?.text = providerTitleArry3[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.table_month
        {
            return 70
        }
        else
        {
            return 60
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections: Int = 0
        
        if arr_plan_list.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No records found."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.table_month
        {
            let refreshAlert = UIAlertController(title: "Basmah", message: "Confirm to purchase Room Lock?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
                
                // Add reachability observer
                if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                {
                    if reachability.connection != .none
                    {
                        if reachability.connection == .wifi || reachability.connection == .cellular
                        {
                            let dic = JSON(self.arr_plan_list.object(at: indexPath.row))
                            
                            self.plan_id = dic["id"].stringValue
                            print(self.plan_id)
                            self.add_room_lock()
                        }
                    }
                    else
                    {
                        self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                    }
                }
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    @IBAction func doclickonBack(_ sender: Any)
    {
        self.daytimer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doclickonRechargeList(_ sender: Any)
    {
        let recharge = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
        self.navigationController?.pushViewController(recharge, animated: true)
    }
    
    //MARK:Api Call
    func show_room_lock()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "show_room_lock"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let arr = swiftyJsonVar["plan_list"].array! as NSArray
                                    self.arr_plan_list = arr.mutableCopy() as! NSMutableArray
                                    
                                    var i = 0
                                    
                                    while i < self.arr_plan_list.count
                                    {
                                        self.uiColorArray.append(UIColor(red: 171/255.0, green: 84/255.0, blue: 249/255.0, alpha: 1.0) )
                                        
                                        self.uiColorArray.append(UIColor(red: 82/255.0, green: 85/255.0, blue: 249/255.0, alpha: 1.0) )
                                        
                                        self.uiColorArray.append(UIColor(red: 245/255.0, green: 58/255.0, blue: 111/255.0, alpha: 1.0) )
                                        
                                        i = i + 1
                                    }
                                    
                                    let arrday = swiftyJsonVar["day_array"].array! as NSArray
                                    self.arr_day = arrday.mutableCopy() as! NSMutableArray
                                    print(self.arr_day)
                                    
                                    if (self.arr_day.count>0)
                                    {
                                        let dic = JSON(self.arr_day.object(at: 0))
                                        
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd"
                                        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                                        self.enddate = dateFormatter.date(from: dic["end_date"].stringValue)!
                                        print(self.enddate)
                                        
                                        self.currentdate = Date()
                                        print(self.currentdate)
                                        
                                        self.scheduledTimerWithTimeInterval()
                                    }
                                    else
                                    {
                                        self.lbl_days.text = "0" as String
                                    }
                                    
                                    self.setTableLayout()
                                }
                                else
                                {
                                    self.hud.dismiss()
                                }
                                
                                self.numberOfSections(in: self.table_month)
                                self.table_month.reloadData()
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    //MARK: -- Dayscalculate
    func scheduledTimerWithTimeInterval()
    {
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        daytimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.calculatedays), userInfo: nil, repeats: true)
    }
    
    @objc func calculatedays()
    {
        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: self.currentdate)
        let date2 = calendar.startOfDay(for: self.enddate)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        print(components.day)
        
        if (components.day == 0)
        {
            self.lbl_days.text = "0" as String
            daytimer.invalidate()
        }
        else
        {
           self.lbl_days.text = NSString(format:"%d",components.day!) as String
        }
    }
    
    //MARK:Api Call
    func add_room_lock()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "user_id":id as AnyObject,
                    "plan":self.plan_id as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "add_room_lock"
            hud = JGProgressHUD(style: .dark)
            hud.textLabel.text = "Loading"
            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                self.hud.dismiss()
                
                switch result
                {
                    case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        print(response.response!)
                        print(response.data!)
                        
                        if (response.result.value == nil)
                        {
                            print("Response nil")
                            self.appDelegate.showAlert(title:alerttitle,message: alertservererr,buttonTitle: alertok);
                        }
                        else
                        {
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    self.hud.dismiss()
                                    let message1 = swiftyJsonVar["message"].stringValue as String
                                    
                                    if (message1 == "Already added room lock")
                                    {
                                        let message1 = swiftyJsonVar["message"].stringValue as String
                                        self.appDelegate.showAlert(title: "Basmah",message: message1,buttonTitle: "OK");
                                    }
                                    else
                                    {
                                        self.show_room_lock()
                                    }
                                }
                                else
                                {
                                    self.hud.dismiss()
                                    
                                    let message1 = swiftyJsonVar["message"].stringValue as String
                                    self.appDelegate.showAlert(title: "Basmah",message: message1,buttonTitle: "OK");
                                }
                            }
                        }
                    }
                    
                    case .failure(let error):
                    self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
