//
//  MenuSelectedTableViewCell.swift
//  Chefx
//
//  Created by samosys on 10/01/18.
//  Copyright © 2018 Samosys. All rights reserved.
//

import UIKit

class MenuSelectedTableViewCell: UITableViewCell
{
    @IBOutlet weak var lbl_selectedTitle: UILabel!
    @IBOutlet weak var img_selected: UIImageView!
    @IBOutlet weak var img_noti: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        img_noti.layer.cornerRadius = img_noti.frame.size.height/2
        img_noti.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
