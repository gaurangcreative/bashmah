//
//  RightViewController.swift
//  LGSideMenuControllerDemo
//

import SwiftyJSON
import Alamofire
import JGProgressHUD
import Kingfisher
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit

class RightViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var view_top: UIView!
    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var btnUsername: UIButton!
    @IBOutlet weak var lbl_userid: UILabel!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var view_bottom: UIView!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var imgbg: UIImageView!
    
    var appDelegate = AppDelegate()
    var arr_user:NSMutableArray = NSMutableArray()
    var hud = JGProgressHUD()
    var chosenImage = UIImage()
    
    private let titlesArray = ["Activity",
                               "Message",
                               "Notification",
                               "Settings",
                               "Recharge",
                               "Store",
                               "Share",
                               "Logout"
                             ]
    
    private let ChefimagesArray = ["activity",
                                   "message",
                                   "notify_icon",
                                   "setting",
                                   "gift",
                                   "store",
                                   "share",
                                   "logout",
                                  ]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        img_user.layer.cornerRadius = img_user.frame.size.width/2
        img_user.layer.masksToBounds = true
        img_user.layer.borderWidth = 1.0
        img_user.layer.borderColor = UIColor.white.cgColor
        
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Profile"), object: nil)
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification)
    {
        // Add reachability observer
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability
        {
            if reachability.connection != .none
            {
                if reachability.connection == .wifi || reachability.connection == .cellular
                {
                    self.UserDetails()
                }
            }
            else
            {
                self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
            }
        }
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("Profile"), object: nil)
    }

    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        let defaults: UserDefaults? = UserDefaults.standard

        if(appDelegate.greeting == "FB")
        {
            if(defaults?.dictionaryRepresentation().keys.contains("userfb_image"))!
            {
                let user_image = UserDefaults.standard.value(forKey: "userfb_image") as! String
                self.img_user.kf.indicatorType = .activity
                (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color = color
                self.img_user.kf.setImage(with: URL(string:user_image), placeholder: UIImage(named: ""))
            }
        }
        else if (appDelegate.greeting == "Google")
        {
            if(defaults?.dictionaryRepresentation().keys.contains("usergoo_image"))!
            {
                let user_image = UserDefaults.standard.value(forKey: "usergoo_image") as! String
                self.img_user.kf.indicatorType = .activity
                (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color = color
                self.img_user.kf.setImage(with: URL(string:user_image), placeholder: UIImage(named: ""))
            }
        }
        else
        {
            if(defaults?.dictionaryRepresentation().keys.contains("user_image"))!
            {
                let user_image = UserDefaults.standard.value(forKey: "user_image") as! String
                self.img_user.kf.indicatorType = .activity
                (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color = color
                self.img_user.kf.setImage(with: URL(string:user_image), placeholder: UIImage(named: ""))
            }
        }
        
        if(defaults?.dictionaryRepresentation().keys.contains("user_id"))!
        {
            let user_id = UserDefaults.standard.value(forKey: "user_id")
            lbl_userid.text =  String(format:"%@%@","ID:",user_id as! CVarArg)
        }
        
        if(defaults?.dictionaryRepresentation().keys.contains("username"))!
        {
            let username = UserDefaults.standard.value(forKey: "username") as! String
            btnUsername.setTitle(username, for: .normal)

            if(username == "")
            {
                lbl_userid.translatesAutoresizingMaskIntoConstraints = false

                NSLayoutConstraint.activate([
                lbl_userid.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
                lbl_userid.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: img_user.frame.origin.x+img_user.frame.size.width+15),
                lbl_userid.widthAnchor.constraint(equalToConstant: lbl_userid.frame.size.width),
                lbl_userid.heightAnchor.constraint(equalToConstant: lbl_userid.frame.size.height),
                ])
            }
            else
            {
                lbl_userid.translatesAutoresizingMaskIntoConstraints = false

                NSLayoutConstraint.activate([
                    lbl_userid.topAnchor.constraint(equalTo: view.topAnchor, constant: btnUsername.frame.origin.y+btnUsername.frame.size.height+2),
                    lbl_userid.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: img_user.frame.origin.x+img_user.frame.size.width+15),
                    lbl_userid.widthAnchor.constraint(equalToConstant: lbl_userid.frame.size.width),
                    lbl_userid.heightAnchor.constraint(equalToConstant: lbl_userid.frame.size.height),
                    ])
            }
        }
        else
        {

        }
    }
    
    //MARK: Api Call
    func UserDetails()
    {
        let defaults: UserDefaults? = UserDefaults.standard
        if(defaults?.dictionaryRepresentation().keys.contains("id"))!
        {
            let id = UserDefaults.standard.value(forKey: "id")
            
            let parameters: Parameters =
                [
                    "id":id as AnyObject,
                    "my_id":"" as AnyObject,
                    "type": "2" as AnyObject,
                ]
            
            print(parameters)
            
            let url = kBaseURL + "user_detail"
//            hud = JGProgressHUD(style: .dark)
//            hud.textLabel.text = "Loading"
//            hud.show(in: self.view)
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Authorization": "dfs#!df154$",
                ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                //self.hud.dismiss()
                
                switch result
                {
                  case .success(let upload, _, _):
                  //self.hud.dismiss()
                    upload.responseJSON { response in
                        
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        print(response.result.value!)
                        do
                        {
                            let swiftyJsonVar = JSON(response.result.value!)
                            
                            if swiftyJsonVar["success"].string == "1"
                            {
                                let dic = swiftyJsonVar["userinfo"]
                                
                                self.img_user.kf.indicatorType = .activity
                                (self.img_user.kf.indicator?.view as? UIActivityIndicatorView)?.color =  UIColor.black
                                self.img_user.kf.setImage(with: URL(string:dic["user_image"].stringValue), placeholder: UIImage(named: ""))
                            }
                            else
                            {
                               // self.hud.dismiss()
//                                let message = swiftyJsonVar["message"].stringValue
//                                self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                            }
                        }
                    }
                    
                    case .failure(let error):
                    //self.hud.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation
    {
        return .slide
    }
    
    @IBAction func doclickonUser(_ sender: Any)
    {
        let mainViewController = UIApplication.shared.delegate!.window!!.rootViewController! as! MainViewController
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ShowProfileViewController") as! ShowProfileViewController
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(viewController, animated: true)
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func doclickonAbout(_ sender: Any)
    {
        let mainViewController = UIApplication.shared.delegate!.window!!.rootViewController! as! MainViewController
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(viewController, animated: true)
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func doclickonHelp(_ sender: Any)
    {
        let mainViewController = UIApplication.shared.delegate!.window!!.rootViewController! as! MainViewController
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(viewController, animated: true)
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
    }
    
//    // func signOutFromInstagram()
//    {
//    let url = "https://instagram.com/accounts/logout"
//    let request : NSMutableURLRequest = NSMutableURLRequest(url: URL(string: url)!)
//    request.httpMethod = "GET"
//    request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
//    let session = URLSession(configuration: URLSessionConfiguration.default)
//    session.dataTask(with: request as URLRequest) { (data, response, error) in
//    if let data = data{
//    let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
//    let userInfo = json?.value(forKey: "data") as Any
//    print(userInfo)
//    }
//    }.resume()
//    }
    
    // MARK: - Table View Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RightViewCell
        cell.titleLabel.text = titlesArray[indexPath.row]
        cell.separatorView.image = UIImage(named:ChefimagesArray[indexPath.row])
        
        if (indexPath.row == 0)
        {
            cell.lbl_count.isHidden = false
        }
        else if indexPath.row == 1
        {
            cell.lbl_count.isHidden = false
        }
        else if indexPath.row == 2
        {
            cell.lbl_count.isHidden = false
        }
        else if indexPath.row == 3
        {
            cell.lbl_count.isHidden = true
        }
        else if indexPath.row == 4
        {
            cell.lbl_count.isHidden = true
        }
        else if indexPath.row == 5
        {
            cell.lbl_count.isHidden = true
        }
        else if indexPath.row == 6
        {
            cell.lbl_count.isHidden = true
        }
        else if indexPath.row == 7
        {
            cell.lbl_count.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let mainViewController = UIApplication.shared.delegate!.window!!.rootViewController! as! MainViewController
        //tableView.reloadData()
        
            if indexPath.row == 0
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "ActivityViewController") as! ActivityViewController
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(viewController, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            else if indexPath.row == 1
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(viewController, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)

            }
            else if indexPath.row == 2
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(viewController, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            else if indexPath.row == 3
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "SattingViewController") as! SattingViewController
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(viewController, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            else if indexPath.row == 4
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(viewController, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            else if(indexPath.row == 5)
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "StoreViewController") as! StoreViewController
                let navigationController = mainViewController.rootViewController as! NavigationController
                navigationController.pushViewController(viewController, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            else if indexPath.row == 6
            {
            }
            else if indexPath.row == 7
            {
                let mainViewController = UIApplication.shared.delegate!.window!!.rootViewController! as! MainViewController
                
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
                // create the alert
                let alert = UIAlertController(title: "Basmah", message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
                }))
                
                alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
                    
                    // Add reachability observer
                    if let reachability = AppDelegate.sharedAppDelegate()?.reachability
                    {
                        if reachability.connection != .none
                        {
                            if reachability.connection == .wifi || reachability.connection == .cellular
                            {
                               self.logout()
                            }
                        }
                        else
                        {
                            self.appDelegate.showAlert(title: "Basmah",message:"Network not reachable",buttonTitle: "OK");
                        }
                    }
                    
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
                let window = UIApplication.shared.keyWindow!
                imgbg.frame = CGRect(x: 0, y:0,width: window.frame.size.width,height:window.frame.size.height);
                window.addSubview(imgbg);
            }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    //country_id
    func resetDefaults()
    {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach
            { key in
                
                let stddefaults: UserDefaults? = UserDefaults.standard
                
                if(stddefaults?.dictionaryRepresentation().keys.contains("country_id"))!
                {
                    
                }
                if(stddefaults?.dictionaryRepresentation().keys.contains("country_name"))!
                {
                    
                }
                else
                {
                    defaults.removeObject(forKey: key)
                }
            }
    }
    
    func logout()
    {
            let defaults: UserDefaults? = UserDefaults.standard
            if(defaults?.dictionaryRepresentation().keys.contains("id"))!
            {
                let id = UserDefaults.standard.value(forKey: "id")
                
                let parameters: Parameters =
                    [
                        "user_id":id as AnyObject,
                    ]
                
                print(parameters)
                
                let url = kBaseURL + "logout"
                hud = JGProgressHUD(style: .dark)
                hud.textLabel.text = "Loading"
                hud.show(in: self.view)
                
                let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data",
                    "Authorization": "dfs#!df154$",
                    ]
                
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                    
                    self.hud.dismiss()
                    
                    switch result
                    {
                        case .success(let upload, _, _):
                        
                        upload.responseJSON { response in
                            
                            print(response.response!) // URL response
                            print(response.data!)     // server data
                            print(response.result.value!)
                            do
                            {
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                if swiftyJsonVar["success"].string == "1"
                                {
                                    if(self.appDelegate.greeting == "Google")
                                    {
                                        GIDSignIn.sharedInstance().signOut()
                                    }
                                    else if (self.appDelegate.greeting == "FB")
                                    {
                                        FBSDKAccessToken.setCurrent(nil)
                                        FBSDKProfile.setCurrent(nil)
                                        let manager = FBSDKLoginManager()
                                        manager.logOut()
                                    }
                                    
                                    self.imgbg.frame = CGRect(x:0, y:1000, width:self.view.frame.size.width, height:self.view.frame.size.height);
                                    
                                    self.resetDefaults()
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
                                    
                                navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: "ViewController")], animated: false)
                                    
                                   
                                    
                                    let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
                                    mainViewController.rootViewController = navigationController
                                    mainViewController.setup(type: 2)
                                    let window = UIApplication.shared.delegate!.window!!
                                    window.rootViewController = mainViewController
                                }
                                else
                                {
                                    let message = swiftyJsonVar["message"].stringValue
                                    self.appDelegate.showAlert(title: "Basmah",message: message,buttonTitle: "OK");
                                }
                            }
                        }
                        
                        case .failure(let error):
                        self.hud.dismiss()
                        print("Error in upload: \(error.localizedDescription)")
                    }
                }
            }
    }
}

extension StringProtocol
{
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}


