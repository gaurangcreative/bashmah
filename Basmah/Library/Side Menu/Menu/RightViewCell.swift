//
//  RightViewCell.swift
//  LGSideMenuControllerDemo
//

class RightViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var separatorView: UIImageView!
    @IBOutlet weak var lbl_count: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.lbl_count.layer.cornerRadius = self.lbl_count.frame.size.width/2
        self.lbl_count.layer.masksToBounds = true
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool)
    {
        
    }
    
}
